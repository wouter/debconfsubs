1
00:00:00,000 --> 00:00:02,490
Thank you very much.

2
00:00:03,150 --> 00:00:05,060
Thanks everybody for coming,…

3
00:00:08,666 --> 00:00:12,186
If you are packaging software and you want
me to work on with you,

4
00:00:12,186 --> 00:00:13,526
this is how you can do that.

5
00:00:13,526 --> 00:00:15,156
It is a very self-centered talk:

6
00:00:15,156 --> 00:00:18,516
I just want to explain some of the things
that I like about,

7
00:00:18,516 --> 00:00:21,366
some practice that I prefer
about Debian packaging,

8
00:00:21,366 --> 00:00:25,236
and I don't pretend this is any sort of…

9
00:00:25,236 --> 00:00:27,296
official, permanent or final thing.

10
00:00:27,296 --> 00:00:30,406
I just wanted to share some ideas
that I have about

11
00:00:30,406 --> 00:00:33,376
the way that I work with packages,
in the hope that maybe…

12
00:00:33,376 --> 00:00:34,616
For two hopes:

13
00:00:34,616 --> 00:00:38,207
One is that I hope that I can show you
something that you have not heard of,

14
00:00:38,207 --> 00:00:39,987
or maybe you were doing differently,

15
00:00:39,987 --> 00:00:42,137
or maybe you think it is
the right think to do

16
00:00:42,137 --> 00:00:44,467
and it is just nice to see
somebody else doing it.

17
00:00:44,467 --> 00:00:47,437
My second hope is that you can tell me
what I am doing wrong,

18
00:00:47,437 --> 00:00:51,047
and you can help me learn and improve
on my own packaging techniques.

19
00:00:51,047 --> 00:00:53,337
If you see something that
I am proposing up here,

20
00:00:53,337 --> 00:00:56,627
and you think there is a problem with it,
I would like to hear about it too.

21
00:00:56,627 --> 00:00:58,737
I just want to see more of the culture
within Debian,

22
00:00:58,737 --> 00:01:00,887
of people who are doing packaging,
explaining what they are doing,

23
00:01:00,887 --> 00:01:02,817
and so I thought I would
just step up and explain:

24
00:01:02,847 --> 00:01:05,377
"Here is some of the practice that I do",

25
00:01:05,377 --> 00:01:09,247
In the hope that other people will do the
same and explain what they are doing,

26
00:01:09,247 --> 00:01:13,047
and maybe they can learn from
me and I can learn from them.

27
00:01:13,047 --> 00:01:16,917
Without much further ado I am
just going to dive into it.

28
00:01:16,917 --> 00:01:20,897
If you have questions, I am perfectly
happy to be interrupted,

29
00:01:20,897 --> 00:01:24,187
we have some folks with
walking mics in the crowd:

30
00:01:24,187 --> 00:01:26,517
you can just raise your hand.

31
00:01:26,517 --> 00:01:29,247
If you have got a question or an
interruption or whatever,

32
00:01:29,247 --> 00:01:30,347
that is fine.

33
00:01:30,347 --> 00:01:33,107
I doubt I will go the whole 15 minutes,
I think there are 20 minutes,

34
00:01:33,107 --> 00:01:36,217
I doubt I will go the whole time,
so there will be also

35
00:01:36,217 --> 00:01:38,337
time for questions at
the end if you prefer.

36
00:01:38,337 --> 00:01:40,137
But I do not mind being interrupted.

37
00:01:40,137 --> 00:01:41,977
So, this is all on this web page here,

38
00:01:41,977 --> 00:01:44,787
you could probably skip this talk and go
read the web page,

39
00:01:44,787 --> 00:01:47,227
but then you would not have the nice
in-person interactions,

40
00:01:47,227 --> 00:01:49,747
and it is easier to tell me that I am
wrong in person,

41
00:01:49,747 --> 00:01:51,477
so I would like to have that happen.

42
00:01:51,477 --> 00:01:53,427
I put this up on the Debian wiki,

43
00:01:53,427 --> 00:01:56,237
because I want anyone
to be able to find it.

44
00:01:56,237 --> 00:02:00,237
If you think you have got some good ideas,
you should put it on the Debian Wiki too:

45
00:02:00,237 --> 00:02:05,007
other people can take advantage of the
ideas that you have got.

46
00:02:07,577 --> 00:02:11,087
First baseline is:
I really like revision control.

47
00:02:11,097 --> 00:02:14,267
And I know that it makes me
a certain flavor on nerd,

48
00:02:14,267 --> 00:02:19,007
but when we are working with things that
are as complicated as software packages,

49
00:02:19,010 --> 00:02:19,890
hmmm…

50
00:02:22,590 --> 00:02:25,430
I think a lot of people don't get
that in Debian you are not

51
00:02:25,430 --> 00:02:27,160
just working on one
software package:

52
00:02:27,160 --> 00:02:30,120
you are actually probably, if you
are doing a responsibly work,

53
00:02:30,120 --> 00:02:33,010
on at least two software packages,
and maybe 5.

54
00:02:33,010 --> 00:02:35,150
So you have got the version
that is unstable,

55
00:02:35,150 --> 00:02:39,260
and you have got the version that you
try to maintain for stable as well.

56
00:02:39,260 --> 00:02:45,170
And we are committing to
doing maintenance work.

57
00:02:45,170 --> 00:02:52,390
A lot of our work in the project is
janitorial in nature:

58
00:02:52,390 --> 00:02:55,490
we want to clean up the mess and
we want us to stay out of the way

59
00:02:55,490 --> 00:02:57,450
and make sure things work,
functionally,

60
00:02:57,450 --> 00:03:00,970
for people who are relying on the
operating system to not get in their way.

61
00:03:00,970 --> 00:03:04,260
So revision control I think is really
helpful because it means you can

62
00:03:04,260 --> 00:03:07,910
keep track of what changes you have done
on different branches of the project

63
00:03:07,910 --> 00:03:09,800
while you are maintaining both of them.

64
00:03:09,800 --> 00:03:14,440
Basically, I'd require working with
the revision system I am comfortable with,

65
00:03:14,440 --> 00:03:17,740
I prefer Git, I am not going to have a
religious war about it.

66
00:03:17,740 --> 00:03:21,290
If upstream uses Git, I am
even happier, and I try to make

67
00:03:21,290 --> 00:03:25,680
my packaging depend on
upstream's revision control.

68
00:03:29,342 --> 00:03:34,652
I like to use 'git-buildpackage', and I
like to use it with debhelper.

69
00:03:34,652 --> 00:03:36,793
If you have not tried out
'git-buildpackage',

70
00:03:36,793 --> 00:03:39,883
we are going to have a
'git-buildpackage' skill share session

71
00:03:39,883 --> 00:03:43,883
later on today, and I welcome you
to come and share your tricks with it,

72
00:03:43,883 --> 00:03:46,293
or learn some tricks from other people.

73
00:03:46,293 --> 00:03:50,483
It is a particular way that you can keep
your Debian packaging in a Git repository,

74
00:03:50,483 --> 00:03:55,893
and it helps you to keep track of all of
the changes that have happened within

75
00:03:55,893 --> 00:03:59,293
your packaging and within upstream to
make sure you are not accidentally

76
00:03:59,293 --> 00:04:00,813
making other changes.

77
00:04:00,813 --> 00:04:03,683
So it is very easy to go back and
review what you have done.

78
00:04:03,683 --> 00:04:05,883
I find that really useful.

79
00:04:05,883 --> 00:04:09,233
I definitely also like to keep
upstream's source code

80
00:04:09,233 --> 00:04:11,063
in the same revision control system.

81
00:04:11,063 --> 00:04:13,823
I like to keep the tarballs in the
revision control system

82
00:04:13,823 --> 00:04:16,033
because it means that
if someone is interested,

83
00:04:16,033 --> 00:04:18,243
they can uses a tool called
'debcheckout'.

84
00:04:18,243 --> 00:04:20,713
You can use 'debcheckout' with
a name of a package:

85
00:04:20,713 --> 00:04:23,383
you just say "I am really
interested in package 'foo',

86
00:04:23,383 --> 00:04:27,053
let me see the source code for that":
'debcheckout foo'

87
00:04:27,053 --> 00:04:30,373
You get the source code, and
you get the source code…

88
00:04:30,373 --> 00:04:32,853
from a revision control system
that you can now track

89
00:04:32,853 --> 00:04:36,243
and you can just propose changes on.

90
00:04:37,267 --> 00:04:40,987
You can also extract the tarball from that
revision control system.

91
00:04:40,987 --> 00:04:44,627
'debcheckout' actually works even if you
do not have upstream stuff in there,

92
00:04:44,627 --> 00:04:47,327
but I like to keep it all in one
revision control system,

93
00:04:47,327 --> 00:04:50,427
it is just easier to find everything
when you want.

94
00:04:50,707 --> 00:04:53,697
Some of these things that
I prefer have to do with

95
00:04:53,697 --> 00:04:55,847
what the upstream software
developer has done,

96
00:04:55,847 --> 00:04:59,587
so I am less inclined to try the package,
an upstream software project,

97
00:04:59,587 --> 00:05:01,777
if they just throw tarballs
here over the wall

98
00:05:01,777 --> 00:05:03,437
to an FTP side every now and then.

99
00:05:03,437 --> 00:05:06,337
It makes it more difficult for me to
know what they are doing,

100
00:05:06,337 --> 00:05:07,577
and why they are doing it.

101
00:05:07,577 --> 00:05:10,317
So i like it, I have already said,
when upstream uses Git,

102
00:05:10,317 --> 00:05:12,657
I also like it when upstream
signs their releases,

103
00:05:12,657 --> 00:05:15,217
and says "hey, this is specific release",

104
00:05:15,217 --> 00:05:18,767
because that is a signal
that I can use,

105
00:05:18,767 --> 00:05:21,887
or somebody else that
understands the project.

106
00:05:22,137 --> 00:05:25,427
As said, "we think that this is
something that other people can use",

107
00:05:25,427 --> 00:05:28,927
or "this is a particular version that
we would like other people to test".

108
00:05:28,927 --> 00:05:32,257
There are a lot of other situations where
maybe it is not so important.

109
00:05:32,257 --> 00:05:35,127
And having that be cryptographically
signed is really useful.

110
00:05:35,127 --> 00:05:38,727
I care about cryptographic signature on
software because I want to know that

111
00:05:38,727 --> 00:05:44,027
what I am running is related to the code
that somebody else out should be run.

112
00:05:44,027 --> 00:05:46,717
And if you don't verify your
software cryptographically,

113
00:05:46,717 --> 00:05:48,907
anyone who can intercept
the network connection

114
00:05:48,907 --> 00:05:52,520
between you and that software, can
modify the software before it gets to you.

115
00:05:52,520 --> 00:05:54,560
And the cryptographic signature just says:

116
00:05:54,560 --> 00:05:58,120
"look, this is a version
that I am OK with.

117
00:05:58,120 --> 00:06:00,390
I am putting it out there
and it comes from me".

118
00:06:00,390 --> 00:06:03,710
So I can have a trace back
to that point.

119
00:06:06,383 --> 00:06:10,573
Just let me talk briefly about how
you do cryptographic verification

120
00:06:10,573 --> 00:06:13,313
of upstream
One is you might know upstream:

121
00:06:13,313 --> 00:06:16,943
you might know them personally, you
know their key already, that is fine.

122
00:06:16,943 --> 00:06:20,523
That is not the usual case:
we work on the Internet.

123
00:06:20,751 --> 00:06:23,661
In the situation where your upstream
is signing their tarballs

124
00:06:23,661 --> 00:06:26,521
and you have not met them,
you do not have to sign their key,

125
00:06:26,521 --> 00:06:29,051
you do not have to say
"I announce this is their key".

126
00:06:29,051 --> 00:06:32,562
But it is probably the same one
that is signing every release,

127
00:06:32,562 --> 00:06:34,162
so you should keep track of that.

128
00:06:34,162 --> 00:06:37,802
Debian has a nice way to
keep track of that:

129
00:06:37,802 --> 00:06:41,982
you can tell Debian how to find the new
version of the upstream tarball.

130
00:06:41,982 --> 00:06:43,722
This is in the Debian 'watch' file.

131
00:06:43,722 --> 00:06:49,532
If you type 'man uscan', you can learn
more about Debian 'watch',

132
00:06:49,532 --> 00:06:52,092
and Debian 'watch' now has
a feature that lets you say

133
00:06:52,092 --> 00:06:54,772
"that is not only this way
you find the tarball,

134
00:06:54,772 --> 00:06:58,492
but upstream publishes signatures
and the signatures look like this".

135
00:06:58,492 --> 00:07:00,602
You know, they got a '.sig' at the end.

136
00:07:00,602 --> 00:07:04,862
So there is a particular arcane way to
specify that, but if you specify that,

137
00:07:04,862 --> 00:07:07,342
then 'uscan' can find
not only the upstream tarball,

138
00:07:07,342 --> 00:07:09,262
it can find the upstream signature.

139
00:07:09,262 --> 00:07:12,472
And, if you drop
upstream's signing key

140
00:07:12,472 --> 00:07:14,802
- which of course I did not
put on the wiki page,

141
00:07:14,802 --> 00:07:16,592
someone should
edit that and fix it -

142
00:07:16,592 --> 00:07:24,192
you can put the upstream signing
key in 'debian/upstream/signing-key.asc'.

143
00:07:25,982 --> 00:07:29,982
And then if you do that, when you say
'uscan', you can tell…

144
00:07:31,684 --> 00:07:34,214
Maybe some people here do not
know how to use 'uscan':

145
00:07:34,214 --> 00:07:35,644
'uscan' is a very simple tool,

146
00:07:35,644 --> 00:07:39,044
you run it from a software package
that has a 'debian' directory,

147
00:07:39,044 --> 00:07:43,084
or even one level up if you keep all of
your software packages in one folder.

148
00:07:43,084 --> 00:07:46,094
You can go one level up
and say 'uscan',

149
00:07:46,094 --> 00:07:49,594
and it will look in all of the folders
that are children of it,

150
00:07:49,594 --> 00:07:50,924
and look for new versions

151
00:07:50,924 --> 00:07:54,274
by trying to find a new upstream
version in 'debian/watch'.

152
00:07:54,274 --> 00:07:56,654
And if you have configured
'debian/watch' properly,

153
00:07:56,654 --> 00:07:58,584
it can find the new upstream signatures,

154
00:07:58,584 --> 00:08:02,084
and if you have got the
'upstream/signing-key.asc',

155
00:08:02,084 --> 00:08:04,484
then it will actually verify
the signatures for you

156
00:08:04,484 --> 00:08:06,674
as part of fetching the
new upstream tarball.

157
00:08:06,674 --> 00:08:10,364
So you can get all of those things just
by setting a pre-packaging that way.

158
00:08:10,364 --> 00:08:13,411
There is a hand up down there, could we
get the mic down to the hand ?

159
00:08:14,151 --> 00:08:15,101
Thanks.

160
00:08:15,101 --> 00:08:18,561
Or to the person who has that hand,
it is not just a hand.

161
00:08:18,561 --> 00:08:20,661
[public laugh]

162
00:08:21,381 --> 00:08:27,221
[attendee] Publish a tarball,
and a hash, '.sha1',

163
00:08:27,221 --> 00:08:31,751
and sign that hash,
'.sha1.asc'.

164
00:08:31,751 --> 00:08:36,137
Can 'uscan' cope with this and
check the signature on the hash

165
00:08:36,137 --> 00:08:38,397
and that the hash belongs
to that tarball ?

166
00:08:38,397 --> 00:08:41,357
[Daniel] I do not believe that 'uscan'
can do that currently.

167
00:08:41,357 --> 00:08:44,967
So anybody out there who wants to
make things better for the world

168
00:08:44,967 --> 00:08:48,067
should go hack on 'uscan': that is a
pretty straightforward thing

169
00:08:48,067 --> 00:08:51,137
that we should fix because
I agree that is common pattern.

170
00:08:53,715 --> 00:08:57,955
[attendee] I have no answer to this
question by I have another question:

171
00:08:57,955 --> 00:09:01,895
how do you convince upstreams
who do not release tarballs

172
00:09:01,895 --> 00:09:06,485
or who do not set tags in Git ?

173
00:09:06,485 --> 00:09:08,455
[Daniel] Who do not make tags in Git ?

174
00:09:08,455 --> 00:09:11,695
[someone] Yes, if there is no tags
you can not check out a tarball.

175
00:09:11,695 --> 00:09:15,795
Is there any good way to
convince upstream to do this ?

176
00:09:17,369 --> 00:09:20,849
[Daniel] Git has this nice feature, which
is that you can create a tag,

177
00:09:20,849 --> 00:09:23,729
which is associated with
a particular revision,

178
00:09:23,729 --> 00:09:26,979
and you would like to have a tag

179
00:09:26,979 --> 00:09:30,979
everywhere that a tarball
has been released from.

180
00:09:30,979 --> 00:09:34,979
I am tempted to pull up a Git
view and show people some tags.

181
00:09:34,979 --> 00:09:38,979
The question that you ask is a
social one tho, not just a technical one,

182
00:09:38,979 --> 00:09:42,289
and I actually find that my upstreams
are pretty responsive.

183
00:09:42,289 --> 00:09:44,509
Usually I frame my request as

184
00:09:44,509 --> 00:09:48,759
"hey, it like you made this tarball
from this particular commit 'id'.

185
00:09:48,759 --> 00:09:52,259
If you could tag you releases,
it would be really helpful to me,

186
00:09:52,259 --> 00:09:55,429
and here is the command
that I would use to tag the release".

187
00:09:55,429 --> 00:09:57,199
And I say "git tag…"

188
00:09:57,199 --> 00:09:59,939
and of course I can never
remember so first I look it up,

189
00:09:59,939 --> 00:10:03,039
but it is either 'tag name' 'commit id'
or 'commit id' 'tag name'.

190
00:10:03,039 --> 00:10:05,759
But I would look it up and
I would write the email so that

191
00:10:05,759 --> 00:10:07,489
all they have to do is they read it,

192
00:10:07,489 --> 00:10:08,599
understand my argument,

193
00:10:08,599 --> 00:10:09,829
and execute one command.

194
00:10:09,829 --> 00:10:13,299
I mean, it doesn't get them in the habit
but it start them towards it

195
00:10:16,239 --> 00:10:19,009
And if you say 'tag -s',

196
00:10:19,009 --> 00:10:23,179
then your tag will be
cryptographically signed,

197
00:10:23,179 --> 00:10:26,049
which I think is a really
good thing to do too.

198
00:10:27,179 --> 00:10:29,299
So, cryptographic verification
of upstream.

199
00:10:29,299 --> 00:10:33,649
As I said, I want to keep upstream's code
in the revision control system.

200
00:10:33,649 --> 00:10:35,559
I also like to keep…

201
00:10:35,559 --> 00:10:38,975
In my ideal case upstream is using Git:
I am using Git for packaging.

202
00:10:38,975 --> 00:10:44,575
I actually like to keep upsteam's Git
history fully in my repository,

203
00:10:44,575 --> 00:10:48,705
so that I do not just have the tarballs,
but I actually have all of their commits.

204
00:10:48,705 --> 00:10:52,137
And that turns out to be really useful
for two specific cases:

205
00:10:52,137 --> 00:10:55,557
In one case, there is a common scenario
where upstream will fix a bug,

206
00:10:55,557 --> 00:10:57,667
but they have not made a release yet.

207
00:10:57,667 --> 00:11:00,417
And that bug is really,
really obviously problematic

208
00:11:00,417 --> 00:11:02,197
for the folks who are using Debian,

209
00:11:02,197 --> 00:11:03,457
so I want to fix it.

210
00:11:03,457 --> 00:11:06,407
All I can do, because I have
their full revision history,

211
00:11:06,407 --> 00:11:10,477
I can use Git to "cherry pick"
the upstream commit.

212
00:11:10,477 --> 00:11:12,577
And then I "cherry pick"
that upstream commit

213
00:11:12,577 --> 00:11:14,767
and I can have it applied separately,

214
00:11:14,767 --> 00:11:17,107
and release a Debian version
that has the fix,

215
00:11:17,107 --> 00:11:19,747
even before upstream has made
a release with the fix.

216
00:11:19,747 --> 00:11:22,777
So one nice thing about
having upstream revision

217
00:11:22,777 --> 00:11:28,667
is that I can pull fixes from upstream
before they decided to release it.

218
00:11:28,667 --> 00:11:32,667
The other advantage is the other
way around.

219
00:11:32,667 --> 00:11:35,517
Often when I am doing packaging,
I discover a problem,

220
00:11:35,517 --> 00:11:37,787
and maybe I can fix the problem.

221
00:11:37,787 --> 00:11:41,787
And in fact maybe I am already shipping
a Debian package that fixes the problem.

222
00:11:41,787 --> 00:11:45,027
If my Debian fixes can be
directly applied to upstream,

223
00:11:45,027 --> 00:11:50,017
then I can use whatever their preferred
upstream patch submission guidelines are,

224
00:11:50,017 --> 00:11:53,677
whether it is a Github pull request,
or a patch to a mailing list,

225
00:11:53,677 --> 00:11:58,377
or a "hey can you pull this from my Git
repository over here", e-mail…

226
00:11:58,377 --> 00:12:01,590
The fact that I am using the same
Git history that they are using

227
00:12:01,590 --> 00:12:05,520
makes it much easier for me
to push my changes back to them.

228
00:12:05,520 --> 00:12:09,311
So, it sort of smooths the interaction
if you can consolidate

229
00:12:09,311 --> 00:12:13,441
and use the same revision control
system as their.

230
00:12:13,441 --> 00:12:14,661
Towards that aim,

231
00:12:14,661 --> 00:12:16,691
I use a system now
called 'patch queue',

232
00:12:16,691 --> 00:12:19,141
which is part of 'git-buildpackage'.

233
00:12:19,141 --> 00:12:21,521
So 'git buildpackage' is 'gbp',

234
00:12:21,521 --> 00:12:23,321
'patch queue' is 'pq',

235
00:12:23,321 --> 00:12:28,784
so to deal with 'patch queue'
you say 'gbp pq'

236
00:12:28,784 --> 00:12:30,644
and then you have some commands.

237
00:12:30,644 --> 00:12:34,067
And what that does, is it takes…

238
00:12:34,067 --> 00:12:36,087
How many of you are Debian packagers ?

239
00:12:36,087 --> 00:12:38,187
How many of you package
software for Debian ?

240
00:12:38,187 --> 00:12:40,097
[most attendees raise their hand]

241
00:12:40,097 --> 00:12:42,097
A very large percentage, but not everyone.

242
00:12:42,097 --> 00:12:46,557
I hope some folks are considering starting
packaging if you have not done it yet.

243
00:12:46,557 --> 00:12:48,617
Of those of you who package software,

244
00:12:48,617 --> 00:12:51,057
how many of you package software
with modifications,

245
00:12:51,057 --> 00:12:53,917
how many of you ship a
modified version of upstream sources ?

246
00:12:53,917 --> 00:12:55,597
[most attendees raise their hand]

247
00:12:55,597 --> 00:12:58,427
Beyond the 'debian' directory,
just Debian patches ?

248
00:12:58,427 --> 00:13:00,757
So the common way to do that,

249
00:13:00,757 --> 00:13:03,407
for the Debian 3.0 quilt
packaging skill,

250
00:13:03,407 --> 00:13:07,088
is that in your 'debian' directory you
have a 'patches' sub-directory

251
00:13:07,088 --> 00:13:11,038
that has a set of individual patches
that apply certain changes,

252
00:13:11,038 --> 00:13:15,971
and they are applied in order based on
the file called 'debian/patches/series'.

253
00:13:15,971 --> 00:13:20,659
So maintaining that is kind of a drag
when upstream makes big changes:

254
00:13:20,659 --> 00:13:24,449
then all of sudden you have got this set
of patches and they do not quite apply…

255
00:13:24,449 --> 00:13:28,424
It is a drag even you do not have it
in the 'debian/patches/' directory.

256
00:13:28,424 --> 00:13:33,984
But what Debian 'patch queue' does is
it maps that directory of patches

257
00:13:33,984 --> 00:13:38,314
into a little branch on your
Git revision history.

258
00:13:38,314 --> 00:13:43,164
So when you get a new upstream version,
you can say 'patch queue rebase',

259
00:13:43,164 --> 00:13:47,067
and it treats it just as Git:
it takes the 'patch queue'…

260
00:13:47,067 --> 00:13:51,537
You have already imported the new version,
and it re-applies your patches,

261
00:13:51,537 --> 00:13:53,777
and sometimes that means
some minor adjustments.

262
00:13:53,787 --> 00:13:58,417
Git is really good at figuring out what
the right minor adjustments are to make,

263
00:13:58,417 --> 00:14:01,587
and so all of the sudden
the patch queue is re-based,

264
00:14:01,587 --> 00:14:05,587
you refresh it in your revision
control system…

265
00:14:05,587 --> 00:14:08,217
and there you go.

266
00:14:08,217 --> 00:14:11,810
So I like to use
git-buildpackage patch queue,

267
00:14:11,810 --> 00:14:14,830
tagging, as already brought up,
thank you for that,

268
00:14:14,830 --> 00:14:17,010
I like to to tag
everything that I release,

269
00:14:17,010 --> 00:14:18,730
I like to push that
as soon as I can,

270
00:14:18,730 --> 00:14:21,740
so that other people
who are following my work

271
00:14:21,740 --> 00:14:24,780
can know where my releases
come from.

272
00:14:24,780 --> 00:14:27,430
The reason that I like other people
following my work is

273
00:14:27,430 --> 00:14:30,616
they can fix my bugs easier.

274
00:14:30,616 --> 00:14:32,636
I make mistakes,
everybody makes mistakes,

275
00:14:32,636 --> 00:14:36,220
and it is really important to me that
if someone catches one of my mistakes,

276
00:14:36,220 --> 00:14:39,416
I can accept their feedback,
their criticism, their improvements,

277
00:14:39,416 --> 00:14:41,226
as easily as possible.

278
00:14:41,226 --> 00:14:45,606
I want a low barrier to entry for people
to help me fix my problems:

279
00:14:45,606 --> 00:14:46,826
it is selfishness.

280
00:14:46,826 --> 00:14:49,956
So I try to patch it and publish these
things for people can find it.

281
00:14:49,956 --> 00:14:54,516
I'm going to rattle through some of these pretty
fast because were are almost out of time.

282
00:14:54,516 --> 00:14:57,656
I like to put my repo in some place
where other people get to the them,

283
00:14:57,656 --> 00:15:00,346
at the moment I like to put them in
'collab-maint',

284
00:15:00,346 --> 00:15:03,636
it has some problems but it is better
than not publishing your stuff,

285
00:15:03,636 --> 00:15:06,501
and it is nice because it is sort of
a public use.

286
00:15:07,777 --> 00:15:10,167
I like to standardize how of
my branches are named,

287
00:15:10,167 --> 00:15:13,187
so if I am working on something
that has got a stable version,

288
00:15:13,187 --> 00:15:15,697
that is for Jessie, I will
name the branch 'jessie',

289
00:15:15,697 --> 00:15:21,317
because I will probably making changes,
like I said, editing multiple of software

290
00:15:21,317 --> 00:15:26,147
I try to push as frequently as I have made
something that looks sensible.

291
00:15:26,147 --> 00:15:29,737
I do not feel obliged to push
my commits to a public repository

292
00:15:29,737 --> 00:15:31,347
when I am still experimenting,

293
00:15:31,347 --> 00:15:33,167
I actually really like to
experiment,

294
00:15:33,167 --> 00:15:36,847
and I also like to keep track of my
experiments while I am doing them.

295
00:15:36,847 --> 00:15:40,067
So I try to push when there is
a sensible set of changes,

296
00:15:40,067 --> 00:15:41,917
and I am trying to get myself

297
00:15:41,917 --> 00:15:45,317
to a point where I can understand
what I have done, even if it is wrong.

298
00:15:45,317 --> 00:15:48,097
If I can get myself to a conceptual
point where it is done,

299
00:15:48,097 --> 00:15:51,287
I will push my changes so other people
can see what I am working on,

300
00:15:51,287 --> 00:15:52,627
and then work from there.

301
00:15:52,627 --> 00:15:54,877
That is OK to push something
that is wrong,

302
00:15:54,877 --> 00:15:57,907
as long as you push something that
people can understand.

303
00:15:57,907 --> 00:16:01,277
When you make a 'git commit'
(if you are working with Git),

304
00:16:01,277 --> 00:16:05,167
one of the things that helps me to think
for commit messages…

305
00:16:05,167 --> 00:16:08,637
People often think that commit messages
should say "what change you made".

306
00:16:08,647 --> 00:16:12,117
I think that the 'git patch' shows what
change what change you have made,

307
00:16:12,117 --> 00:16:16,677
and I thin your commit messages should
say "why you made the change".

308
00:16:16,677 --> 00:16:19,017
That is what people really want to read.

309
00:16:19,017 --> 00:16:22,977
If you need to explain technically
why the thing that you did

310
00:16:22,977 --> 00:16:25,677
maps to the conceptual thing
that you wanted to do,

311
00:16:25,677 --> 00:16:28,117
that is fine: do that in
your commit message too.

312
00:16:28,117 --> 00:16:31,067
But it is really important to say
why you made the change.

313
00:16:31,067 --> 00:16:34,287
It is not just like
"initialize variable to 'no'":

314
00:16:34,287 --> 00:16:36,405
OK, we can see that from the patch,

315
00:16:36,405 --> 00:16:39,815
but what you are really saying is
"there was a crash if someone did 'x',

316
00:16:39,815 --> 00:16:43,925
and we are avoiding that crash by
setting this to 'no'.

317
00:16:43,925 --> 00:16:46,345
So I like to send patches via email,

318
00:16:46,345 --> 00:16:47,985
so I try to configure Git email,

319
00:16:47,985 --> 00:16:51,565
which make it really easy to just
push patches back upstream.

320
00:16:51,565 --> 00:16:55,195
If I am starting taking over a project
that somebody else has past on,

321
00:16:55,195 --> 00:16:58,345
and they did not use Git,
I will try to restore all of the imports.

322
00:16:58,345 --> 00:17:01,065
I would be happy to talk with people
about how to do that,

323
00:17:01,065 --> 00:17:02,795
if you have questions come find me.

324
00:17:02,795 --> 00:17:04,525
I like to keep my files
nice and simple:

325
00:17:04,525 --> 00:17:09,965
there is a tool 'wrap-and-sort',

326
00:17:09,965 --> 00:17:13,585
that just canonicalizes your files
to make them look

327
00:17:13,585 --> 00:17:16,395
in a simple and sensible way.

328
00:17:16,395 --> 00:17:20,575
And it is nice because it
means that everything is…

329
00:17:20,575 --> 00:17:23,675
It does things like alphabetize
your list of build depends,

330
00:17:23,675 --> 00:17:25,555
and brake them out one per line.

331
00:17:25,555 --> 00:17:29,054
The nice thing about that,
since you are using revision control,

332
00:17:29,054 --> 00:17:31,284
when you make a change
to your build depends,

333
00:17:31,284 --> 00:17:32,954
the changes become
very easy to see:

334
00:17:32,954 --> 00:17:35,514
"oh, they added one new package here,
there is a single '+'".

335
00:17:35,514 --> 00:17:37,711
One new dependency, they removed
a build dependency

336
00:17:37,711 --> 00:17:40,051
so you can see that kind of thing.

337
00:17:40,051 --> 00:17:44,851
I like to use DEP-5 to format
Debian copyright to be machine readable,

338
00:17:44,851 --> 00:17:47,701
it is nice for people who are
doing scans of the archive

339
00:17:47,701 --> 00:17:49,791
and try reason about
what the patterns are,

340
00:17:49,791 --> 00:17:51,275
and licensing of free software.

341
00:17:51,275 --> 00:17:54,905
And if I am doing something really crazy,
that is going to make a big change,

342
00:17:54,905 --> 00:17:57,325
I like to use a feature branch in
revision control.

343
00:17:57,325 --> 00:18:01,705
So we have got one minute left,
I want to open it up for other questions.

344
00:18:01,705 --> 00:18:05,705
it's kind of rambling

345
00:18:10,330 --> 00:18:13,764
[attendee] You said you are using
'wrap-and-sort' which is nice,

346
00:18:13,764 --> 00:18:18,931
I had learned that Config::Model editors
- 'cme' - do the same job,

347
00:18:18,931 --> 00:18:25,541
and somehow does a better job:
it also enhances standard version

348
00:18:25,541 --> 00:18:31,191
if it does not fit, or it makes VCS
fields properly has it should be.

349
00:18:31,191 --> 00:18:36,761
'cme fix dpkg-control' fixes your
control file.

350
00:18:36,761 --> 00:18:40,131
[Daniel] 'cme' ? And it is in
what package ?

351
00:18:40,131 --> 00:18:42,921
[attendee] The package 'cme', in
unstable is cme

352
00:18:42,921 --> 00:18:45,331
In Jessie it's libconfig-model-perl

353
00:18:45,331 --> 00:18:47,763
[Daniel] You are developing in unstable,
that is OK.

354
00:18:47,763 --> 00:18:49,783
'cme'
OK, thank you.

355
00:18:49,783 --> 00:18:53,381
Other questions or suggestions,
or complains ?

356
00:18:56,793 --> 00:19:01,664
[attendee] If you change the original
source code, and do some commits,

357
00:19:01,664 --> 00:19:06,814
how do you convert that into a series
of quilt patches ?

358
00:19:06,818 --> 00:19:09,838
[Daniel] I use 'patch queue' for that
as well, so what I do is I say

359
00:19:09,838 --> 00:19:12,788
"I want to move over to my 
'patch queue' view of the tree",

360
00:19:12,788 --> 00:19:15,028
and then I make may changes, I make
my commits,

361
00:19:15,028 --> 00:19:18,058
and then I say
'gbp pq export',

362
00:19:18,058 --> 00:19:19,733
so that 'patch queue export',

363
00:19:19,733 --> 00:19:21,563
and it takes the 'patch queue'
that I am on

364
00:19:21,563 --> 00:19:24,263
and dumps it back into
the Debian patches directory.

365
00:19:24,263 --> 00:19:28,263
If you have not use 'gbp pq', I
recommend looking into it.

366
00:19:28,263 --> 00:19:31,913
It takes a little while to get used to,
and I still screwed it up sometimes,

367
00:19:31,913 --> 00:19:34,433
but it makes easy to fix your
mistakes too.

368
00:19:34,433 --> 00:19:36,833
[organizer] Last question ?

369
00:19:36,833 --> 00:19:38,673
[attendee] Do you think it is possible

370
00:19:38,673 --> 00:19:42,473
to make this 'patch queue' branch
"pullable" by upstream ?

371
00:19:45,846 --> 00:19:49,056
[Daniel] I do not actually think it is
possible to make it directly

372
00:19:49,056 --> 00:19:52,536
"pullable" by upstream: I think upstream
can cherry pick patches from it,

373
00:19:52,536 --> 00:19:54,886
but I do not see how to
make it "pullable".

374
00:19:54,886 --> 00:19:58,156
If someone else does, I would be
happy to learn.

375
00:19:58,691 --> 00:20:02,721
[organizer] This was "before last",
so last.

376
00:20:02,721 --> 00:20:07,601
[attendee] Do you have a recording of
you using the tools that you mentioned,

377
00:20:07,601 --> 00:20:12,151
a video recording would be great,
just to show your workflow ?

378
00:20:12,151 --> 00:20:15,441
[Daniel] I do not really know how
to do that:

379
00:20:15,441 --> 00:20:20,321
if somebody wants to help me do that
I would be happy to do it.

380
00:20:20,321 --> 00:20:22,311
I am going to give one last plug,

381
00:20:22,311 --> 00:20:25,595
I know we are out of time here,
sorry.

382
00:20:25,595 --> 00:20:28,795
This tool is called 'gitk'.

383
00:20:28,795 --> 00:20:31,105
This is an example…

384
00:20:31,105 --> 00:20:32,415
- sorry we should leave -

385
00:20:32,415 --> 00:20:35,965
but this the way that I visualize
my revision control system.

386
00:20:35,965 --> 00:20:38,195
We could do a whole other session
about 'gitk'.

387
00:20:38,195 --> 00:20:41,045
If you do not try to visualize your
revision control system,

388
00:20:41,045 --> 00:20:42,085
you are missing out:

389
00:20:42,085 --> 00:20:44,385
I recommend try to find a way
to visualize stuff,

390
00:20:44,385 --> 00:20:45,725
find one that works for you.

391
00:20:45,725 --> 00:20:46,725
Thanks for coming.

392
00:20:46,725 --> 00:20:48,835
[organizer] Thank you.
