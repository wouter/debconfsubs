1
00:42:26,101 --> 00:42:28,441
So this is a rather short presentation

2
00:42:28,441 --> 00:42:31,281
on 7 slides. It was originally planned

3
00:42:31,281 --> 00:42:36,480
for a lightning talk, but i managed to

4
00:42:36,480 --> 00:42:49,361
hand in my proposal too late, so....

5
00:42:49,361 --> 00:42:53,221
Ok, so i am presenting today:

6
00:42:53,221 --> 00:42:54,562
DUCK, the Debian URL checker.

7
00:42:54,562 --> 00:43:02,901
The reason why i made this is exactly this:

8
00:43:02,901 --> 00:43:09,382
You see a debcheckout here, with bugs

9
00:43:09,382 --> 00:43:13,221
showing that it basically does not work.

10
00:43:13,221 --> 00:43:16,901
This was in a time where i tried to increase

11
00:43:16,901 --> 00:43:20,321
my involvement into Debian, and i felt like:

12
00:43:20,321 --> 00:43:25,801
This should be fixed somehow.

13
00:43:25,801 --> 00:43:29,640
In the debian/control file are several URLs defined

14
00:43:29,640 --> 00:43:31,781
e.g. ths Vcs-Git: URL, or the Vcs-Browser: URL,

15
00:43:31,781 --> 00:43:36,101
which in this case shows me:

16
00:43:36,101 --> 00:43:38,701
"No repository found". Probabyl you have already

17
00:43:38,701 --> 00:43:41,181
seen such a screen.

18
00:43:41,181 --> 00:43:43,761
So the basic idea behind DUCK is:

19
00:43:43,761 --> 00:43:46,460
I check those links and entries in the control-file

20
00:43:46,460 --> 00:43:49,581
on a daily basis. Currently we have about ~2300

21
00:43:49,581 --> 00:43:51,881
packages which have broken URLs. These are eiter

22
00:43:51,881 --> 00:43:56,641
homepages, or problems with the email addresses

23
00:43:56,641 --> 00:44:00,101
of the maintainers or some other problems with

24
00:44:00,101 --> 00:44:03,220
URLs. There is also a package availabe which you

25
00:44:03,220 --> 00:44:08,281
can run locally and also show you informations about problems with links.

26
00:44:08,281 --> 00:44:11,941
I also made a challenge. So if you fix one of

27
00:44:11,941 --> 00:44:14,000
your packages until the end of DebConf 15

28
00:44:14,000 --> 00:44:16,541
you get one of those awsome lighters. I know this is

29
00:44:16,541 --> 00:44:19,401
a very short time announcement , but i have still a

30
00:44:19,401 --> 00:44:22,221
bag of lighters here. And if your really promise me

31
00:44:22,221 --> 00:44:23,440
hard to fix your stuff you can take one out.

32
00:44:23,440 --> 00:44:25,241
Thank you!
