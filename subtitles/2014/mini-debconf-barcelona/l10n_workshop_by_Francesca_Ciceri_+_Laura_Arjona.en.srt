1
00:00:00,811 --> 00:00:03,001
My name is Laura Arjona and

2
00:00:04,463 --> 00:00:07,181
my name is Francesca Ciceri

3
00:00:07,521 --> 00:00:11,576
And this is the localization workshop

4
00:00:13,176 --> 00:00:20,070
Localization or translation. We say 
localization because its the process

5
00:00:20,070 --> 00:00:25,983
that envolve a bit more than 
translation. Maybe you can also need

6
00:00:25,983 --> 00:00:34,330
to change date formats or currency 
formats or the text orientation to have a

7
00:00:34,330 --> 00:00:37,252
software in your own language. Ok?

8
00:00:40,046 --> 00:00:46,534
For this to be done the software 
has to be prepared and this process

9
00:00:46,534 --> 00:00:51,088
of preparing the software is internalization.
Is a software engineering process

10
00:00:51,088 --> 00:01:01,307
to make a program ready for translation. 
So people not beeing programers can

11
00:01:01,307 --> 00:01:03,289
translate the software.

12
00:01:06,129 --> 00:01:12,612
And debian is very well internationalized
so we have lots of things we can translate

13
00:01:12,612 --> 00:01:15,576
without even touching a line of code.

14
00:01:16,223 --> 00:01:22,010
The debian installer, the debconf 
templates that Miriam talked a bit ago,

15
00:01:24,156 --> 00:01:31,656
the descriptions of the packages, when 
you search for package website or in the

16
00:01:31,656 --> 00:01:37,366
software center or in synaptics or
anything, the manuals documentation

17
00:01:37,575 --> 00:01:44,825
of debian I mean, and of course the
information that the Debian Project

18
00:01:45,197 --> 00:01:52,877
produces, the website, the press releases,
the debian project news, wiki, everything

19
00:01:55,266 --> 00:02:00,670
So for many people the most important 
thing is the debian installer.

20
00:02:00,670 --> 00:02:06,057
That you can install debian, many people 
dont know english or they dont want to use

21
00:02:06,546 --> 00:02:13,059
english, so its nice that debian installer
is in their own language and thanks for

22
00:02:13,466 --> 00:02:17,810
Christian Perrier and all the people in 
the different language teams,

23
00:02:18,255 --> 00:02:24,542
we have the installer in many languages
and we are still improving that, ok ?

24
00:02:30,405 --> 00:02:32,725
No? Yeah! this is your turn ...

25
00:02:33,689 --> 00:02:39,364
Thank you, ok! Then there are debconf
template, Miriam told about debconf

26
00:02:39,892 --> 00:02:46,121
messages in the previous talk, and they 
are messages sent by the system when you

27
00:02:46,570 --> 00:02:54,059
are installing a new package and they just
need ask on a prompt to the user

28
00:02:55,949 --> 00:02:58,019
about configuring the package and so.

29
00:02:58,880 --> 00:03:11,947
And there are translatable files, po files
and you can use different tools to

30
00:03:11,947 --> 00:03:20,087
translate them, as the translation is done
by submitted directly to the bug tracking

31
00:03:20,087 --> 00:03:27,037
system - the BTS to the relevant package.
And there are stats ...

32
00:03:48,341 --> 00:03:50,355
We will do it ...

33
00:04:03,381 --> 00:04:06,430
My laptop is dying ...

34
00:04:10,685 --> 00:04:12,858
You have to support it ...

35
00:04:20,274 --> 00:04:23,882
Wait a minute we have a backup laptop...

36
00:04:24,342 --> 00:04:27,080
I'm back ...

37
00:04:39,282 --> 00:04:50,098
And here are the stats for the translation
of debconf messages from the different

38
00:04:50,098 --> 00:04:59,680
teams, you can see that Spanish is at 95 %
so you're not doing perfectly, but

39
00:04:59,680 --> 00:05:08,165
you're doing well, better than Italian at
least, and Catalan is 45% , you need to

40
00:05:08,165 --> 00:05:10,587
work more , really really. Ok

41
00:05:23,237 --> 00:05:29,968
You can also translate or help translating
the descriptions of the packages. And this

42
00:05:30,211 --> 00:05:38,562
is an interface very, very easy, you just
have the description in english and

43
00:05:38,889 --> 00:05:44,028
here's a text box where you put the same
description in your language, and press a

44
00:05:44,028 --> 00:05:49,750
button, submit and that's all. Later
another person can review it, and improve

45
00:05:49,750 --> 00:05:56,502
it, but its very easy, you don't have to 
do anything, just go to one webpage, read

46
00:05:56,502 --> 00:05:59,866
and translate to your language.

47
00:06:02,698 --> 00:06:08,298
There's other documents that we use to 
translate...

48
00:06:10,763 --> 00:06:15,264
The release notes and the installation 
guide which we translate them obviously

49
00:06:15,872 --> 00:06:24,023
with each new release,and there are other 
manuals that are not as much updated, so

50
00:06:24,307 --> 00:06:28,719
the translation is done there's no work 
to do.

51
00:06:29,493 --> 00:06:35,581
And this kind of documentation is managed
by the debian documentation project and

52
00:06:36,108 --> 00:06:43,442
has a specific workflow and a specific
repository, all this kind of translations:

53
00:06:44,252 --> 00:06:53,648
the debconf messages, the website, the 
documentation have their own workflow.

54
00:06:55,179 --> 00:07:01,269
They are managed sometimes by different
teams besides the local language team.

55
00:07:05,317 --> 00:07:11,764
And finally the website, I said before
that the installer was very important but

56
00:07:12,046 --> 00:07:18,452
the debian website is very important too,
because people need to go there to

57
00:07:18,859 --> 00:07:25,245
download debian and if they cannot 
understand the website probably they

58
00:07:25,246 --> 00:07:30,293
will not manage to download the iso file
and to install it.

59
00:07:31,870 --> 00:07:33,997
Also the website is something

60
00:07:38,224 --> 00:07:44,180
It's all the time producing new content
and updating content, so there is always

61
00:07:44,660 --> 00:07:49,010
work to be done in the website translation
team.

62
00:07:50,710 --> 00:07:52,900
And is also quite easy, just like

63
00:07:53,830 --> 00:07:58,756
someone said before, just read and send
emails, this is the same.

64
00:08:08,235 --> 00:08:16,085
The most important thing is to understand
the philosophy, its a team work, your

65
00:08:16,363 --> 00:08:22,476
work needs to be reviewed by other people
and you need to review the translations

66
00:08:22,849 --> 00:08:26,689
that other people do, if you do like that
everything goes very well.

67
00:08:27,375 --> 00:08:34,232
And we have also a robot (a bot) that 
understand emails the similar way

68
00:08:34,637 --> 00:08:40,556
sent to the bug tracking system for the 
translations we just sent that read the

69
00:08:41,196 --> 00:08:44,078
subject with the file that you want to
translate and what you going to do with

70
00:08:44,450 --> 00:08:50,447
that. The robot understands that and
generate the pages with the statistics

71
00:08:50,770 --> 00:08:56,111
and the situation of each file so 
everybody knows in each situation is the

72
00:08:56,476 --> 00:09:06,602
translation. As you understand as it works
its very easy to continue work that was

73
00:09:07,174 --> 00:09:16,856
done by other people. From the beginning
can be very strange but it works very well

74
00:09:23,484 --> 00:09:31,553
And there is obviously different tools for
the different kind of translation you're

75
00:09:32,567 --> 00:09:40,422
going to do. For the debconf messages you
can use some specific po editors, but I

76
00:09:40,754 --> 00:09:46,795
actually use a text editor, which is really
good as well i mean basically its a text

77
00:09:47,112 --> 00:09:56,966
file, so you can edit it with any text
editor. For the website we have again a

78
00:09:57,209 --> 00:10:03,873
text editor, but you need to have at least
a personal checkout of a repository of

79
00:10:03,878 --> 00:10:10,888
this site to build a page and check that
everything is ok (and I told also your part)

80
00:10:17,297 --> 00:10:28,892
About consistency in translation, well it
used to be at least at the Italian team

81
00:10:29,297 --> 00:10:38,665
we try to be consistent having a glossary
and keeping track of what is the

82
00:10:38,990 --> 00:10:48,149
translation specific for. But its really
dependent of the team. I have no idea

83
00:10:48,472 --> 00:10:52,150
about the Spanish and the Catalan teams.

84
00:10:52,878 --> 00:11:00,542
Normally each team has some rules or you
can ask for example with Spanish we have

85
00:11:00,785 --> 00:11:07,352
informal you and formal you, so when you 
need to translate you have to know that in

86
00:11:07,799 --> 00:11:10,068
debian we treat the you instead

87
00:11:10,635 --> 00:11:19,307
This kind of rules are normally discussed
in the mailing lists and sometimes a file

88
00:11:20,320 --> 00:11:25,792
is written with all the rules or the most
important and its uploaded to the website

89
00:11:26,156 --> 00:11:32,271
somewhere. So if you have that you can do
things, ask the mailing list or just read

90
00:11:32,398 --> 00:11:36,545
and try to follow the same style that you

91
00:11:36,545 --> 00:11:42,583
are already reading in your own language
If you come from another project maybe the

92
00:11:42,587 --> 00:11:47,759
other project has another kind of rules, so
If you are translating for debian ask first

93
00:11:48,206 --> 00:11:52,383
for those rules, if you don't agree with
the rules you can discuss about the rules,

94
00:11:52,416 --> 00:11:56,798
too in the mailing list and well good luck.

95
00:12:05,858 --> 00:12:10,449
Probably you would think that all this
translation is very complicated

96
00:12:10,692 --> 00:12:15,963
we are talking about debconf, po, mailing
with some strange subjects

97
00:12:18,449 --> 00:12:27,052
in fact, everything has the same philosophy
you just ask:" I want to translate this and

98
00:12:27,544 --> 00:12:33,059
then you send your translation as proposal,
other people comment about the translation

99
00:12:33,463 --> 00:12:39,986
Then you send the final or the one you
think its the final one and when everybody

100
00:12:40,391 --> 00:12:45,898
agrees to the translation somebody uploads
it to the website or BTS or to anyplace

101
00:12:46,384 --> 00:12:53,598
Its the same philosophy ask for reservation
I want to translate this, you translate

102
00:12:53,964 --> 00:13:00,368
at home. Then you send your proposal,
comments after that you send a last chance

103
00:13:00,571 --> 00:13:08,429
for comments, your final version and after
that you or a person with permission uploads

104
00:13:08,756 --> 00:13:20,712
the file to the correct repository and we
keep the difficult bureaucracy as debian

105
00:13:20,914 --> 00:13:27,887
is a very big project if you don't follow
some protocols at the end people work

106
00:13:28,131 --> 00:13:33,516
double so it's better to follow the
protocols and everything goes very well

107
00:13:34,936 --> 00:13:44,090
And you have to keep in mind we are a team
maybe other people have to finish your work

108
00:13:44,454 --> 00:13:49,551
because you start and later you don't
finish so if you follow the protocols other

109
00:13:49,753 --> 00:13:56,839
people can finish your work and you can
finish the work of other people pretty well

110
00:13:59,037 --> 00:14:05,398
That's what I say... but anyway you don't
need to know everything you don't need to

111
00:14:05,561 --> 00:14:11,923
do everything, debian is very big, you just
need to focus in something and try to do that

112
00:14:12,281 --> 00:14:17,832
Choose the part where you feel comfortable
even in translation there are many different

113
00:14:18,164 --> 00:14:26,983
kind of things, people from the old school
like people knowing po files, gettext and

114
00:14:26,983 --> 00:14:33,995
translate the documentation of debconf
templates, the new people, people coming

115
00:14:34,409 --> 00:14:42,594
from the web world can use the DDTS
the package descriptions because they just

116
00:14:42,964 --> 00:14:49,196
have to go to the website and fill in a form
or translate the wiki pages, its just an

117
00:14:49,396 --> 00:14:54,095
edit in the page, creating a new page in 
your language and put the same content in

118
00:14:54,385 --> 00:14:59,649
your language, its very easy, or the website
its also edited in a text file its very easy

119
00:14:59,941 --> 00:15:07,239
And this is my favorite, the super lazy mode
you just read what others people do and give

120
00:15:07,557 --> 00:15:13,769
your opinion, so you know your language so
even you don't need to know English

121
00:15:15,958 --> 00:15:21,672
You just read what other people translate
and you say here's a typo, its not

122
00:15:22,191 --> 00:15:31,321
understandable, this grammar is wrong and
that's all that kind of work its needed too

123
00:15:32,932 --> 00:15:39,827
Even you only know English you are important
for the translation teams because the people

124
00:15:40,313 --> 00:15:48,412
native english are very useful for
reviewing the work, the descriptions that

125
00:15:49,304 --> 00:16:00,203
non native English debian developers write
or for example the content for the website

126
00:16:00,657 --> 00:16:07,064
that is written by Spanish people or
French people or Chinese People, if somebody

127
00:16:07,995 --> 00:16:15,573
knows English very well just read that and
try to improve it, and this is very welcome

128
00:16:16,179 --> 00:16:26,474
So you can not escape, you know
one language at least you cannot escape the

129
00:16:26,911 --> 00:16:28,421
translation teams.

130
00:16:31,700 --> 00:16:39,449
And some tips, better contribute with
something than not contribute with anything

131
00:16:39,554 --> 00:16:41,746
Even a typo!

132
00:16:47,261 --> 00:16:52,372
If you don't want to be a translator
for debian you can start in a

133
00:16:52,769 --> 00:16:58,130
translation team and later you can find
another thing to do, but start from our part

134
00:17:01,854 --> 00:17:09,588
And say hello to the list, because we use
the mailing for communication so first say

135
00:17:09,910 --> 00:17:15,265
hello and keep in mind that we are a team,
somebody is reading the list, don't worry

136
00:17:17,528 --> 00:17:25,872
Sometimes I think when you are new in a team
it's kind of Olympic shooting, you receive

137
00:17:26,359 --> 00:17:30,412
many many mails in the list, 'I want to
translate this, this is wrong ...'

138
00:17:30,977 --> 00:17:35,916
and you just don't know what to do, you
just introduce yourself and say I want

139
00:17:36,199 --> 00:17:41,823
collaborate and that's all, please help me,
No you don't have to do that, you

140
00:17:42,066 --> 00:17:52,357
just have to focus on a small thing, focus
there and shoot, and when you finish repeat

141
00:17:54,950 --> 00:17:57,707
It like shooting ...

142
00:17:59,408 --> 00:18:05,118
And again trust in the community if you
don't say anything:'I don't know how to

143
00:18:05,482 --> 00:18:14,864
translate this, or this translation is too
big, I regret that I send this reservation

144
00:18:15,195 --> 00:18:19,527
I don't want to translate it anymore, if
you don't say we cannot guess it

145
00:18:20,295 --> 00:18:21,589
so say Hello

146
00:18:26,130 --> 00:18:29,898
If you became a translator you'll be famous

147
00:18:31,232 --> 00:18:36,432
Translations are credited from the very
beginning, you will see it in the statistics

148
00:18:37,407 --> 00:18:45,719
page the name of the person asking a
reservation "I want to do that it's

149
00:18:45,847 --> 00:18:51,271
written there", if you don't want you
just use a nickname or something, no problem

150
00:18:52,974 --> 00:19:02,815
And you will be proud to be a debian
contributor with a very new hot debian

151
00:19:03,179 --> 00:19:07,146
contributors, and you also can be proud of

152
00:19:07,632 --> 00:19:15,568
being a non uploading debian developer, not
only being a translator you can be a full

153
00:19:16,337 --> 00:19:27,598
member of the debian project, also there are
good things you can improve for other things

154
00:19:27,879 --> 00:19:37,192
different than debian, you improve your mother
language and English too, and this is a good

155
00:19:37,676 --> 00:19:43,605
thing for getting a new job for example, if
you speak very well people think you are an

156
00:19:43,635 --> 00:19:50,307
elegant person, you make debian very
friendly because it's in your language

157
00:19:50,721 --> 00:19:56,952
your mother can use it and your son or your
daughter can use it because ok now in the

158
00:19:57,283 --> 00:20:05,264
school they teach english but if its in his
or her mother language it's better and

159
00:20:05,627 --> 00:20:14,014
imagine your six/seven years old kid
installing debian and saying to their/his

160
00:20:14,419 --> 00:20:20,449
friends I'm installing my operating system
and i can understand because it's in my

161
00:20:20,773 --> 00:20:23,295
language.

162
00:20:24,470 --> 00:20:32,614
And also if you become a translator or
convince somebody to become a translator

163
00:20:33,053 --> 00:20:40,830
the debian community will be much bigger and
more diverse because just for the language

164
00:20:41,235 --> 00:20:48,687
diversity and the geographic diversity it
will much more diverse than only english

165
00:20:49,010 --> 00:20:59,048
speaking community and i think that's all.
That's only the theory part and now we are

166
00:20:59,048 --> 00:21:01,569
going to translate

167
00:21:04,450 --> 00:21:13,324
OK the idea basically is that now you are
locked in this room with us and you are

168
00:21:13,469 --> 00:21:20,983
forced to translate something. Are you up
to do this? Yes you are! Trust me !

169
00:21:22,931 --> 00:21:31,392
We want to try to do a couple of brief
translations in Spanish or in Catalan or

170
00:21:31,789 --> 00:21:38,073
in both, I don't now, it depends of how many
of you speak Spanish or Catalan here ...

171
00:21:39,657 --> 00:21:53,523
I don't speak either so I'm OK, and we doing
this for a webpage debian.org and for a

172
00:21:55,386 --> 00:22:13,831
debconf template of ganeti because is one 
of Spanish with less work, I mean there

173
00:22:14,399 --> 00:22:32,423
are only four strings. So if you have
installed or want to do it, gobby_0.4

174
00:22:36,911 --> 00:22:44,670
whatever i have the four one. You can connect
to gobby.debian.org and you can see the

175
00:22:44,953 --> 00:22:52,937
document we are trying to translate, so if
you want to do it we can do it together ...

176
00:23:01,930 --> 00:23:04,974
Ah Ah lots of people here!

177
00:23:10,969 --> 00:23:17,533
While you install it, I'll try to show you
where are the debconf messages to translate

178
00:23:23,451 --> 00:23:31,837
I will take as an example the Spanish
because we are going to do one of this...

179
00:23:34,017 --> 00:23:46,579
This is the address, this is a page with
statistics about the translations of debconf

180
00:23:47,066 --> 00:23:57,960
messages for the Spanish team, and has you
can see here are important links specially

181
00:23:58,244 --> 00:24:05,174
this one with hints for translators, I 
invite to read it when you try to do it

182
00:24:05,500 --> 00:24:16,152
alone ate home, and here there are ...
this one, because apparently the Spanish

183
00:24:16,599 --> 00:24:22,676
team is doing really a good job translating
so there not many, and this is the one I

184
00:24:22,920 --> 00:24:34,058
chosen because is really a little one, you
have to download from here, just the link

185
00:24:37,811 --> 00:24:41,178
But we have it already on the gobby

186
00:24:50,092 --> 00:24:52,439
So if you are ready

187
00:25:00,552 --> 00:25:02,869
This is a PO file

188
00:25:04,599 --> 00:25:07,994
here you can see you have to fill in

189
00:25:16,830 --> 00:25:19,231
Ohh I have an assistant !

190
00:25:20,151 --> 00:25:31,414
Here you can put something like translation
for ganeti , Spanish translation for ...

191
00:25:58,195 --> 00:26:11,247
Here goes your email, and here's the header
for the file and you don't have to do much

192
00:26:11,813 --> 00:26:21,011
Beside your name and email address, the
language team and the mailing list of the

193
00:26:21,019 --> 00:26:27,165
language team, the language obviously and
the encoding of the charset ...OK

194
00:26:27,501 --> 00:26:35,354
If you use a PO editor, for example you
configure the editor and it will fill in all

195
00:26:35,540 --> 00:26:43,400
that kind of strings for you and for all
the files that you translate, you can

196
00:26:43,400 --> 00:26:47,905
choose do it yourself or using a PO editor
for example.

197
00:26:48,617 --> 00:26:54,680
OK so I don't speak Spanish, how do you say
abort package removal?

198
00:27:04,154 --> 00:27:06,076
I'm forbidden I cannot do ...

199
00:27:07,537 --> 00:27:13,414
So obviously this is a variable so you
don't have to translate it

200
00:27:13,890 --> 00:27:18,185
you just put it as it is, but ....

201
00:27:25,277 --> 00:27:34,524
"Cancelar la eliminacion ...."

202
00:27:52,542 --> 00:27:56,970
You can send like that and people will say
where is the question mark, you can have

203
00:27:57,065 --> 00:27:59,766
to open a question mark and close question
mark...

204
00:28:00,858 --> 00:28:04,629
OK I don't have it on my keyboard so 
you do it.

205
00:28:05,481 --> 00:28:11,589
But as I said it's better than nothing, I
mean if you are starting we are very

206
00:28:11,872 --> 00:28:19,085
welcome with starting people, If I 
review , I mean I'm not going to do the

207
00:28:19,278 --> 00:28:26,575
translation, I don't like PO files...OK
But if somebody comes and say I am

208
00:28:26,867 --> 00:28:32,743
new in the list I want to translate this PO
file and this is my proposal, I will review

209
00:28:33,106 --> 00:28:41,050
it, and I will try to say/explain the
mistakes, because is a different thing is

210
00:28:41,253 --> 00:28:46,793
somebody new coming for the team, so I will
not spend time translating one PO file but

211
00:28:46,793 --> 00:28:56,249
I will spend time welcoming somebody to the
team. So its better than you do something,

212
00:28:56,484 --> 00:29:02,244
if it's not correct, totally correct even
if you now it's not totally correct

213
00:29:02,486 --> 00:29:09,470
no matter. Of course if your are six years
translating for debian like that, we will

214
00:29:09,470 --> 00:29:17,021
get angry, but for the first times really
it doesn't matter, reviewing work of other

215
00:29:17,430 --> 00:29:19,090
people is a nice thing.

216
00:29:20,189 --> 00:29:24,116
OK we have ten minutes to show the website

217
00:29:30,371 --> 00:29:34,827
Another thing is you don't need to
translate files

218
00:29:35,394 --> 00:29:40,617
even if you don't translate files
you just need to ...it's very useful that

219
00:29:41,013 --> 00:29:44,869
update the translations that other people
do and I will show...

220
00:31:21,820 --> 00:31:36,583
This one cd/artwork/index, you just need to
put in Spanish this thing, this part only

221
00:31:38,651 --> 00:31:48,824
and remove this part, so for keeping update
that file in the website, just need to

222
00:31:50,278 --> 00:31:56,480
No ..even you don't need to translate just
move this up and that's all

223
00:31:59,878 --> 00:32:09,777
Because they just change the place, so its
really very very easy and with small parts

224
00:32:09,894 --> 00:32:11,367
that you can help.

225
00:32:31,577 --> 00:32:35,929
But sometimes the updates are very small.

226
00:32:36,899 --> 00:32:39,639
For the Spanish team I did all the
small parts

227
00:32:43,819 --> 00:32:49,730
I spend one year doing only small
things , and thats all, but anyway

228
00:32:51,351 --> 00:32:53,498
So questions ?

229
00:32:54,742 --> 00:33:01,271
If you have any question, you can speak
up now, or ... never .... Oh yeah

230
00:33:03,911 --> 00:33:11,088
Q: Hey, so how does the book keeping work,
when you want to work on a file, you say

231
00:33:11,410 --> 00:33:15,662
hey I want to work on that? That's done
via the packaging system or, how does it

232
00:33:15,968 --> 00:33:17,158
work?

233
00:33:19,183 --> 00:33:25,747
A: You have to send a mail to the 
translation team with a special subject

234
00:33:26,119 --> 00:33:35,970
we have a tag ....
you send an intent to translate (ITT)

235
00:33:36,986 --> 00:33:44,078
between brackets and then the URL of the
file, and that's all , the bot then put the

236
00:33:44,609 --> 00:33:51,815
name of the sender of the mail in the
statistics and everybody can see

237
00:33:52,098 --> 00:34:01,019
if somebody is already translating, they
can say No I sent the ITT for, but look

238
00:34:01,062 --> 00:34:12,284
at the statistics page, and try to find
a file that is free, and you send the ITT

239
00:34:13,336 --> 00:34:21,155
mail, to the list, later when you have your
translation done, you send a mail with a

240
00:34:21,676 --> 00:34:30,841
request for review (RFR) and a URL, so
people comment and you attach the file

241
00:34:31,287 --> 00:34:41,203
and people comment on that, normally 
people will not correct directly but you

242
00:34:41,614 --> 00:34:48,500
make you the changes you want if you
accepted that suggestion and you send the

243
00:34:48,705 --> 00:35:02,322
file again ... that's why I said proof
reading, it's the super lazy mode you even

244
00:35:02,686 --> 00:35:07,225
don't need to change the file that other
people send, Just say this is not correct

245
00:35:07,507 --> 00:35:13,427
I would say like that, and the other one 
has to change the file, of course you can

246
00:35:13,710 --> 00:35:20,292
say I don't agree with you blah blah blah
But anyway you can help just reading the

247
00:35:20,665 --> 00:35:31,882
mailing list and answering the reviewers.
And here are the statistics of translations

248
00:35:35,573 --> 00:35:41,548
You can order by translator, by file,
by type of files, by anything.

249
00:35:44,344 --> 00:35:49,169
Really I have lots of files but it's just
a one line things.

250
00:35:51,158 --> 00:35:57,151
That's why I said you can be famous
latter you can be present on the statistics

251
00:35:57,555 --> 00:35:59,912
even if you do a small work.

252
00:36:04,600 --> 00:36:06,180
So more questions?

253
00:36:10,199 --> 00:36:17,984
And if you do want to try translating
something during this mini-debconf we can

254
00:36:18,309 --> 00:36:30,196
find ... but I can find maybe tomorrow 
afternoon to do some work, come to me

255
00:36:30,686 --> 00:36:37,582
and we can try to do something together
its really easy, well I don't speak Spanish

256
00:36:38,029 --> 00:36:41,762
but it's really easy on the procedure part

257
00:36:42,820 --> 00:36:49,423
And really say Hello to the list but don't
say only I want to collaborate please

258
00:36:49,614 --> 00:36:51,607
help me and that's all ...

259
00:36:54,054 --> 00:36:59,994
Choose one thing and remember the Olympic
shooting, choose one thing I want to do

260
00:37:00,037 --> 00:37:06,313
this and I need help to do this, and I'm sure
somebody will answer and will help you.

261
00:37:08,619 --> 00:37:13,738
Ok thank you ! And sorry it was brief but
intense. Thank you !
