1
00:00:00,620 --> 00:00:01,920
Welcome everyone!

2
00:00:01,920 --> 00:00:04,080
Today is the last day of DebConf 15.

3
00:00:04,080 --> 00:00:06,340
I hope you enjoyed it so far.

4
00:00:06,340 --> 00:00:08,450
If you haven't, make sure
you enjoy it today.

5
00:00:08,450 --> 00:00:10,340
[laughter]

6
00:00:11,860 --> 00:00:14,660
So, we've already mentioned this
a couple of times,

7
00:00:14,660 --> 00:00:16,780
but just in case you were not there,

8
00:00:16,780 --> 00:00:18,120
you need to checkout at 10.

9
00:00:18,120 --> 00:00:20,360
If you are leaving day is today,

10
00:00:20,360 --> 00:00:22,360
you basically need to check out now.

11
00:00:22,360 --> 00:00:26,620
The hostel has some space where you
can put your luggage.

12
00:00:26,620 --> 00:00:30,480
So you can leave your luggage and enjoy
the rest of your day,

13
00:00:30,480 --> 00:00:33,440
but you need to leave your room by then.

14
00:00:33,800 --> 00:00:35,670
Also we've already mentioned

15
00:00:35,670 --> 00:00:37,580
we have several ways of giving feedback.

16
00:00:37,580 --> 00:00:39,800
We really want your feedback, good or bad.

17
00:00:40,930 --> 00:00:44,500
You can edit the wiki and put it there

18
00:00:44,500 --> 00:00:45,740
so that everyone can see.

19
00:00:45,740 --> 00:00:47,280
You can send it by mail.

20
00:00:47,280 --> 00:00:49,020
Only a few people read that mail

21
00:00:49,020 --> 00:00:51,120
and then the results will be anonymized.

22
00:00:51,120 --> 00:00:53,720
So if you don't want everyone
to see what you think,

23
00:00:53,720 --> 00:00:55,180
you can send it by mail.

24
00:00:55,180 --> 00:00:57,900
And if you feel like it
was a great conference

25
00:00:57,900 --> 00:00:59,150
and you really enjoyed it,

26
00:00:59,150 --> 00:01:03,100
there are some post cards for
the Orga team at the front desk

27
00:01:03,100 --> 00:01:05,500
where you can leave a
short message saying,

28
00:01:05,500 --> 00:01:07,780
"Oh, I really liked it", or whatever.

29
00:01:09,430 --> 00:01:10,480
But for the postcards

30
00:01:10,480 --> 00:01:12,040
we prefer good feedback.

31
00:01:12,040 --> 00:01:13,040
[laughter]

32
00:01:13,940 --> 00:01:16,000
There will be a tip jar.

33
00:01:16,000 --> 00:01:16,880
Did you bring it?

34
00:01:17,520 --> 00:01:19,170
Well it's actually a tip box,

35
00:01:19,170 --> 00:01:21,100
but it says tip jar.

36
00:01:21,800 --> 00:01:23,760
That's for the hostel,

37
00:01:23,760 --> 00:01:27,580
so if you feel like the hostel staff
treated you well

38
00:01:27,580 --> 00:01:30,780
and you want to appreciate that,
you can do that.

39
00:01:36,000 --> 00:01:38,360
So you can also, if you have money left

40
00:01:38,360 --> 00:01:41,180
in your card.

41
00:01:43,400 --> 00:01:44,100
This card.

42
00:01:44,100 --> 00:01:45,960
If you have money left in your

43
00:01:45,960 --> 00:01:48,320
colorful card, you can leave the card

44
00:01:48,320 --> 00:01:50,080
and that can also be a tip jar

45
00:01:50,080 --> 00:01:52,900
or you can get your money back,
depending on what you want.

46
00:01:54,780 --> 00:01:56,520
The tip jar will be outside.

47
00:01:58,800 --> 00:02:00,900
There are some attendee T-shirts left

48
00:02:00,900 --> 00:02:04,120
that are on sale for 15 Euros.

49
00:02:04,120 --> 00:02:06,120
There will also be other swag,

50
00:02:06,120 --> 00:02:11,120
like lanyards, water bottles, Bierkrugs,
and USB fans that we gave to some people

51
00:02:11,120 --> 00:02:14,360
that you can buy at the front desk.

52
00:02:14,360 --> 00:02:16,360
Prices will be there.

53
00:02:16,360 --> 00:02:19,940
And if you didn't get an attendee bag
when you checked in

54
00:02:19,940 --> 00:02:23,320
because we didn't know exactly how many
people would turn up

55
00:02:23,320 --> 00:02:27,040
and for some late registrans, we didn't
give an attendee bag

56
00:02:27,040 --> 00:02:29,560
you can get one now
because we have enough.

57
00:02:30,740 --> 00:02:33,160
And finally for the cleanup,

58
00:02:33,160 --> 00:02:35,240
we will need volunteers for the cleanup.

59
00:02:36,840 --> 00:02:38,700
Which will start after dinner.

60
00:02:38,700 --> 00:02:40,700
So will do the closing ceremony,

61
00:02:40,700 --> 00:02:42,100
then we will have dinner.

62
00:02:42,100 --> 00:02:44,100
And after dinner we will do the cleanup.

63
00:02:44,100 --> 00:02:46,860
If you feel like you can give a hand,

64
00:02:46,860 --> 00:02:48,860
please help out.

65
00:02:50,720 --> 00:02:52,540
Okay, for the raffle today,

66
00:02:52,540 --> 00:02:54,540
we don't have the actual items here

67
00:02:54,540 --> 00:02:58,340
so we will need to write down your
name and address so we can ship it.

68
00:02:58,860 --> 00:03:03,060
The first one, we do have something,

69
00:03:03,060 --> 00:03:06,140
which is this laptop sleeve.

70
00:03:08,340 --> 00:03:11,200
Oh, we actually got the laptop?
Great.

71
00:03:11,200 --> 00:03:21,900
[laughter and applause]

72
00:03:21,900 --> 00:03:25,460
Okay, so the first item for
today's raffle is

73
00:03:25,460 --> 00:03:30,800
sponsored by one of our silver sponsors,
Meinberg, maker of NTP devices.

74
00:03:30,800 --> 00:03:35,280
They have agreed to give a Libreboot X200

75
00:03:35,280 --> 00:03:36,440
to the winner.

76
00:03:36,440 --> 00:03:39,020
And once you- that's it, that's right,

77
00:03:39,020 --> 00:03:39,960
bring it up here.

78
00:03:39,960 --> 00:03:42,820
[laughter]

79
00:03:42,820 --> 00:03:46,060
The winner will get it shipped directly
from the UK.

80
00:03:48,780 --> 00:03:51,980
Alright, so let's start this.

81
00:04:05,320 --> 00:04:08,140
There are less people today
because some people left.

82
00:04:14,160 --> 00:04:16,839
Ina Faye-Lund, are you here?

83
00:04:17,980 --> 00:04:19,120
No.

84
00:04:19,120 --> 00:04:21,120
[nervous laugh from speaker]

85
00:04:23,540 --> 00:04:25,140
Harsh Daftary?

86
00:04:25,880 --> 00:04:26,920
Also not here.

87
00:04:32,220 --> 00:04:33,680
Ying-Chun Liu?

88
00:04:34,920 --> 00:04:42,340
[applause]

89
00:04:44,020 --> 00:04:48,180
Okay, so Martin will write down the
people's details so that they can

90
00:04:48,180 --> 00:04:50,280
get the things.

91
00:04:50,280 --> 00:04:53,180
Now I have to explain the other things.

92
00:04:53,180 --> 00:04:55,900
So the other things that we have

93
00:04:55,900 --> 00:05:03,100
are some NAS servers, which
are prototypes, so they-

94
00:05:03,100 --> 00:05:04,380
[laughter]

95
00:05:04,380 --> 00:05:06,000
They may or may not work!

96
00:05:06,000 --> 00:05:08,000
But you know you're getting them for free.

97
00:05:11,560 --> 00:05:12,780
NAS servers is correct,

98
00:05:12,780 --> 00:05:14,400
you can use them as a NAS server,

99
00:05:14,400 --> 00:05:17,350
but they are actually products from
a small startup in Hamburg.

100
00:05:17,350 --> 00:05:22,260
Their idea was to create these little
boxes that can be your personal cloud.

101
00:05:22,260 --> 00:05:26,440
So the experimental platform which we're
giving out three of them today

102
00:05:26,440 --> 00:05:28,800
is actually a device that is
designed to be

103
00:05:28,800 --> 00:05:32,140
your cloud server that you can have
on your desk.

104
00:05:32,140 --> 00:05:35,640
I think it's a very cool project, and

105
00:05:35,640 --> 00:05:38,620
they are running Ubuntu, and when

106
00:05:38,620 --> 00:05:43,800
they were chatting whether the surprise
is exceptable to people because it might

107
00:05:43,800 --> 00:05:47,580
not work properly, it's all free software
and stuff, then I said that,

108
00:05:47,580 --> 00:05:51,070
"probably there is no better place than to
give these away right here."

109
00:05:51,070 --> 00:05:53,400
[laughter]

110
00:05:53,640 --> 00:05:55,320
Alright, so we have three of them,

111
00:05:55,320 --> 00:05:57,560
and they will get shipped to you.

112
00:05:57,560 --> 00:06:03,180
We don't have them. They will get shipped
to you by the company.

113
00:06:04,300 --> 00:06:06,060
So we will need three winners.

114
00:06:09,000 --> 00:06:10,960
[inaudible audience member]

115
00:06:10,960 --> 00:06:14,320
Yeah yeah. [laughter]

116
00:06:18,400 --> 00:06:29,060
[laughter and applause]

117
00:06:29,060 --> 00:06:30,130
(Thomas) That's me!

118
00:06:30,130 --> 00:06:33,260
[more laughter]

119
00:06:35,860 --> 00:06:38,680
The name of the company is Protonet.

120
00:06:38,680 --> 00:06:39,800
Protonet.

121
00:06:39,800 --> 00:06:42,720
I will show the website again
after the raffle.

122
00:06:49,040 --> 00:06:50,080
Nigel.

123
00:06:51,100 --> 00:06:58,980
[applause]

124
00:07:02,720 --> 00:07:04,160
Eder Marques?

125
00:07:04,880 --> 00:07:05,740
He's here!

126
00:07:06,500 --> 00:07:12,700
[applause]

127
00:07:13,820 --> 00:07:16,200
So I will show the webpage again.

128
00:07:16,200 --> 00:07:19,260
So the website is protonet.info.

129
00:07:43,880 --> 00:07:47,160
This is actually the device that these
three people are getting.

130
00:07:49,980 --> 00:07:51,320
Yeah, it's beta.

131
00:07:52,360 --> 00:07:55,860
It's supposed to be little cubes like
this hexagons.

132
00:07:58,420 --> 00:08:01,060
That's right, well they still call
them cubes.

133
00:08:03,980 --> 00:08:06,820
But actually, there's no casing for the
ones that you get.

134
00:08:06,820 --> 00:08:08,820
[laughter]

135
00:08:08,820 --> 00:08:10,000
That's great isn't it,

136
00:08:10,000 --> 00:08:11,580
so you can choose your own.

137
00:08:11,580 --> 00:08:13,580
[laughter]

138
00:08:14,260 --> 00:08:17,700
Alright so, that's it for the
morning briefings.

139
00:08:17,700 --> 00:08:21,000
In six minutes we have Jacob's talk.

140
00:08:21,920 --> 00:08:24,320
If you need to checkout, go run and
check out now.
