1
00:00:00,000 --> 00:00:02,480
Hi, my name is Sindhu

2
00:00:02,480 --> 00:00:04,480
and I'm gonna talk about

3
00:00:04,480 --> 00:00:06,480
"Newcomer experience in Debian packaging"

4
00:00:06,480 --> 00:00:08,480
"Who? Where? Why?"

5
00:00:09,250 --> 00:00:12,160
From 2006 to 2009

6
00:00:12,160 --> 00:00:14,160
were my first few years using

7
00:00:14,160 --> 00:00:16,160
GNU/Linux systems and

8
00:00:16,190 --> 00:00:17,940
I was majorly just distro hopping

9
00:00:17,970 --> 00:00:22,000
kind of get the latest desktop and ??? everyone of them

10
00:00:22,160 --> 00:00:24,600
and when I couldn't find the latest

11
00:00:24,600 --> 00:00:26,600
packages I would install

12
00:00:26,600 --> 00:00:29,990
things from the testing repositories

13
00:00:29,990 --> 00:00:31,990
and it didn't make much of a difference, you know?

14
00:00:31,990 --> 00:00:33,990
I would have the latest packages

15
00:00:33,990 --> 00:00:35,990
and I soon learnt that testing and

16
00:00:35,990 --> 00:00:37,990
devel package repositories where

17
00:00:37,990 --> 00:00:40,320
really testing and that would be unstable and

18
00:00:40,320 --> 00:00:42,320
break my system.

19
00:00:42,320 --> 00:00:44,320
So I went back to fix my system

20
00:00:44,320 --> 00:00:45,860
and for the next three years

21
00:00:45,860 --> 00:00:47,860
this is what I was doing

22
00:00:47,860 --> 00:00:49,860
you know... ???

23
00:00:49,860 --> 00:00:52,990
Later I was looking for a career change

24
00:00:52,990 --> 00:00:56,540
and a friend came along to teach me how to make a patch

25
00:00:58,070 --> 00:01:01,080
this is about the time when Outreach Program for Women

26
00:01:01,500 --> 00:01:04,489
was announced and

27
00:01:04,489 --> 00:01:07,970
I became a Documentation Intern for GNOME

28
00:01:07,970 --> 00:01:11,900
and I also went on to Google Summer of Code internship

29
00:01:11,900 --> 00:01:14,760
with Gnome I contributed code to gitg project

30
00:01:14,770 --> 00:01:19,360
and I'm now a mentor for Documentation interns

31
00:01:19,370 --> 00:01:22,190
at the Outreach Program.

32
00:01:22,340 --> 00:01:24,340
"What?"

33
00:01:24,340 --> 00:01:28,040
My first failed attempt at packaging

34
00:01:28,400 --> 00:01:32,390
was when I tried to package a

35
00:01:32,390 --> 00:01:34,630
a ??? called cnetworkmanager

36
00:01:34,630 --> 00:01:36,630
which is a command line

37
00:01:36,630 --> 00:01:40,290
application to access network manager

38
00:01:40,290 --> 00:01:44,280
and looking at the ??? documentation then

39
00:01:44,280 --> 00:01:46,280
pretty overwhelming and

40
00:01:46,280 --> 00:01:48,280
in retrospect I think that

41
00:01:48,280 --> 00:01:50,280
I abandoned my efforts quickly

42
00:01:50,280 --> 00:01:56,000
because I wasn't experienced in using GNU/Linux systems

43
00:01:56,000 --> 00:01:59,240
and because there was a lot of documentation, truly.

44
00:01:59,300 --> 00:02:02,950
My second failed attempt was as recent as last year

45
00:02:02,950 --> 00:02:04,950
and I just wanted to package something for Fedora

46
00:02:04,950 --> 00:02:08,889
and looking at the specs files and so many things to

47
00:02:08,889 --> 00:02:13,530
do about it, I was quick to just dismiss it.

48
00:02:13,530 --> 00:02:15,530
Other commitments came along

49
00:02:15,530 --> 00:02:19,820
but this was a ???

50
00:02:19,820 --> 00:02:21,720
"So then?"

51
00:02:21,720 --> 00:02:25,900
My first job ??? two years

52
00:02:25,910 --> 00:02:30,220
when I was contributing to GNOME, was

53
00:02:30,220 --> 00:02:32,220
not just about learning for myself

54
00:02:32,220 --> 00:02:34,220
but also teaching other newcomers

55
00:02:34,220 --> 00:02:36,220
and helping people get into the community

56
00:02:37,920 --> 00:02:42,350
Soon I was speaking to many newcomers on mailing lists

57
00:02:42,350 --> 00:02:46,320
and in person teaching at local colleges

58
00:02:46,320 --> 00:02:49,920
and events on how to contribute to GNOME

59
00:02:49,920 --> 00:02:51,920
and subliminally

60
00:02:51,920 --> 00:02:53,920
you know, you tend to summarise in your head

61
00:02:54,490 --> 00:02:57,890
telling the same things to many different people

62
00:02:57,890 --> 00:02:59,890
It's just really four steps to

63
00:03:00,090 --> 00:03:04,810
start, to really start contributing to any project.

64
00:03:04,810 --> 00:03:08,720
And that would be to go to the official website and

65
00:03:08,720 --> 00:03:11,390
read the pages about how to help Project X

66
00:03:11,390 --> 00:03:14,510
or "Get Involved" or "Help us" pages

67
00:03:14,510 --> 00:03:19,330
and next step would be to set up the developement environment

68
00:03:19,330 --> 00:03:21,290
and do your ground work

69
00:03:21,290 --> 00:03:22,770
and find a willing mentor

70
00:03:22,780 --> 00:03:25,520
who will be able to take you from there.

71
00:03:25,520 --> 00:03:28,590
So, where's Debian in the picture?

72
00:03:28,590 --> 00:03:30,770
"Enter Debian"

73
00:03:30,770 --> 00:03:32,770
I have used Ubuntu extensively

74
00:03:32,770 --> 00:03:35,200
and I knew my way around apt-get

75
00:03:35,200 --> 00:03:37,200
but this is all I knew about Debian

76
00:03:37,200 --> 00:03:39,200
and Debian based distributions

77
00:03:39,720 --> 00:03:42,450
but learning about Debian MiniConf

78
00:03:42,450 --> 00:03:46,290
made me realise that this is my opportunity to

79
00:03:46,320 --> 00:03:50,660
change the memories I had about failing

80
00:03:50,660 --> 00:03:54,460
something and it would be a great way to get a headstart

81
00:03:54,460 --> 00:03:56,460
in such vibrant community

82
00:03:57,020 --> 00:03:59,320
and start packaging.

83
00:03:59,320 --> 00:04:01,950
"Choosing your tools"

84
00:04:01,950 --> 00:04:03,950
Choosing your tools.

85
00:04:03,950 --> 00:04:05,950
My first step to contributing to

86
00:04:05,950 --> 00:04:07,950
Debian would be to get Debian!

87
00:04:09,870 --> 00:04:13,420
I was an Arch Linux user and

88
00:04:13,420 --> 00:04:15,420
there are a couple of things to keep in mind when you

89
00:04:15,420 --> 00:04:17,420
try to contribute to a distribution.

90
00:04:17,420 --> 00:04:19,420
For example, the time it takes to set up

91
00:04:19,420 --> 00:04:21,420
and the time it takes to

92
00:04:21,420 --> 00:04:24,490
transfer your work over this new distribution

93
00:04:24,490 --> 00:04:26,490
and the disk space it would require

94
00:04:26,490 --> 00:04:30,130
and whether or not your device...

95
00:04:30,130 --> 00:04:34,000
...you can set up this distribution easily on your device

96
00:04:34,130 --> 00:04:38,170
Not to mention the bandwidth required for

97
00:04:38,170 --> 00:04:40,170
the Internet bandwidth required for

98
00:04:40,170 --> 00:04:43,380
downloading and upgrading packages.

99
00:04:43,760 --> 00:04:47,440
In GNOME, to make this transition easier

100
00:04:47,440 --> 00:04:52,130
to have this quickstart with newcomers, we have

101
00:04:52,130 --> 00:04:54,130
a virtual machine image preloaded

102
00:04:54,130 --> 00:04:57,700
with our build system, so they can try it out and setup

103
00:04:57,730 --> 00:05:00,300
they don't have to setup stuff from scratch

104
00:05:00,300 --> 00:05:03,650
and when they're confident they can go set it up on disk

105
00:05:03,650 --> 00:05:07,420
Debian didn't offer me any

106
00:05:07,420 --> 00:05:13,710
such - you know - instant noodle like

107
00:05:13,710 --> 00:05:17,430
VM image but, it wasn't hard to set up

108
00:05:17,430 --> 00:05:22,430
so I went about using another fast project called Vagrant

109
00:05:22,430 --> 00:05:26,370
and I made a Debian box

110
00:05:26,370 --> 00:05:29,490
which I pre-filled with packaging tools

111
00:05:29,490 --> 00:05:33,010
???

112
00:05:33,020 --> 00:05:41,810
I also learned about another project called Docker

113
00:05:41,810 --> 00:05:44,600
which is an even more elegant solution

114
00:05:44,600 --> 00:05:50,430
to have newcomers try out different environments in a GNU/Linux system.

115
00:05:51,270 --> 00:05:55,320
So what happened after this?

116
00:05:55,320 --> 00:05:58,040
"So..."

117
00:05:58,040 --> 00:06:02,870
My first experience when I was contributing to GNOME

118
00:06:03,160 --> 00:06:06,690
was trying to understand how ???

119
00:06:06,690 --> 00:06:10,960
and after learning how to make a patch I realized

120
00:06:10,960 --> 00:06:14,110
the meaning of the word "upstream", you know,

121
00:06:14,110 --> 00:06:18,200
realizing concepts about stable and unstable stages

122
00:06:18,200 --> 00:06:21,660
releases, and what kind of ???

123
00:06:21,660 --> 00:06:24,620
and all this information was pieced together

124
00:06:24,620 --> 00:06:27,730
over time of reading GNOME wiki pages

125
00:06:27,730 --> 00:06:30,120
including the content of GNOME Love,

126
00:06:30,120 --> 00:06:32,120
and interacting with my mentor

127
00:06:32,120 --> 00:06:35,560
and also from this knowledge I had

128
00:06:35,560 --> 00:06:38,250
from previously distro hopping

129
00:06:38,250 --> 00:06:41,430
I also made a upstream release myself

130
00:06:41,430 --> 00:06:43,430
and become the maintainer of

131
00:06:43,430 --> 00:06:45,430
one of the projects I was contributing

132
00:06:45,430 --> 00:06:47,430
a user documentation tool.

133
00:06:47,430 --> 00:06:51,530
So, doing all of this peripheral work

134
00:06:51,530 --> 00:06:56,440
made me a nice picture, a visualization in my head

135
00:06:56,440 --> 00:06:59,760
of how software development in facts takes place.

136
00:06:59,760 --> 00:07:02,560
So when I started with Debian

137
00:07:02,560 --> 00:07:04,560
I navigated to the Debian website

138
00:07:04,560 --> 00:07:08,150
read the introductory page about packaging

139
00:07:08,720 --> 00:07:11,500
I went to the Developer's Corner

140
00:07:11,500 --> 00:07:13,500
and the landing page for Documentation

141
00:07:13,500 --> 00:07:16,800
and I clicked on everything that was related to packagin.

142
00:07:17,300 --> 00:07:20,620
Since I knew that GNOME had a wiki

143
00:07:20,930 --> 00:07:23,990
it was likely that Debian also maintained a wiki

144
00:07:23,990 --> 00:07:27,230
and there was also a really helpful Teams page

145
00:07:27,230 --> 00:07:29,230
which made me realize

146
00:07:29,230 --> 00:07:32,280
how Debian is separated in terms of

147
00:07:32,280 --> 00:07:34,280
division of labour

148
00:07:34,280 --> 00:07:37,900
And I started doing all the things

149
00:07:37,900 --> 00:07:39,900
that were required for packaging

150
00:07:39,900 --> 00:07:41,900
and I kind of get stuck.

151
00:07:41,900 --> 00:07:45,180
"Hello, I am..."

152
00:07:45,180 --> 00:07:49,020
In my excitement of wanting to generate a deb file

153
00:07:49,020 --> 00:07:51,020
and at this point thinking that this was

154
00:07:51,020 --> 00:07:53,020
all that was to packaging

155
00:07:53,020 --> 00:07:57,470
I went through #debian-devel, #debian-gnome, #debian-mentors

156
00:07:57,470 --> 00:07:59,470
and #debian-women

157
00:07:59,470 --> 00:08:02,680
to told them that I was stuck because

158
00:08:02,680 --> 00:08:04,680
my deb file was empty

159
00:08:04,680 --> 00:08:09,790
and lot of them suggested in good intentions

160
00:08:09,790 --> 00:08:13,490
they suggested do this, do that but

161
00:08:13,490 --> 00:08:15,490
nothing made sense because...

162
00:08:16,080 --> 00:08:20,070
I had no idea what they were talking about

163
00:08:20,070 --> 00:08:24,310
So even though I was talking in English

164
00:08:24,890 --> 00:08:26,800
I was not able to communicate

165
00:08:26,800 --> 00:08:29,730
and it was quite frustrating because

166
00:08:29,930 --> 00:08:32,520
they did not know where I was coming from.

167
00:08:32,520 --> 00:08:34,520
So I went back to

168
00:08:34,520 --> 00:08:38,500
#debian-bcn2014 and Ana

169
00:08:38,500 --> 00:08:42,140
who first helped me put together

170
00:08:42,140 --> 00:08:45,980
my talk for this conference

171
00:08:45,980 --> 00:08:47,980
said "calm down and

172
00:08:47,980 --> 00:08:49,980
I'm gonna lead you through the steps"

173
00:08:49,980 --> 00:08:53,550
and she went by this ideology of learn

174
00:08:53,550 --> 00:08:55,550
to package first, and then making the package

175
00:08:55,550 --> 00:08:57,550
so this was fine

176
00:08:57,550 --> 00:09:01,150
this suited me fine because that's the way I learnt.

177
00:09:01,150 --> 00:09:02,680
I guess the most important thing that

178
00:09:02,680 --> 00:09:04,680
we can take away from this episode

179
00:09:04,680 --> 00:09:07,910
is to be a good mentor, you have to

180
00:09:07,910 --> 00:09:09,910
assume a very little baseline

181
00:09:09,910 --> 00:09:13,440
knowledge with the person ???

182
00:09:13,440 --> 00:09:15,940
be clear with instructions

183
00:09:15,940 --> 00:09:17,940
and don't expect them to have

184
00:09:17,940 --> 00:09:19,580
done XYZ

185
00:09:19,580 --> 00:09:23,040
because they may not

186
00:09:23,040 --> 00:09:26,150
know about it, but they may know ABC

187
00:09:26,150 --> 00:09:30,320
Ask if they have done XYZ

188
00:09:30,320 --> 00:09:32,320
and - you know - take it from there.

189
00:09:33,680 --> 00:09:36,610
So, what exactly did I do?

190
00:09:36,610 --> 00:09:39,440
"Actual work"

191
00:09:39,440 --> 00:09:42,550
I'm currently packaging GNOME Code Assistance

192
00:09:42,550 --> 00:09:45,310
and Gedit Code Assistance modules

193
00:09:45,310 --> 00:09:46,400
for GNOME.

194
00:09:46,400 --> 00:09:51,440
The former gives code assistance services for GNOME

195
00:09:51,440 --> 00:09:53,680
Basically if you have an IDE

196
00:09:53,680 --> 00:09:55,680
or any text editor

197
00:09:55,680 --> 00:09:57,680
in GNOME that you want to

198
00:09:57,680 --> 00:09:59,990
putting syntax error checking features

199
00:09:59,990 --> 00:10:01,420
for a particular language,

200
00:10:01,420 --> 00:10:03,420
then this is the package that does it.

201
00:10:03,420 --> 00:10:05,420
While the latter is a package

202
00:10:05,420 --> 00:10:07,420
that emits the dbus signal

203
00:10:07,420 --> 00:10:10,590
in Gedit text editor

204
00:10:10,600 --> 00:10:13,280
back to GNOME Code Assistance

205
00:10:13,280 --> 00:10:17,620
so when the correct backend is enabled

206
00:10:17,620 --> 00:10:19,910
this two packages interact with each other

207
00:10:19,950 --> 00:10:25,020
to give syntax validation features

208
00:10:25,050 --> 00:10:29,060
So far these are the things I've done

209
00:10:29,160 --> 00:10:32,290
I've gotten past in generating

210
00:10:32,290 --> 00:10:34,290
a working deb file

211
00:10:34,290 --> 00:10:36,290
but it can be improved

212
00:10:36,290 --> 00:10:39,300
and I really understood

213
00:10:39,300 --> 00:10:42,020
what dependencies meant.

214
00:10:42,020 --> 00:10:44,880
And that how autotools work

215
00:10:44,880 --> 00:10:47,340
and I started seen

216
00:10:47,340 --> 00:10:49,340
I really started paying attention

217
00:10:49,340 --> 00:10:51,340
and ??? how autotools

218
00:10:51,340 --> 00:10:54,430
are related to configuration files

219
00:10:54,430 --> 00:10:59,050
and I also patched our own build system called jhbuild

220
00:10:59,050 --> 00:11:03,520
to build these two modules in GNOME

221
00:11:03,520 --> 00:11:06,180
My next immediate concerns

222
00:11:06,180 --> 00:11:08,750
are getting the upstream author to

223
00:11:08,750 --> 00:11:10,750
bring it to a...

224
00:11:10,750 --> 00:11:14,420
usable position, because it works for some people

225
00:11:14,420 --> 00:11:16,420
and for some it doesn't: it works for the upstream author

226
00:11:16,420 --> 00:11:18,710
it works for him...

227
00:11:18,760 --> 00:11:23,050
and Ana suggested that I

228
00:11:23,050 --> 00:11:27,290
update a package

229
00:11:27,290 --> 00:11:29,290
that it is in QA and I think

230
00:11:29,290 --> 00:11:31,290
that's a small contribution

231
00:11:31,290 --> 00:11:33,010
for me to start from and then she

232
00:11:33,010 --> 00:11:36,640
sponsor it, so that's my next task.

233
00:11:36,640 --> 00:11:40,210
I want to document it

234
00:11:40,210 --> 00:11:41,730
for end users

235
00:11:41,740 --> 00:11:46,240
So one of the bugs that ??? against all of these packages are

236
00:11:46,240 --> 00:11:48,240
that there is no user documention so nobody knows

237
00:11:48,240 --> 00:11:50,240
how to use it, it's all magic

238
00:11:50,560 --> 00:11:54,000
So, packaging has also made

239
00:11:54,000 --> 00:11:56,000
these things my priority

240
00:11:56,000 --> 00:11:58,000
and I have few ideas

241
00:11:58,000 --> 00:12:00,000
that come from GNOME love

242
00:12:02,340 --> 00:12:04,340
A few ideas I feel

243
00:12:04,340 --> 00:12:06,340
Debian can benefit

244
00:12:06,340 --> 00:12:10,350
and to have a greater newcomers engage in the community

245
00:12:10,350 --> 00:12:15,280
or having the concept of GNOME Love like

246
00:12:15,280 --> 00:12:17,280
in packaging

247
00:12:17,280 --> 00:12:19,280
For example, tagging packages

248
00:12:19,280 --> 00:12:21,280
that are orphaned or are in QA

249
00:12:21,280 --> 00:12:25,980
that can be updated by anyone and this would make a great

250
00:12:25,980 --> 00:12:27,360
beginner contribution

251
00:12:27,980 --> 00:12:29,980
Specifically having a tutorial to do this

252
00:12:29,980 --> 00:12:32,970
would be nice

253
00:12:32,970 --> 00:12:34,970
And wiki pages about

254
00:12:34,970 --> 00:12:37,810
packaging that could reutilize videos

255
00:12:37,810 --> 00:12:42,430
and BoF sessions from previous Debian conferences

256
00:12:42,430 --> 00:12:44,430
For example there was a wiki page about

257
00:12:44,430 --> 00:12:47,650
Debtags, which uses a 2007 video to

258
00:12:47,650 --> 00:12:51,200
talk about what Debtags is, and what it does

259
00:12:51,200 --> 00:12:53,200
and there are some great videos

260
00:12:53,200 --> 00:12:57,370
I found in the archives of Debian conferences

261
00:12:57,390 --> 00:13:00,250
the first one is "Documentation in Debian"

262
00:13:00,250 --> 00:13:04,390
another one is "How contribute and get involved"

263
00:13:04,390 --> 00:13:09,630
and there's another great video about how other FLOSS communities mentor

264
00:13:09,630 --> 00:13:11,630
and the last idea is

265
00:13:13,240 --> 00:13:16,950
it's some ??? an idea is something ???

266
00:13:16,950 --> 00:13:18,950
Vincent Untz conducted

267
00:13:18,950 --> 00:13:21,730
an interesting session in FOSDEM 2010

268
00:13:21,730 --> 00:13:25,440
where they discussed what would be a good move

269
00:13:25,440 --> 00:13:30,600
to make life of packagers downstream easier

270
00:13:30,650 --> 00:13:32,480
So from the point of view of GNOME

271
00:13:32,480 --> 00:13:35,600
what should we be doing in order to make their life easier

272
00:13:35,600 --> 00:13:37,600
A reverse session

273
00:13:37,600 --> 00:13:42,380
??? developers on how to say in Debian

274
00:13:42,380 --> 00:13:46,530
packaging and you know, make them interact

275
00:13:46,530 --> 00:13:50,290
and learn how packaging...

276
00:13:50,340 --> 00:13:53,830
how upstream changes affect packaging

277
00:13:53,830 --> 00:13:55,830
and what they can or cannot do

278
00:13:55,830 --> 00:13:57,830
to make our life easier would be great

279
00:13:57,830 --> 00:14:06,070
And a newcomers IRC channel or mailing list specifically

280
00:14:06,070 --> 00:14:08,070
for dealing with newcomers

281
00:14:08,070 --> 00:14:10,070
issues would be great

282
00:14:10,070 --> 00:14:15,210
and even local packaging workshops,

283
00:14:15,210 --> 00:14:17,210
BoFs, and parties

284
00:14:17,210 --> 00:14:21,580
if this hasn't already been done it would be nice.

285
00:14:21,580 --> 00:14:26,800
And all the links to the resources I'm talking about

286
00:14:26,800 --> 00:14:28,800
are there in my blogpost

287
00:14:28,800 --> 00:14:33,670
you can go it's sindhus.bitbucket.org

288
00:14:33,670 --> 00:14:38,470
I'd like to thank Tassia and Ana for having

289
00:14:38,470 --> 00:14:44,300
giving me this opportunity to present remotely, thank you very much!

290
00:14:44,300 --> 00:14:47,830
"Thank you"

291
00:14:47,830 --> 00:14:49,830
Thank you for listening so far

292
00:14:49,830 --> 00:14:55,820
and I hope I've given some interesting insights on how

293
00:14:55,820 --> 00:14:57,820
we make newcomers experience

294
00:14:57,820 --> 00:14:59,820
easy in GNOME and I hope

295
00:14:59,820 --> 00:15:03,800
to stick around in the Debian community and meet

296
00:15:03,800 --> 00:15:06,590
you all in the next Debian conference

297
00:15:06,590 --> 00:15:08,590
Thank you so much Tassia and Ana

298
00:15:08,590 --> 00:15:10,590
for helping me put this together

299
00:15:10,590 --> 00:15:14,160
and I hope to see you soon!

300
00:15:14,160 --> 00:15:17,930
"Thank you"

301
00:15:17,930 --> 00:15:19,930
Thank you so much for listening this far

302
00:15:19,930 --> 00:15:21,930
it's a shame I cannot be there

303
00:15:21,930 --> 00:15:23,930
in person to say this

304
00:15:23,930 --> 00:15:25,930
Thank you Tassia and Ana for

305
00:15:25,930 --> 00:15:27,930
helping me put this together

306
00:15:27,930 --> 00:15:31,740
I hope to stay involved in Debian

307
00:15:31,740 --> 00:15:35,820
in the packaging scene, contributing to it

308
00:15:35,820 --> 00:15:39,280
and all the things that I've spoken about

309
00:15:39,280 --> 00:15:41,280
in this talk is available on my blog

310
00:15:43,500 --> 00:15:46,190
it's sindhus.bitbucket.org

311
00:15:46,190 --> 00:15:50,920
So if you have any feedback, please do email me

312
00:15:50,920 --> 00:15:52,430
it will be good to hear from you

313
00:15:52,430 --> 00:15:56,240
I hope you guys enjoy your time at this conference, thank you bye bye!
