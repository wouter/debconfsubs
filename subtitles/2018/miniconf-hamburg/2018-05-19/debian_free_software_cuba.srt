1
00:00:05,773 --> 00:00:06,472
Hi everybody

2
00:00:10,090 --> 00:00:16,569
I will share with you some experiences 
about activities that are taking place

3
00:00:18,028 --> 00:00:21,868
nowadays in Cuba and also related with
the Debian Project

4
00:00:23,467 --> 00:00:28,305
Who am I? I am Jonathan Bustillos Osornio 
(Jathan) also

5
00:00:29,556 --> 00:00:36,273
I am a Debian contributor and a Debian
user since 2008 and I have been helping

6
00:00:36,695 --> 00:00:42,100
in some teams like po-debconf templates,
translations in the Debian Spanish

7
00:00:42,946 --> 00:00:50,214
Translation Team since 2011 and on Debian
Reproducible Builds in 2017

8
00:00:53,011 --> 00:00:58,200
In Cuba there is a community of Free Software
users and developers who have been working

9
00:00:58,433 --> 00:01:02,682
on different activities and useful
developments to extend the use of Debian

10
00:01:03,053 --> 00:01:05,766
and other Free Software projects 
throughout the country

11
00:01:07,959 --> 00:01:16,159
The first group that we have is the GUTL
The Free Tecnologies Users Group, in

12
00:01:16,519 --> 00:01:21,857
Spanish - Grupo de Usuarios de Tecnologias
Libres, is an organisation in Cuba since

13
00:01:22,467 --> 00:01:26,336
the development of the GNU/Linux users
communities of the whole country.

14
00:01:27,030 --> 00:01:34,529
It pushes the usage of free technologies
organizing events, workshops, courses and

15
00:01:34,741 --> 00:01:37,687
many more activities of social impact with
Free Software.

16
00:01:39,185 --> 00:01:45,875
Some of this events are like the Free
Document Day related with Libreoffice

17
00:01:48,270 --> 00:01:56,784
And this are the pictures of the events,
also the FLISol the Latin American

18
00:01:58,768 --> 00:02:01,837
Festival Free Software Instalation

19
00:02:06,385 --> 00:02:09,547
Also recreation activities

20
00:02:16,243 --> 00:02:28,775
GUTL has a website in each they publish
many articles or news related with Debian

21
00:02:31,528 --> 00:02:33,650
and another Free Software projects.

22
00:02:33,650 --> 00:02:42,965
The GUTL was born in 2009 and since then
it has been one of the most influencial

23
00:02:43,256 --> 00:02:48,953
organisations in Cuba to learn, use and
share Free Software, hardware and knowledge

24
00:02:51,499 --> 00:02:57,892
The main work objectives of this 
organisation are technological sovereignty

25
00:02:58,363 --> 00:03:03,380
Social Comptroller, socialization of
Information, Project development

26
00:03:04,633 --> 00:03:09,337
International visibility for Cuba in the
field for free software and free

27
00:03:09,523 --> 00:03:10,542
Technologies.

28
00:03:13,152 --> 00:03:19,187
Now we will know about free software 
projects and it's achievements

29
00:03:22,775 --> 00:03:35,502
The GUTL group has created a repository 
called REPOGUTL which is an implementation

30
00:03:36,159 --> 00:03:42,035
of something called the Cuban Sourceforge,
by their developers and users

31
00:03:43,012 --> 00:03:47,953
where free software developers have the 
opportunity to publicize their creations

32
00:03:48,319 --> 00:03:53,220
and get feedback, promoting the migration
and use of Free Software to Cuban users.

33
00:03:55,501 --> 00:04:06,014
This is the system which you could
find in the website

34
00:04:07,708 --> 00:04:17,332
the repository generated with Cuban free
software called Alfidi is structured in two

35
00:04:17,707 --> 00:04:24,414
major categories, these are applications
and products, with subcategories under them

36
00:04:25,445 --> 00:04:32,024
and they include the title of the application,
the author, contacts if it is desired by the

37
00:04:32,330 --> 00:04:37,954
creator, size, a brief description and a
long description in addition to the

38
00:04:38,320 --> 00:04:40,472
instructions for installation.

39
00:04:41,993 --> 00:04:48,703
These are the main developers of REPOGUTL
and maintainers, Maikel Enrique Pernia Matos

40
00:04:49,308 --> 00:04:51,139
And Delio Orozco González.

41
00:04:53,054 --> 00:04:58,077
Also we have a free software project called
MISOX

42
00:05:00,924 --> 00:05:05,925
Misox is of set of Free Software
applications developed by Maikel Pernía

43
00:05:06,172 --> 00:05:10,647
Matos , Delio Orozco González and
Alberto Miguel Nuevo Rojo in the

44
00:05:10,859 --> 00:05:16,515
province of Manzanillo Granma in Cuba, 
under the GNU GPL license and also

45
00:05:16,756 --> 00:05:26,772
packaged as .deb for our debian distribution
with the purpose of helping computer users

46
00:05:27,138 --> 00:05:31,873
to create a customization of Debian 
GNU/Linux adjusting it to their necessities

47
00:05:32,162 --> 00:05:36,279
since an easy and well integrated 
environment.

48
00:05:38,119 --> 00:05:46,982
These are some screenshots of the program,
it's main purpose is to facilitate some

49
00:05:47,590 --> 00:05:57,783
computer users customization of debian
GNU/Linux operating system and to put

50
00:05:58,184 --> 00:06:11,762
the packages the users want, to set the
graphical desired interface like

51
00:06:12,160 --> 00:06:16,658
some desktop enviroment with Gnome
Kde or other windows manager like

52
00:06:17,408 --> 00:06:21,789
Fluxbox, Awesome, I3.

53
00:06:24,010 --> 00:06:33,490
And also it offers the possibility to add
individual debian packages from an

54
00:06:34,072 --> 00:06:35,210
external source.

55
00:06:38,296 --> 00:06:46,114
There is another project called VideoMorph
which is a GUI wrapper for ffmpeg,

56
00:06:46,490 --> 00:06:56,109
based on code from python-video-converter
and presets idea from QWinFF and FF Multi

57
00:06:56,368 --> 00:07:02,843
Converter, developed by Ozkar L. Garcell, 
Leodanis Pozo Ramos and Leonel Salazar

58
00:07:03,184 --> 00:07:04,449
Videaux in Holguín, Cuba.

59
00:07:06,800 --> 00:07:08,543
And this is the screenshot.

60
00:07:09,845 --> 00:07:18,979
The mais goal of VideoMorph is to focus
is a single purpose making video conversion

61
00:07:19,621 --> 00:07:25,334
simple with an easy to use GUI and allowing
the user to convert to the currently most

62
00:07:25,539 --> 00:07:27,127
popular video formats.

63
00:07:29,249 --> 00:07:39,155
These are the colleagues that are working
on the group of GUTL in Holguín.

64
00:07:41,290 --> 00:07:50,018
And they also organize activities to talk
about some free software community

65
00:07:50,546 --> 00:08:00,020
or present some program to their users.
And they have also created a website

66
00:08:00,430 --> 00:08:08,479
called Debian GNU/Linux in Holguín, 
that is Debian Hlg, and in this website

67
00:08:08,742 --> 00:08:19,077
they publish news related to Debian,
events that are organized in this part

68
00:08:19,271 --> 00:08:28,937
of the far east of Cuba in Holguín, and 
it's a very nice way to stay in contact

69
00:08:29,459 --> 00:08:31,429
with the local users.

70
00:08:32,514 --> 00:08:43,789
Some achievements of the GUTL in Holguín
is one of the most active communities

71
00:08:44,020 --> 00:08:52,924
in Cuba, because they are a lot of members,
and they have the support of the university

72
00:08:53,487 --> 00:09:00,767
of Holguín, for some activities.
We have also an official member of the

73
00:09:01,195 --> 00:09:10,088
document foundation, Carlos Parra Zaldivar
and he's working with libreoffice community

74
00:09:10,280 --> 00:09:11,618
directly.

75
00:09:12,107 --> 00:09:15,142
Also there are four free software active
developers,

76
00:09:16,557 --> 00:09:18,950
there are two programs in development,

77
00:09:19,607 --> 00:09:22,756
and two active repositories in GitHub.

78
00:09:25,941 --> 00:09:32,102
Beside this it would have three websites
with international visibility like Debian

79
00:09:33,006 --> 00:09:37,488
in Holguín, and Python Scouts and 
Libreoffice cubava

80
00:09:40,158 --> 00:09:48,204
Sometimes the Holguín free software
community has a space to talk about free

81
00:09:48,439 --> 00:09:53,947
technologies at a TV program of Cuba called
"A Buen Tiempo", there is something like

82
00:09:54,551 --> 00:09:56,099
[???] time.

83
00:09:58,352 --> 00:10:04,294
By the other hand we have a lot of activity
in Granma province, that is also in the East

84
00:10:05,062 --> 00:10:06,773
of Cuba

85
00:10:08,261 --> 00:10:12,355
This is the place in which more Free
Software projects have been developed

86
00:10:13,209 --> 00:10:23,262
in island, also REPOGUTL was hosted here,
until some months ago, because now the

87
00:10:23,495 --> 00:10:33,945
organization was supporting the group
it is not [???] anymore, to give the hosting

88
00:10:34,342 --> 00:10:38,787
because of institutional restrictions.

89
00:10:40,203 --> 00:10:47,916
Many programs in development,
also there are one GUTL website editor,

90
00:10:48,845 --> 00:10:53,852
and there is a free software active
developer.

91
00:10:56,525 --> 00:11:03,355
In Santiago de Cuba also we have a 
GUTL website editor, and they have created

92
00:11:04,202 --> 00:11:12,719
a blog called SWL-X, that is a a web blog
related to free software activities

93
00:11:14,301 --> 00:11:18,550
and there is a free software active developer

94
00:11:19,072 --> 00:11:30,068
So now i will talk about some events that 
has been taking place during the last two

95
00:11:30,327 --> 00:11:36,104
years, this is the CubaConf and these the 
Free Technologies Cuban Conference

96
00:11:37,872 --> 00:11:43,180
CubaConf, is an international conference about
Free Software and open technologies that have

97
00:11:43,456 --> 00:11:52,748
taken place in Havana, Cuba since 2016.
CubaConf is a meeting from the international

98
00:11:52,966 --> 00:12:00,708
community of enthusiasts of many parts of
the world besides the Cuban local communities,

99
00:12:01,754 --> 00:12:07,480
peer to peer focused and collective
development. It seeks the participation of people

100
00:12:07,930 --> 00:12:11,994
with different profiles and diverse geographical
locations.

101
00:12:14,753 --> 00:12:27,790
[???] CubaConf that was on 2016, we have
here the last that was in November of last

102
00:12:28,171 --> 00:12:38,049
year and we had the opportunity to have
some Debian developers, for instance in

103
00:12:38,347 --> 00:12:56,097
2016 Tiago Bortolleto and Tassia Camões
went to CubaConf and they pushed to

104
00:12:56,341 --> 00:13:06,252
create with Leonel Salazar a Cuban
free software developer to organize

105
00:13:06,524 --> 00:13:10,568
for the first time the mini-debconf
in Cuba.

106
00:13:14,120 --> 00:13:19,289
From the organization of the conference
it is aimed at the diversity of topics and

107
00:13:19,557 --> 00:13:25,063
projects, as of its participants, so the 
participation of women, minority groups

108
00:13:25,490 --> 00:13:31,480
in these activities is crucial to build new 
inclusive spaces of knowledge and dialogue

109
00:13:31,914 --> 00:13:36,996
about technology. The main idea is to know 
people experience and organizations who

110
00:13:37,392 --> 00:13:44,770
work with and in Free Software, with old 
hardware technologies and little or slow

111
00:13:45,068 --> 00:13:49,743
bandwidth, to talk about how Free Software
can help developing countries.

112
00:13:53,057 --> 00:14:06,761
During the last CubaConf we had a great
number of girls participating with talks

113
00:14:07,324 --> 00:14:19,430
and even attending the event and that was
very motivating, we also had a talk with Molly

114
00:14:19,643 --> 00:14:27,280
de Blanc of the Free Software Foundation
and she did a very nice presentation

115
00:14:28,470 --> 00:14:33,432
And this is the global photo from last year

116
00:14:38,693 --> 00:14:52,484
Now I will talk about the mini debconf,
CubaConf 2016 these encounter between

117
00:14:54,882 --> 00:15:04,704
Tassia, Tiago and Leonel, has pushed to
organize for the first time the mini debconf

118
00:15:04,928 --> 00:15:06,198
in Cuba.

119
00:15:07,260 --> 00:15:14,486
This college is Leonel Salazar Videaux
and he's one of the founders of UTL and

120
00:15:14,767 --> 00:15:21,277
Debian GNU(Linux in Holguín, he's a free
software user since 2004

121
00:15:23,081 --> 00:15:26,365
First time using Debian GNU/Linux was with
“Sarge”,

122
00:15:27,561 --> 00:15:32,048
he has been spreading the Debian Project
in local communities in Cuba and organizing

123
00:15:32,528 --> 00:15:37,245
events as MiniDebConf. He is a programmer
has a profession.

124
00:15:40,597 --> 00:15:53,465
So during last mini debconf we had some topics
like how about to help the debian project

125
00:15:53,724 --> 00:16:05,133
from Cuba, the Cuban community share 2
main projects where Misox and VideoMorph

126
00:16:06,457 --> 00:16:17,569
developers showed how is the development
of the programs and how to use them

127
00:16:17,940 --> 00:16:25,449
Also I had the opportunity to present a talk
about reproducible builds for first time in

128
00:16:25,682 --> 00:16:39,622
Spanish and it was very useful to attract
new debian enthusiasts users and also to

129
00:16:39,831 --> 00:16:46,926
the currently developers like Mike and Oscar
from Videomorph and Misox,

130
00:16:48,130 --> 00:16:57,715
it was a new topic to start taking care with
the principles of the reproducible builds

131
00:16:58,302 --> 00:17:05,718
in the construction of then new versions
for their development with Misox and

132
00:17:06,236 --> 00:17:07,361
Videomorph.

133
00:17:12,235 --> 00:17:16,848
So much of this work has been carried 
out by enthusiastic and committed people

134
00:17:17,111 --> 00:17:20,365
with Debian and the Free Software 
movement, that despite the different

135
00:17:20,538 --> 00:17:25,815
difficulties to sustain some costs
economically, access the Internet, acquire

136
00:17:26,036 --> 00:17:30,174
computer hardware and achieve official
recognition as developers of some

137
00:17:30,502 --> 00:17:35,748
Free Software community in the world 
due to all the above, they continue to

138
00:17:35,950 --> 00:17:40,660
contribute in a decisive way projects
that should be available to everyone

139
00:17:40,876 --> 00:17:42,301
inside and outside of Cuba.

140
00:17:42,877 --> 00:17:50,602
Because for instance the Misox project
Videomorph and many other programs

141
00:17:51,067 --> 00:18:00,632
are hosted in the REPOGUTL by the
limitations of hardware, money

142
00:18:01,665 --> 00:18:06,834
available to the Cuban users.

143
00:18:09,155 --> 00:18:19,139
In that way I consider that it would be very
interesting and important to think

144
00:18:19,591 --> 00:18:28,523
about how the Debian could support and 
recognize those who contribute directly to

145
00:18:28,709 --> 00:18:33,121
the use and development of our 
distribution, in places where there

146
00:18:33,487 --> 00:18:39,784
is a willingness to do so and strengthen 
our community by being inclusive of them?

147
00:18:40,264 --> 00:18:50,685
Because we know the Debian Social Contract,
our guidelines, but how can we

148
00:18:53,134 --> 00:19:04,274
considered to those that are in a country
in which you could connect to the

149
00:19:04,426 --> 00:19:09,364
internet but it's very expensive and 
if you want to interact with the mailing

150
00:19:09,664 --> 00:19:14,418
list or you want to stay in contact with
another debian maintainer or debian

151
00:19:14,592 --> 00:19:22,383
developers it's not so easy, because simple
you don't have the conditions

152
00:19:22,944 --> 00:19:37,138
So I want to leave the question to have a 
dialog and to see what do you think, which

153
00:19:37,389 --> 00:19:44,915
possibilities are in the debian project to
support for instance the Misox development

154
00:19:45,785 --> 00:19:54,011
that is based on debian and even the packages
will be in the repositories in the free section

155
00:19:57,962 --> 00:20:03,300
Well that is all, and thank you very much for
your attention

156
00:20:04,457 --> 00:20:10,055
[Applause]

157
00:20:13,278 --> 00:20:14,915
Any comments or questions?

158
00:20:38,530 --> 00:20:43,607
[Q:] The Misox distribution is it
registrated with the derivatives sensus do

159
00:20:43,929 --> 00:20:47,521
you know if they already collaborate with
debian in that way?

160
00:20:48,668 --> 00:20:51,198
That's the first thing that comes to my mind

161
00:20:52,803 --> 00:20:58,050
[A:] No the Misox development is driven
just by local developers in Cuba,

162
00:20:58,590 --> 00:21:01,781
by Maikel , Delio and Alberto but

163
00:21:02,022 --> 00:21:08,806
they have not done contact with some
debian developers outside.

164
00:21:12,618 --> 00:21:21,181
Then would be my first suggestion, for them
to just inform debian in a relatively

165
00:21:21,519 --> 00:21:26,860
formalized way what is this they are doing,
with derives from debian how is it different

166
00:21:27,648 --> 00:21:32,091
That's an helpful thing for me and I 
believe also for others in debian and also

167
00:21:32,280 --> 00:21:37,506
from outside debian to compare, and then
getting [???] what would be interesting

168
00:21:38,537 --> 00:21:44,503
to cherry pick and put into debian,
and what would be important to coordinate

169
00:21:45,386 --> 00:21:50,821
So the very reference that they exist
in a more formal way it's not a lot of work

170
00:21:51,126 --> 00:21:57,356
just a wikipage to inform, who to contact
how to contact, i remember once

171
00:21:57,697 --> 00:22:04,386
looking at Misox webpage and trying to
navigate, and I was lost in locating who

172
00:22:04,610 --> 00:22:10,981
should I even contact, how is it structured
and that's partially a cost because I

173
00:22:11,144 --> 00:22:19,532
don't understand Spanish , so that's one
[???] personally , but that could be other

174
00:22:19,689 --> 00:22:25,195
things also, how do they put the information
for their own users, and how to present it

175
00:22:25,473 --> 00:22:26,861
for debian collaboraters.

176
00:22:27,059 --> 00:22:28,361
Ok, thank you...

177
00:22:53,074 --> 00:22:58,305
[Q:] Just want to ask how are yourself
related to the Cuban community?

178
00:22:59,506 --> 00:23:12,760
[A:] I was in contact with some developers
like Leonel, Maikel, Delio because the

179
00:23:12,979 --> 00:23:24,281
CubaConf and the mini-debconf have
this knowledge, beyond than this

180
00:23:25,474 --> 00:23:33,117
I don't know if there are even more 
projects that are underground more

181
00:23:33,497 --> 00:23:42,844
than this, so yes this is the only contact
that I have with the Cuban developers

182
00:23:43,835 --> 00:23:45,198
of free software.

183
00:23:52,194 --> 00:24:00,160
[Q:] Can you tell a bit more about the kind
of barriers, I understand the one that is

184
00:24:00,395 --> 00:24:05,969
expensive, so technically possible to get
online but expensive, but are there other

185
00:24:06,207 --> 00:24:11,695
kind of barriers? Is there a wall like in
China, what kind of barriers is there for

186
00:24:11,861 --> 00:24:15,821
the developers in Cuba to collaborate
with others in Debian?

187
00:24:17,494 --> 00:24:24,070
[A:] My opinion I consider that the main
barrier is to pay to connect to the internet

188
00:24:24,911 --> 00:24:31,185
because in free software movement the 
connection to the internet has been very

189
00:24:31,353 --> 00:24:42,401
important to stay in touch with another 
developers or interested users , so

190
00:24:42,862 --> 00:24:51,174
in the case of Cuba they have an internet
but if they want to socialize their programs

191
00:24:52,340 --> 00:25:05,917
outside the island you have to pay for
instance 5 [???] that is more or less 4 euro

192
00:25:05,917 --> 00:25:15,292
per 5 hours of internet and not always the
connection is good, because you need to

193
00:25:15,521 --> 00:25:26,610
go to public parks or you need to go to
some monument, a public place which

194
00:25:26,797 --> 00:25:35,386
there are access points and then you use 
your card and try to get an IP, but this is

195
00:25:35,571 --> 00:25:42,452
really hard sometimes, you could wait 20, 
25 minutes, because there are a lot of

196
00:25:42,641 --> 00:25:51,209
people with smartphones, and more devices
and even if you , in my personal situation

197
00:25:51,502 --> 00:25:58,377
when i was in Cuba, I tried to connect to
the internet and I delayed around 35, 40

198
00:25:58,614 --> 00:26:07,240
minutes to get an IP to my computer,
so first it's the main obstacle to

199
00:26:07,510 --> 00:26:13,918
communicate with another people outside
using internet.

200
00:26:16,557 --> 00:26:25,211
By other hand the hardware, the weather
conditions, it is very hot, and they do not

201
00:26:25,407 --> 00:26:37,113
have air conditioner systems to maintain
community projects or self driven servers

202
00:26:38,382 --> 00:26:46,750
Unless you are working in some institution
or in the government or in a university,

203
00:26:46,998 --> 00:26:55,199
maybe you could have a chance to host 
your project in a local computer and

204
00:26:55,406 --> 00:27:04,913
then use a public IP, maybe , but if you 
are notworking in these fields of technology

205
00:27:05,346 --> 00:27:11,044
in Cuba, is very hard, if you are an
independent and you want to contribute

206
00:27:14,966 --> 00:27:23,752
and let it know more people about your
project you need to connect to the network

207
00:27:24,709 --> 00:27:31,574
and you need to pay also because internet
is not money free in Cuba.

208
00:27:34,566 --> 00:27:44,261
I was also thinking in the kind of tools, 
does it feel from the perspective of

209
00:27:44,530 --> 00:27:48,888
Cubans, does it feel [???] social for
debian developers to be hanging

210
00:27:49,097 --> 00:27:56,963
out on IRC so they need to be
real online, would it be more beneficial

211
00:27:57,211 --> 00:28:02,472
to them if some of us is using mail more
or mailing list more, or IRC more, forms more

212
00:28:02,791 --> 00:28:09,733
[???] you have noticed, the styles of 
communication could be helpful for this

213
00:28:09,973 --> 00:28:11,484
kind of barriers.

214
00:28:12,024 --> 00:28:19,327
I think that if that would be possible,
if the Cuban developers could at least use

215
00:28:19,525 --> 00:28:30,748
IRC and communicate with some Debian 
maintainers or developers, maybe an

216
00:28:31,099 --> 00:28:43,902
interchange of knowledge , they could learn
more from you and also you could take

217
00:28:44,226 --> 00:28:54,152
their packages into some GIT lab server or 
you could be some kind of Debian sponsor

218
00:28:54,749 --> 00:29:10,712
to be public and allow the public access
outside of Cuba to this software, for

219
00:29:10,882 --> 00:29:19,307
instance Misox or another one from RepoGUTL
and maybe you could help in that way, but

220
00:29:22,268 --> 00:29:29,701
Cuban developer needs to connect to the 
network, to be in constant communication.

221
00:29:35,116 --> 00:29:42,512
[Q:] What is the best place to learn about
Debian events related in Cuba in case I have

222
00:29:42,752 --> 00:29:49,018
any way over the Atlantic and I think: oh,
could I pass by Cuba and [???] question.

223
00:29:49,260 --> 00:29:56,499
Do you know whether there are any problems
to go to the US via CUBA or going from the

224
00:29:56,761 --> 00:29:58,964
US ,Cuba and then back to Europe?

225
00:30:00,596 --> 00:30:11,557
[A:] I think there are some restrictions for
United States citizens to visit Cuba unless

226
00:30:12,195 --> 00:30:22,794
you argument some research or education
causes, but if you are coming from Europe

227
00:30:24,904 --> 00:30:33,815
there's no problem, there's no restriction
for European citizen or Latin American

228
00:30:34,096 --> 00:30:43,925
citizens or Asian to be in Cuba, to work
with technology topics with the local

229
00:30:44,183 --> 00:30:45,123
people.

230
00:30:45,480 --> 00:30:49,120
[Q:] And how to learn about events? Is there
a Cuban mailing list or something like that?

231
00:30:51,235 --> 00:30:56,284
[A:] Not a mailing list but there is the
RepoGUTL website.

232
00:31:26,329 --> 00:31:36,374
This is the main groups in which you could
stay in contact with the Cuban community

233
00:31:37,052 --> 00:31:49,747
and also you could write through the contact
section of the RepoGUTL website,

234
00:31:51,592 --> 00:32:00,070
this is the general way of communicate with
the more active Cuban developers.

235
00:32:12,501 --> 00:32:18,632
There are some projects that are hosted in
GIT Lab like Videomorph

236
00:32:41,237 --> 00:32:46,682
And here are the contact of some
developers, but this is very particular

237
00:32:47,049 --> 00:32:53,168
you need to know the name of the program
because in RepoGUTL there are more that

238
00:32:54,104 --> 00:33:01,944
50 different programs many of them are 
developed by the same guy but by the

239
00:33:02,192 --> 00:33:09,325
other hand there are other ones that
even doesn't have a GIT Lab or GIT Hub

240
00:33:09,958 --> 00:33:22,111
server in this case the better way is 
to do it by the GUTL Website to contact

241
00:33:22,390 --> 00:33:29,835
the community of developers of free
software in Cuba.

242
00:33:42,395 --> 00:33:43,613
Any further questions ?

243
00:33:48,454 --> 00:33:54,336
[Q:] Absolutely not an expert but i have
heard of stories of people building their

244
00:33:54,726 --> 00:34:05,815
own ad-hoc network in countries in which
internet is not well connected, for instance

245
00:34:06,305 --> 00:34:15,032
using the baboo protocol and would it be
possible to build your own connection to

246
00:34:15,312 --> 00:34:17,818
the internet using that kind of technology?

247
00:34:22,062 --> 00:34:33,480
[A:] Well I have not realized about this,
I have to say I'm not Cuban, so I have

248
00:34:35,480 --> 00:34:45,001
been trying to help our collegues, but as
you have said I don't know if they have

249
00:34:45,261 --> 00:34:56,076
tried to use this alternatives, or if it
even works, but well it's nice to know it

250
00:34:57,023 --> 00:34:58,124
and thank you.

251
00:35:05,453 --> 00:35:07,897
So any further question or comments?

252
00:35:08,674 --> 00:35:10,436
If not then I thank you again!

253
00:35:11,103 --> 00:35:15,792
[Applause]
