#!/usr/bin/env python3
# Sync subtitles from amara into a local folder (SUBS_FOLDER)
# Replaces old versions of subtitles if found
#
# Sanity checks done by this script:
# - Check that every video has a project. If it doesnt, it tries to assign one to them
# - Check local paths and craeted them
#
# This contain funtions able to rename projects in a sane way (ie. creating a new project
# and moving videos to it. Amara cant just rename a project)
#
# WIP: upload local subtitles to amara

import json, sys, os
import re
import requests
from pprint import pprint
from os.path import splitext, basename
from urllib.parse import urlparse
from glob import glob

JSON_FILE = '.amara.video.list.json'

SUBS_FOLDER = '../../subtitles'
#SUBS_FOLDER = 'subs'

AMARA_BASE_URL = 'https://www.amara.org/'

# set to None to skip fixing videos without project or the name of the project
# ex. update_videos_without_project_with_project = '2016-debconf16'
update_videos_without_project_with_project = '2014_debconf14'

#load credentials
if not os.path.isfile('amara_credentials'):
    print("this script needs a file called amara_credentials with the apikey and the username. ex:")
    print("gohwie8ighoh6Fae5me3chahl username")
    print("ask for it if you want to do something or test this script")
    exit(1)

with open('amara_credentials', "r") as f:
    API_KEY, USERNAME = f.read().split()[0:]
    print('Using Amara username: '+USERNAME)
    print('Using Amara API key: '+API_KEY)

amara_headers = {
   'Content-Type': 'application/json',
   'X-api-username': USERNAME,
   'X-api-key': API_KEY,
   'format': 'json'
}



def amara_call(uri, body = None, text=False, put=False, post=False):
    try:
        if put:
            r = requests.put(uri, json=body, headers=amara_headers)
        else:
            if post:
                r = requests.post(uri, json=body, headers=amara_headers)
            else:
                r = requests.get(uri, params=body, headers=amara_headers)
        r.raise_for_status()

        if text:
            response = r.text
        else:
            response = r.json()

    except requests.HTTPError as e:
        response = None
        print(e)
        print("request body: " + body)

        # print the json with details on bad request (400)
        if r.status_code == 400:
            print(r.json())

        # dont exit on 404 or anything catched previosly
        elif r.status_code != 404:
            sys.exit(1)

    return response

def add_url_to_video(videoid, new_uri):
    url = AMARA_BASE_URL + '/api/videos/' + videoid + '/urls/'

    body = {
        'url' : new_uri,
    }
    amara_call(url, body, post=True)

def add_video(uri, lang, project):
    url = AMARA_BASE_URL + 'api/videos/'

    body = {
        'video_url' : uri,
        'primary_audio_language_code' : lang,
        'team' : 'debconf',
        'project' : project
    }
    amara_call(url, body, post=True)

def create_project(project_name):
    url = AMARA_BASE_URL + 'api/teams/debconf/projects/'
    body = {
        'name': project_name,
        'slug': project_name
        }
    amara_call(url, body, post=True)

def get_video_id_by_url(video_uri):
    url = AMARA_BASE_URL + 'api/videos/'
    body = {
        'video_url': video_uri,
        }
    json_response = amara_call(url, body)
    id = None
    if json_response['meta']['total_count'] > 0:
        id = json_response['objects'][0]['id']
    else:
        print("no video found in amara with " + video_uri)
        pprint(json_response)
    return id

def list_videos():
    limit = 100
    url = AMARA_BASE_URL + 'api/videos/'
    videos = []
    body = {
        'limit': limit,
        'team': 'debconf'
        }

    # the response is paginated so we have to iterate to get everything
    while True:
        json_response = amara_call(url, body)

        #pprint(json_response["objects"])
        videos += json_response["objects"]

        url = json_response['meta']['next']

        if not url or not limit :
            break

    return videos

def update_all_videos_file():
    print('UPDATING CACHE FILE')
    videos = list_videos()
    with open(JSON_FILE, 'w') as f:
        json.dump(videos, f, sort_keys=True, indent=4)
    print('UPDATING CACHE FILE DONE: ' + JSON_FILE + ' created')

def list_videos_without_project(videos, urls = False):
    ids = []
    for v in videos:
        if not v['project']:
            if urls:
                pprint(v['all_urls'])
            ids.append(v['id'])
    return ids

def check_video(amara_id):
    url = AMARA_BASE_URL + 'api/videos/' + amara_id
    json_response =  amara_call(url)

    return json_response

def update_video_project(amara_id, project):

    url = AMARA_BASE_URL + 'api/videos/'+amara_id+'/'
    body = {
        'project': project,
        'team': "debconf"
        }
    json_response = amara_call(url, body, put=True)

    return json_response

def update_videos_project(ids, project):
    for v in ids:
        update_video_project(v, project)

# renaming a project changes the project description, but no the project slug
# in order to rename a project we need to create the new project and rename videos individually
# the new project slug must not exist or this will fail badly
def rename_project(videos, old_project, new_project = update_videos_without_project_with_project):
    create_project(new_project)
    for v in videos:
        if v['project'] == old_project:
            update_video_project(v['id'], new_project)

def check_and_fix_videos_without_project(videos):
    if list_videos_without_project(videos):

        print("ATTENTION: video without project:")
        ids = list_videos_without_project(videos, urls = True)


        if update_videos_without_project_with_project:
            reply = input("Update videos with project " + update_videos_without_project_with_project + "? [y]/n")
            if not reply or reply == 'y':
                print('UPDATING VIDEOS without project with '+update_videos_without_project_with_project + ' project')
                update_videos_project(ids, update_videos_without_project_with_project)

                #after updating videos with project, update cache
                update_all_videos_file()
            else:
                print("All videos must have a project")
                exit(1)

def download_sub(subtitles_uri):
    subtitles_uri += 'subtitles/?format=srt'
    sub_text = amara_call(subtitles_uri, text = True)
    return sub_text

def year_and_event_from_project(project):
    return re.split('_', project)

def path_from_project(project):
    folders = year_and_event_from_project(project)
    path = os.path.join(SUBS_FOLDER, *folders)
    return path

#filename = raw name without extension or path
def download_and_save_sub(filename, project, subtitles_uri):

    json_response = amara_call(subtitles_uri)

    year, event = year_and_event_from_project(project)

    lang = json_response['language_code']
    remote_version = json_response['num_versions']
    local_version = local_subs[year][event][lang][filename]['version']

    # some subs have url despite not existing (404)
    # so we skip it when remote is not at least 1
    if remote_version > 0:

        #TODO: if there is a local version but not remote version or the version is higher, upload it
        if not local_version or int(local_version) < remote_version:
            # get sub file
            sub = download_sub(subtitles_uri)
            if not sub:
                print("Cant download subs file for: " + subtitles_uri)
                return


            #mark subtitles that are WIP
            finished = None
            if not json_response['subtitles_complete']:
                finished = 'UNFINISHED'

            #filter to remove finished from the components when is None
            filename_with_extensions_components = filter(None, [filename, finished, str(remote_version), lang, 'srt'])
            filename_with_extensions = '.'.join(filename_with_extensions_components)
            new_file_path = os.path.join(path_from_project(project), filename_with_extensions)

            with open(new_file_path, 'w') as f:
                f.write(sub)
            print("New version (" + str(remote_version) + ") for " + filename + " (" + lang + ") downloaded")

            #if there is an old version left, we remove it.
            #This only targets files that use the schema: name[.UNFINISHED].version.lang.srt
            if local_version:
                filename_with_extensions = '.'.join((filename , '*', lang, 'srt'))
                print(filename_with_extensions)
                old_paths = os.path.join(path_from_project(project), filename_with_extensions)
                for local_file in glob(old_paths):
                    #skip the newer version
                    if local_file != new_file_path:
                        reply = input("Delete old sub " + local_file + "? [y]/n")
                        if not reply or reply == 'y':
                            os.remove(local_file)

def check_paths_and_create_them(videos):
    projects = set()
    for v in videos:
        projects.add(v['project'])

    for p in projects:
        path = path_from_project(p)
        if not os.path.isdir(path):
            os.makedirs(path, exist_ok=True)

# create local subs structure like:
#{'2014': {'debconf14': {'en': {'Coming_of_Age_My_Life_with_Debian': {'finished': False,
#                                                                     'version': '3'},
#                               'Debian_in_the_Dark_Ages_of_Free_Software': {'finished': True,
#                                                                            'version': '38'}, ... }}}}
# '2015': {'debconf15': {'en': {'Bits_from_the_DPL': {'finished': False,
#                                                     'version': '5'},
#                               'Citizenfour_Q_A_Session': {'finished': True,
#                                                           'version': '18'},...}}}}
def collect_local_subs():
    class autodict(dict):
        def __missing__(self, key):
            value = self[key] = type(self)()
            return value

    local_subs = autodict()

    SUBRE = re.compile('(.+?)(\.UNFINISHED)?\.(\d+)\.(.+)\.srt')

    #blackmagic: glob('*/') = list only folders. It assumes all folders are years
    years = glob(SUBS_FOLDER + '/*/')

    for year in years:
        year = os.path.normpath(year)
        for root, dirs, files in os.walk(year):
            for f in files:
                if f.endswith('.srt'):
                    m = SUBRE.match(f)
                    if m:
                        event = os.path.basename(root)
                        y = os.path.basename(year)
                        finished = not bool(m.group(2))
                        local_subs[y][event][m.group(4)][m.group(1)] = { 'version' : m.group(3),
                                                                            'finished' : finished }

    #pprint(local_subs)
    return local_subs

if __name__ == '__main__':
    #if cache file does not exist, update it
    if not os.path.isfile(JSON_FILE):
        update_all_videos_file()

    #read cache file
    with open(JSON_FILE) as f:
        videos = json.load(f)

    #sanity check
    check_and_fix_videos_without_project(videos)
    check_paths_and_create_them(videos)

    #GLOBALs
    local_subs = collect_local_subs()
    #rename_project(videos, "2016-debconf16", "2016_debconf16")
    #rename_project(videos, "2015-debconf15", "2015_debconf15")
    #rename_project(videos, "2014-debconf14", "2014_debconf14")
    #rename_project(videos, "2013-debconf13", "2013_debconf13")
    #rename_project(videos, "2014-mini_debconf_barcelona", "2014_mini-debconf-barcelona")
    #update_all_videos_file()

    for v in videos:
        for lang in v['languages']:

            download_uri = lang['resource_uri']

            #TODO: get filename from subtitles_uri and inside the download_and_save_sub
            #black magic to get the filename from the url
            filename =  basename(splitext(urlparse(v['all_urls'][0]).path)[0])

            download_and_save_sub(filename, v['project'], download_uri)


        else:
            pass
            #print('video without sub:' + v['all_urls'][0])

