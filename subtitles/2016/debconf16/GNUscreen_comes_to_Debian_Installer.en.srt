1
00:00:02,000 --> 00:00:08,000
Hello everyone, welcome to my presentation.

2
00:00:08,000 --> 00:00:17,000
I will introduce GNU/Screen to Debian Installer today.

3
00:00:19,000 --> 00:00:24,000
I will show you its background.

4
00:00:24,000 --> 00:00:26,000
What is Debian Installer,

5
00:00:26,000 --> 00:00:30,000
What is GNU/Screen,

6
00:00:30,000 --> 00:00:35,000
Why I want GNU/Screen in Debian Installer

7
00:00:35,000 --> 00:00:40,000
and How to add GNU/Screen support in it.

8
00:00:42,000 --> 00:00:47,000
First, The background.

9
00:00:47,000 --> 00:00:51,000
I know that most of you know the Debian Installer.

10
00:00:51,000 --> 00:00:55,000
It is just an installer.

11
00:00:55,000 --> 00:01:01,000
But actually, It is a bootable Debian environment,

12
00:01:01,000 --> 00:01:07,000
which is minimized in size.

13
00:01:08,000 --> 00:01:15,000
That is to say, there is no document in the image.

14
00:01:15,000 --> 00:01:34,000
There is no other non-replicated usually-useful information for normal people, which is usually putted in "/usr/share/doc".

15
00:01:34,000 --> 00:01:37,000
But installer stage, we don't need them.

16
00:01:37,000 --> 00:01:44,000
So, It contributes for minimizing environment.

17
00:01:44,000 --> 00:01:50,000
Previously, we have a installer in one or two floppy disks.

18
00:01:50,000 --> 00:01:53,000
It means that installer was very small.

19
00:01:53,000 --> 00:01:55,000
Nowadays, it is not possible.

20
00:01:55,000 --> 00:02:04,000
Because the kernel size is already over size of the floppy.

21
00:02:04,000 --> 00:02:14,000
Second, there is a partitioner inside the installer.

22
00:02:14,000 --> 00:02:24,000
So, you can do the partition and create a new disk image inside the installer.

23
00:02:24,000 --> 00:02:36,000
After that, we can do the "debootstrap" to install Debian environment for you.

24
00:02:36,000 --> 00:02:41,000
Finaly, the installer will install boot loader.

25
00:02:41,000 --> 00:02:53,000
There is all kinds of boot loader, such as <b>GRUB</b> for <i>PC</i> or for <i>ARM64</i>.

26
00:02:53,000 --> 00:03:07,000
For <i>ARM</i> platform, such as <i>armel</i> or <i>armhf</i>, we use <b>flash-kernel</b> as a boot loader.

27
00:03:12,000 --> 00:03:15,000
What is GNU/Screen?

28
00:03:15,000 --> 00:03:17,000
First, It is a terminal multiplexer.

29
00:03:17,000 --> 00:03:28,000
It means that you can have multiple virtual terminal in real terminal.

30
00:03:28,000 --> 00:03:42,000
And you can switch one virtual terminal to another virtual terminal by a shortcut.

31
00:03:42,000 --> 00:03:47,000
In GNU/Screen, the shortcut is "Ctrl-A + 1", 2, 3, 4.

32
00:03:47,000 --> 00:04:00,000
And Tmux (GNU/Screen alternatives)
use "Ctrl-B + 1", 2, 3, 4.

33
00:04:00,000 --> 00:04:14,000
Then you must be thinking why we need GNU/Screen in Debian Installer.

34
00:04:14,000 --> 00:04:19,000
What is the benefit?

35
00:04:19,000 --> 00:04:22,000
Let me show you normal installer.

36
00:04:22,000 --> 00:04:28,000
This is a normal installer.

37
00:04:28,000 --> 00:04:30,000
We have UI.

38
00:04:30,000 --> 00:04:42,000
And if we face some problem, such as partitioning or creating new disk image, we want to check the log.

39
00:04:42,000 --> 00:04:49,000
We can press the key "Alt-F4"
to switch to the log console.

40
00:04:49,000 --> 00:04:55,000
This is a case for normal PC.

41
00:04:55,000 --> 00:05:03,000
If you want to switch back,
we can press the "Alt-F1".

42
00:05:03,000 --> 00:05:06,000
I can show you the demo.

43
00:05:53,000 --> 00:05:56,000
This is a normal Debian Installer screen.

44
00:05:56,000 --> 00:06:00,000
I can enter "Alt-F2".

45
00:06:00,000 --> 00:06:06,000
Command line appears.

46
00:06:06,000 --> 00:06:09,000
I can see lots of stuff here.

47
00:06:09,000 --> 00:06:17,000
By "Alt-F4", you can see the logs here.

48
00:06:19,000 --> 00:06:22,000
If we want to turn back,
we can enter "Alt-F1".

49
00:06:22,000 --> 00:06:26,000
Then we go back.

50
00:06:26,000 --> 00:06:28,000
This is a case for normal PC.

51
00:06:28,000 --> 00:06:36,000
But, there is no such convenient methods
in embedded.

52
00:06:55,000 --> 00:06:58,720
For embedded device
such as <i>armel</i> or <i>armhf</i>

53
00:06:58,720 --> 00:07:09,000
we usually install using the serial console
or the SSH over the network.

54
00:07:09,000 --> 00:07:14,000
So, there is the only one real screen
for you.

55
00:07:14,000 --> 00:07:17,150
There is no other screen.

56
00:07:17,150 --> 00:07:19,000
Actually, there is a way.

57
00:07:19,000 --> 00:07:21,000
You can see the previous page.

58
00:07:21,000 --> 00:07:29,000
There is a "Go Back" option here.

59
00:07:29,000 --> 00:07:34,000
We can go back and we can have this menu.

60
00:07:34,000 --> 00:07:42,000
This is an advanced menu and you can enter a shell from here for embedded device.

61
00:07:42,000 --> 00:07:46,000
After you enter a shell,
you can check the logs.

62
00:07:46,000 --> 00:07:52,000
The log is in "/var/log/syslog".

63
00:07:52,000 --> 00:07:54,000
It is there, you can check it.

64
00:07:54,000 --> 00:07:58,000
But, if you want to do it,

65
00:07:58,000 --> 00:08:03,000
every time you have to go back
and enter the shell.

66
00:08:03,000 --> 00:08:05,000
You cannot do it all the time either.

67
00:08:05,000 --> 00:08:10,000
Because sometimes the installer is running to install packages.

68
00:08:10,000 --> 00:08:16,000
You cannot do it at anytime
like in a normal PC.

69
00:08:16,000 --> 00:08:18,000
I was just thinking,

70
00:08:18,000 --> 00:08:24,000
if we have GNU/Screen introduced
into the Debian Installer,

71
00:08:24,000 --> 00:08:36,000
we can have it simple and convenient as a normal PC in the embedded area like <i>armel</i>.

72
00:08:42,000 --> 00:08:47,000
So, I have started to work for it.

73
00:08:50,000 --> 00:08:54,000
So, How to achieve it?

74
00:08:54,000 --> 00:08:58,000
After some search, I found that

75
00:08:58,000 --> 00:09:07,000
I need to support GNU/Screen binary package and its dependencies such as library to support udeb.

76
00:09:07,000 --> 00:09:18,000
udeb is special format for Debian Installer
which minimize installer image.

77
00:09:18,000 --> 00:09:22,000
That is to say,
it remove documents for example.

78
00:09:22,000 --> 00:09:32,000
So, we have to support udeb
for GNU/Screen and its library.

79
00:09:33,000 --> 00:09:40,000
And, we need to add those udebs
into the Debian Installer image.

80
00:09:42,000 --> 00:09:52,000
The third one is that we need to write some script to start GNU/Screen in Debian Installer.

81
00:09:55,000 --> 00:10:00,000
First, udeb support.

82
00:10:05,000 --> 00:10:13,000
Normally, udeb is controlled by "debian/control".

83
00:10:15,000 --> 00:10:19,000
it is like every binary packages
for the source packages.

84
00:10:19,000 --> 00:10:28,000
So we just add additional section
for supporting a udeb (new binary).

85
00:10:28,000 --> 00:10:38,000
After that, we just create a new "-udeb.install" file.

86
00:10:38,000 --> 00:10:49,000
And it is almost identical to the original ".install" file, but entries for unnecessary file are removed.

87
00:10:49,000 --> 00:10:53,000
After that we can create a patch.

88
00:10:53,000 --> 00:11:05,000
Then submit it to package maintainer
to let him or her to incorporate my changes.

89
00:11:05,000 --> 00:11:12,000
We add the new udeb package.

90
00:11:12,000 --> 00:11:15,000
But, if the package is maintained by DM,

91
00:11:15,000 --> 00:11:28,000
unfortunately DM need to ask DD
to sponsor the package to upload.

92
00:11:28,000 --> 00:11:35,000
For the same reason, the package will stay
in NEW queue in ftp-master.

93
00:11:35,000 --> 00:11:40,000
And we need to wait the ftp-master
to approve the upload.

94
00:11:40,000 --> 00:11:49,000
So, it takes much longer time
than normal packages.

95
00:11:52,000 --> 00:12:01,000
What I do is to add these udeb support
of GNU/Screen and it's dependencies.

96
00:12:01,000 --> 00:12:10,000
I created four Bug reports,
actually these are four bugs with patches.

97
00:12:28,000 --> 00:12:34,000
After that, we can have udeb package
that are ready to use.

98
00:12:34,000 --> 00:12:40,000
But, we still need to include
in the Debian Installer image.

99
00:12:40,000 --> 00:12:54,000
So, I patched to "debian-installer.git" repository
to support it.

100
00:12:56,000 --> 00:13:08,000
And finally we need to start GNU/Screen command
after we started Debian Installer.

101
00:13:08,000 --> 00:13:22,000
We also need GNU/Screen configuration to emulate Debian Installer environment like PC.

102
00:13:22,000 --> 00:13:29,000
We actually have four virtual screen
in the Debian Installer.

103
00:13:29,000 --> 00:13:37,000
First one is main screen, second is command
line console, and last is log console.

104
00:13:37,000 --> 00:13:42,000
We want to emulate them.

105
00:13:49,000 --> 00:13:54,000
I want to show you the demo.

106
00:14:01,000 --> 00:14:06,000
Actually, I run the Debian Installer
on virtual machine.

107
00:14:29,000 --> 00:14:34,000
This is the Debian Installer
with GNU/Screen support.

108
00:14:34,000 --> 00:14:40,000
So, we have four virtual terminals here,
1, 2, 3, 4.

109
00:14:40,000 --> 00:14:56,000
And I can switch terminal by a shortcut
such as "Ctrl-A + 2", "Ctrl-A + 3", and "Ctrl-A + 4".

110
00:14:56,000 --> 00:15:01,000
It is a log.

111
00:15:06,000 --> 00:15:12,000
As a PC side, as you see now,
it seems not much useful.

112
00:15:12,000 --> 00:15:17,000
Because we can use "Alt-F1", "Alt-F2",
and "Alt-F3" to switch.

113
00:15:17,000 --> 00:15:23,000
But for embedded side, It is much useful.

114
00:15:35,000 --> 00:15:40,000
I have a progress update.

115
00:15:40,000 --> 00:15:47,000
Actually these action items
are already done.

116
00:15:47,000 --> 00:15:53,000
For example, first two packages are for library.

117
00:15:53,000 --> 00:15:56,000
Actually, it was not needed at all.

118
00:15:56,000 --> 00:16:05,000
Because, I learned that from Mr. Laurent,

119
00:16:05,000 --> 00:16:09,000
GNU/Screen can be configured
to compile twice.

120
00:16:09,000 --> 00:16:24,000
First time is for normal GNU/Screen and second time is for the one which does not use <i>audit</i> and <i>pam</i> library.

121
00:16:24,000 --> 00:16:32,000
So, it can make GNU/Screen udeb binary minimized.

122
00:16:32,000 --> 00:16:38,000
Actually, library which needs
to be really supported is <i>ncurses</i>.

123
00:16:38,000 --> 00:16:49,000
<i>ncurses</i> has been uploaded to the ftp-master two weeks ago with the GNU/Screen binary package.

124
00:16:49,000 --> 00:17:06,000
For Debian Installer image, I just pushed a commit after Debian Installer Stretch Alpha 7 was released this week.

125
00:17:06,000 --> 00:17:10,000
Since I just pushed the commit
after the release,

126
00:17:10,000 --> 00:17:21,000
you can now try GNU/Screen
in Debian Installer daily image.

127
00:17:29,000 --> 00:17:32,000
Which device will benefit from it?

128
00:17:32,000 --> 00:17:43,000
These are usual some ARM boards installed
by serial console or SSH network console

129
00:17:43,000 --> 00:17:56,000
and some big machines
like SPARC64 or IBM s390/s390x.

130
00:17:56,000 --> 00:18:01,000
Even some PC can benefit from it.

131
00:18:01,000 --> 00:18:04,000
Because some PC are headless.

132
00:18:04,000 --> 00:18:13,000
There is no HDMI or VGA ports
on that kinds of PC-server-like PC.

133
00:18:17,000 --> 00:18:26,000
I would like to thank various people through early request-for-comment stage on my proposal

134
00:18:26,000 --> 00:18:35,000
and the help during
my udeb package upload stage.

135
00:18:35,000 --> 00:18:37,000
Thank you very much.

136
00:18:37,000 --> 00:18:42,000
[Applause]

137
00:18:42,000 --> 00:19:02,000
Are there any questions?

138
00:19:02,000 --> 00:19:04,000
[chair]: OK, do it.

139
00:19:04,000 --> 00:19:13,000
[questioner 00]: First of all, I use a SPARC embedded systems all the time.

140
00:19:13,000 --> 00:19:15,000
[questioner 00]: So, absolutely this will be a great help.

141
00:19:15,000 --> 00:19:19,000
[questioner 00]: Thank you very much for all your work.

142
00:19:19,000 --> 00:19:21,000
[questioner 00]: I really appreciate this.

143
00:19:25,000 --> 00:19:32,000
[question 00-00]: What was the one
thing that you found hardest with doing this?

144
00:19:32,000 --> 00:19:39,000
[question 00-01]: What did make you surprised the most "How easy it was"?

145
00:19:39,000 --> 00:19:44,000
Actually, most difficult part
is uploading the package.

146
00:19:44,000 --> 00:19:50,000
Because one package is maintained by DM.

147
00:19:50,000 --> 00:19:58,000
Usually DM can upload
without DD sponsorship.

148
00:19:58,000 --> 00:20:01,000
But, since udeb is regarded as new package,

149
00:20:01,000 --> 00:20:06,000
we need a DD to sponsor it.

150
00:20:06,000 --> 00:20:19,000
I think it takes almost two month through the process.

151
00:20:21,000 --> 00:20:24,000
[questioner 00]: And one thing you found very easy to do,

152
00:20:24,000 --> 00:20:26,000
[questioner 00]: which you thought got to be hard?

153
00:20:26,000 --> 00:20:29,000
mmm...

154
00:20:29,000 --> 00:20:30,000
[questioner 00]: Nothing is not all hard?

155
00:20:30,000 --> 00:20:31,000
[laughs]

156
00:20:31,000 --> 00:20:34,000
From technical side, it was not hard.

157
00:20:34,000 --> 00:20:37,000
Because everything is there.

158
00:20:37,000 --> 00:20:42,000
I just write some simple patch
to add udeb support

159
00:20:42,000 --> 00:20:48,000
and some script to start
GNU/Screen in Debian Installer.

160
00:20:48,000 --> 00:20:52,000
So, from technical side, it is not so difficult.

161
00:20:52,000 --> 00:20:54,000
[questioner 00]: Awesome.

162
00:20:54,000 --> 00:20:55,000
Thank you.

163
00:20:55,000 --> 00:21:00,000
[questioner 00]: Next question?

164
00:21:01,000 --> 00:21:07,000
[questioner 01]: You said that it was already in Stretch Alpha 7...

165
00:21:07,000 --> 00:21:13,000
No, I pushed my commit after Stretch Alpha 7.

166
00:21:13,000 --> 00:21:15,000
[questioner 01]: OK, So is it gonna be after the freeze?

167
00:21:15,000 --> 00:21:16,000
Yes.

168
00:21:16,000 --> 00:21:17,000
[questioner 01]: OK.

169
00:21:17,000 --> 00:21:25,000
[question 01-00]: Do you know if adding all these components related to GNU/Screen, how much the size of image will be increased in?

170
00:21:25,000 --> 00:21:29,000
[questioner 01]: A lot or not much? Does it make any difference?

171
00:21:29,000 --> 00:21:41,000
Yes, Debian Installer image size will be little bit larger than before.

172
00:21:41,000 --> 00:21:49,000
For example, there are "QNAP" series which is armel architecture machine.

173
00:21:49,000 --> 00:21:57,000
"QNAP" series are supported by Debian Installer.

174
00:21:57,000 --> 00:22:00,000
But, "QNAP" series has size limitation.

175
00:22:00,000 --> 00:22:07,000
Because we put the kernel and initrd image
in flash memory

176
00:22:07,000 --> 00:22:16,000
and the flash memory size is about 2 MB for kernel and 4 MB for initrd.

177
00:22:18,390 --> 00:22:21,100
So, the size is limited.

178
00:22:21,100 --> 00:22:29,000
Whereas, the size of GNU/Screen udeb
package is about 500 KB.

179
00:22:29,000 --> 00:22:38,000
So, it might be impossible incorporate with this new feature into that specific images.

180
00:22:38,000 --> 00:22:43,000
For other images, I think it is OK.

181
00:22:43,000 --> 00:22:48,000
There is no big size limitation concerned.

182
00:22:57,000 --> 00:23:03,000
Currently, it is in Debian Installer daily build image.

183
00:23:03,000 --> 00:23:08,000
So, you can just try it in daily build image.

184
00:23:08,000 --> 00:23:22,000
And, URL is "https://d-i.debian.org/daily-images/".

185
00:23:22,000 --> 00:23:25,000
There are many images in there.

186
00:23:25,000 --> 00:23:41,000
Because there are images for all supported architectures such as amd64 and GNU Hurd etc.

187
00:23:42,000 --> 00:23:49,000
So, you need to choose the right architecture and image.

188
00:23:54,000 --> 00:23:56,000
This is my talk. Thank you very much.

189
00:23:56,000 --> 00:24:00,000
[Applause]
