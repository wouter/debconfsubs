1
00:00:05,351 --> 00:00:06,936
I'm Solveig.

2
00:00:07,961 --> 00:00:10,115
Here you have my contact info.

3
00:00:12,555 --> 00:00:18,888
I use Free Software and especially Debian
since quite some time now

4
00:00:19,130 --> 00:00:22,095
and I also contribute to Tails

5
00:00:22,502 --> 00:00:28,640
so my interests are in privacy…

6
00:00:44,255 --> 00:00:47,336
No? Yes? Do you hear me?

7
00:00:53,360 --> 00:00:56,690
I do some non-developer things

8
00:00:56,690 --> 00:01:03,401
and in Debian I found a way to contribute
without coding

9
00:01:03,881 --> 00:01:07,147
or maintaining packages which is to
triage bugs.

10
00:01:11,703 --> 00:01:14,582
Bug triaging, it helps,

11
00:01:15,484 --> 00:01:21,999
it's kind of non visible but it helps
Debian as a whole

12
00:01:22,195 --> 00:01:26,907
because maintainers don't always
have the time to deal

13
00:01:26,907 --> 00:01:28,371
with all their bug reports,

14
00:01:28,614 --> 00:01:31,499
some packages have a lot of
bug reports,

15
00:01:31,824 --> 00:01:35,968
like the kernel or Xorg.

16
00:01:39,700 --> 00:01:44,822
Also, it's a good way to improve the
package quality.

17
00:01:45,147 --> 00:01:50,306
When some packages have a lot of
bugs open against them,

18
00:01:51,974 --> 00:01:57,051
it can make it harder for the maintainers
to know which ones are

19
00:01:57,051 --> 00:02:03,136
solvable, actionable, and they can get a bit
over their head.

20
00:02:04,483 --> 00:02:10,541
So when you triage bugs, you help
everybody have a better experience

21
00:02:10,541 --> 00:02:11,839
with Debian.

22
00:02:13,584 --> 00:02:15,694
So, you want to do it.

23
00:02:17,406 --> 00:02:18,856
First, it's easy.

24
00:02:19,028 --> 00:02:22,966
You don't need to learn any new tool
supposing you already know

25
00:02:22,966 --> 00:02:25,320
how to read and write e-mail.

26
00:02:26,552 --> 00:02:32,029
So that's a low threshold to start.

27
00:02:33,288 --> 00:02:38,290
It's very rewarding, the maintainers are
happy when you help them,

28
00:02:38,290 --> 00:02:41,450
even if you don't touch their packages,

29
00:02:41,450 --> 00:02:49,433
if you sort their bugs, they'll be happy
and the users who submitted them

30
00:02:49,433 --> 00:02:51,382
will be happy that somebody looked
at them

31
00:02:51,993 --> 00:02:55,613
so it can be very joyful.

32
00:02:57,526 --> 00:03:03,451
Also, you search random bugs for packages
you don't necessarily know,

33
00:03:04,181 --> 00:03:07,793
so you learn about a lot of software
in Debian and

34
00:03:07,793 --> 00:03:12,069
some of them are really really surprising
and you…

35
00:03:12,759 --> 00:03:16,457
"Wha? What does this do?" and that's kind
of fun.

36
00:03:17,836 --> 00:03:19,868
And of course, it saves kittens.

37
00:03:25,747 --> 00:03:29,244
On this page, there's a…

38
00:03:30,710 --> 00:03:36,154
The bug triage page is a howto page
I made some years ago, with tips

39
00:03:36,968 --> 00:03:43,148
and this part, especially, has a list
of teams that added themselves

40
00:03:43,148 --> 00:03:47,170
so that they want you to help
sort their bugs.

41
00:03:48,873 --> 00:03:51,956
Those are the teams I worked with,
they're really really nice,

42
00:03:52,884 --> 00:03:54,270
they don't bite.

43
00:03:56,546 --> 00:03:59,031
They will let you know if you did an error,

44
00:03:59,031 --> 00:04:02,603
they will answer your questions,
you can work together.

45
00:04:04,831 --> 00:04:07,763
I don't recommend closing random bugs.

46
00:04:08,164 --> 00:04:12,840
If you go and touch packages from people
you have not warned

47
00:04:12,840 --> 00:04:17,077
or who are not willing to have somebody
touch their bugs,

48
00:04:17,077 --> 00:04:20,445
you might have backfire.

49
00:04:22,517 --> 00:04:28,290
To start, I think it's good to go packages
that you know people are happy

50
00:04:28,290 --> 00:04:29,419
if you help with.

51
00:04:35,230 --> 00:04:38,479
The first tool to triage bugs is UDD.

52
00:04:39,660 --> 00:04:47,294
I don't know if you've ever tried it,
the interface is really great.

53
00:04:55,801 --> 00:04:57,777
Here, I can show you.

54
00:05:02,151 --> 00:05:04,112
Here, that's UDD.

55
00:05:06,546 --> 00:05:08,504
So it's a bit arid like this, but

56
00:05:13,561 --> 00:05:21,687
it allows you to select many many
types of packages,

57
00:05:23,107 --> 00:05:25,667
we can see that later.

58
00:05:26,114 --> 00:05:31,595
Then you can choose a team or
other criteria

59
00:05:32,694 --> 00:05:38,021
and when you're happy about
your criteria, you search.

60
00:05:39,403 --> 00:05:45,049
It will give you a list of packages
corresponding to your criteria

61
00:05:46,556 --> 00:05:51,306
and you can select some more info
you want listed here.

62
00:05:53,718 --> 00:05:55,628
So, that's UDD search.

63
00:06:02,455 --> 00:06:08,505
I usually ignore the bug reports that
somebody has searched in the last year.

64
00:06:11,874 --> 00:06:14,147
Probably somebody else will look at them,

65
00:06:14,147 --> 00:06:18,005
let's look at those that are lost
in the limbos.

66
00:06:19,225 --> 00:06:23,694
I select wontfix, moreinfo, upstream or
unreproducible.

67
00:06:23,978 --> 00:06:27,593
Those are those that probably you can do
something on.

68
00:06:29,708 --> 00:06:33,774
And then you chose a team, preferably
one of those that is listed

69
00:06:33,774 --> 00:06:35,890
in the page we saw before.

70
00:06:45,329 --> 00:06:50,247
Once you'll have selected a bug and
something to do on it,

71
00:06:50,247 --> 00:06:52,523
you'll have to document what you do.

72
00:06:57,072 --> 00:07:00,399
Because you can change many many stuff
on the bug,

73
00:07:00,399 --> 00:07:07,108
you send the commands to
control@bugs.debian.org

74
00:07:07,352 --> 00:07:12,433
but it's always nice to put a small
a small sentence, or 2 or 3

75
00:07:12,433 --> 00:07:17,547
to say what made you conclude that is
the right change.

76
00:07:21,935 --> 00:07:26,161
Also make sure the e-mail where you do
the commands is sent

77
00:07:26,161 --> 00:07:31,811
to everybody interested, because
by default it only sends it

78
00:07:31,811 --> 00:07:35,995
to the maintainer and the submitter
in some cases.

79
00:07:36,889 --> 00:07:39,897
So if other people answered the bug
report saying

80
00:07:39,897 --> 00:07:47,612
"Hey, I have the bug too" or if upstream
came by to explain something,

81
00:07:47,612 --> 00:07:52,811
it's good to see all of those who
interacted on the bug report and

82
00:07:52,811 --> 00:07:55,448
put them all in copy.

83
00:08:01,225 --> 00:08:08,050
Ideally, people can receive the e-mail,
read what you're saying and

84
00:08:08,050 --> 00:08:11,788
don't have to go back to the bug page
to read it again.

85
00:08:12,716 --> 00:08:19,958
So that you should sum up the thread
if it was long and have them know everything.

86
00:08:28,821 --> 00:08:35,239
If you do massive triage, you should have
a few generic messages

87
00:08:35,239 --> 00:08:40,605
so you keep the messages and just
replace the words as needed.

88
00:08:41,454 --> 00:08:43,531
It saves you a lot of time.

89
00:08:44,827 --> 00:08:50,519
Also, it allows you to put a lot of
nice things in your generic e-mail

90
00:08:50,519 --> 00:08:53,724
that people are always happy to read
without more effort.

91
00:08:54,608 --> 00:08:58,237
You know, add a little "Thanks for
submitting the bug" or

92
00:08:58,237 --> 00:09:04,339
"That was a very interesting discussion"
or something like that.

93
00:09:06,084 --> 00:09:10,153
Let's keep the positive energy flowing.

94
00:09:13,001 --> 00:09:17,838
There are many ways to triage.

95
00:09:18,366 --> 00:09:21,417
One of them is trying to reproduce
bug reports.

96
00:09:22,065 --> 00:09:26,169
In the UDD we saw earlier, if you select
'unreproducible'

97
00:09:26,453 --> 00:09:30,113
Oh no… those that don't have the tag
'confirmed',

98
00:09:30,878 --> 00:09:36,526
these are bugs that one person submitted
but nobody knows if they're really

99
00:09:36,526 --> 00:09:42,217
still up to date or if it's just, somebody
submitted it but…

100
00:09:43,755 --> 00:09:48,158
If it's confirmed, there's more chance
that the maintainer will look at them.

101
00:09:50,344 --> 00:09:54,092
If they're really old, maybe they have been
corrected and nobody bothered

102
00:09:54,092 --> 00:09:55,434
to close the bug.

103
00:09:57,181 --> 00:10:03,233
If they're new, maybe you should have
them too, so see if it's the case.

104
00:10:04,252 --> 00:10:06,973
If it's the case, you write to this adress

105
00:10:06,973 --> 00:10:15,755
the 'nnn' is the number of the bug and
you add the tag 'confirmed'

106
00:10:17,455 --> 00:10:22,550
That's how we interact with control@b.d.o

107
00:10:23,883 --> 00:10:29,825
All the bug tracking is on a e-mail
interface

108
00:10:31,247 --> 00:10:33,764
'found bugnumber versionnumber'

109
00:10:36,164 --> 00:10:40,102
that's a command that control will
recognize,

110
00:10:40,386 --> 00:10:43,226
you give the bug number and what version
you're running.

111
00:10:44,361 --> 00:10:47,696
You add the tag 'confirmed'.

112
00:10:48,267 --> 00:10:50,666
Since you found it, you're 2, so it's
confirmed.

113
00:10:51,073 --> 00:10:56,391
And 'thanks', you always have to end
your e-mails to control with 'thanks'

114
00:10:56,391 --> 00:10:59,810
or 'thank you' or whatever variation
of it you want.

115
00:11:00,782 --> 00:11:06,233
The control is a very very polite beast
and likes you to be the same.

116
00:11:07,861 --> 00:11:11,928
If you don't put politeness, it won't work.

117
00:11:13,383 --> 00:11:16,803
Actually it's to tell them that the commands
are done, but

118
00:11:16,803 --> 00:11:19,969
let's be polite also with machines.

119
00:11:26,598 --> 00:11:35,176
If the bug was not confirmed, you tried
to reproduce it and you couldn't.

120
00:11:37,450 --> 00:11:44,280
You could add the tag 'unreproducible' or
'moreinfo'

121
00:11:44,598 --> 00:11:49,891
So, depending if you're quite sure that…
if you're not the first saying

122
00:11:49,891 --> 00:11:54,743
"I can't reproduce it" or if you're sure
you have exactly the same setup as

123
00:11:54,743 --> 00:11:56,091
the original submitter,

124
00:11:56,091 --> 00:11:58,556
then you should put 'unreproducible'.

125
00:11:59,624 --> 00:12:05,230
If it might be reproducible for other
people, but just not you,

126
00:12:05,230 --> 00:12:10,754
then you should ask 'moreinfo' so that
the original submitter gives

127
00:12:10,754 --> 00:12:12,989
more details on how to reproduce their bug

128
00:12:15,111 --> 00:12:19,339
And it also requires you to be polite
at the end of the command.

129
00:12:22,634 --> 00:12:25,597
An other very useful thing is to forward
them upstream.

130
00:12:26,369 --> 00:12:30,922
Some upstream follow the Debian bug tracker

131
00:12:30,922 --> 00:12:32,971
but a lot of them don't.

132
00:12:35,762 --> 00:12:39,099
Maybe somebody reported the issue in
the Debian bug tracker but

133
00:12:39,099 --> 00:12:40,764
upstream is not aware of it

134
00:12:41,410 --> 00:12:47,311
and most Debian maintainers are not
gonna solve the bug themselves,

135
00:12:47,595 --> 00:12:50,847
they're more probably gonna wait for it
to be corrected upstream,

136
00:12:50,847 --> 00:12:54,513
so we need the bug to go back to where
it will be corrected

137
00:13:00,853 --> 00:13:07,448
In a lot of cases, it can be because
upstream considers it not a bug,

138
00:13:07,855 --> 00:13:11,135
so won't fix it, so let's say it on the
Debian bug too

139
00:13:14,516 --> 00:13:18,856
or maybe upstream is not aware of
the bug so…

140
00:13:22,935 --> 00:13:27,318
Ok, that's very tiny…

141
00:13:30,486 --> 00:13:32,109
At least you have all.

142
00:13:33,491 --> 00:13:38,534
Here you have the command to add
the upstream bug tracker number.

143
00:13:38,901 --> 00:13:43,742
"forwarded bugnumber", you put the URL
in the upstream's bug tracker

144
00:13:45,327 --> 00:13:47,763
and then you say thanks again.

145
00:13:59,939 --> 00:14:06,579
So that's what I was saying before, you
can also report it upstream

146
00:14:06,783 --> 00:14:09,019
if it hasn't been already.

147
00:14:14,506 --> 00:14:17,760
Sometimes, the upstream bug tracker
is more up to date,

148
00:14:18,452 --> 00:14:23,327
so in upstream it's fixed, so it's good
to let know to the Debian bug tracker

149
00:14:24,142 --> 00:14:27,106
and add the tag 'fixedupstream'

150
00:14:28,164 --> 00:14:35,521
and it's good to say in which version
so that the maintainer may be

151
00:14:35,521 --> 00:14:39,053
motivated to update to the new version.

152
00:14:52,641 --> 00:14:58,090
In lot of cases, the bug reports are tagged
'moreinfo', which is

153
00:14:58,090 --> 00:15:04,884
somebody said "It doesn't work", which,
sorry for you, but there's no chance

154
00:15:04,884 --> 00:15:07,201
it's gonna be fixed with that.

155
00:15:08,096 --> 00:15:12,727
So in lots of cases, the bug is tagged
'moreinfo' to say

156
00:15:12,727 --> 00:15:16,670
"This bug does not give enough info
to be solved"

157
00:15:19,796 --> 00:15:23,624
Or sometimes, the maintainer
packages a new version

158
00:15:23,624 --> 00:15:26,842
and you think probably the bug is
solved,

159
00:15:27,573 --> 00:15:31,601
and you also need to ask the original
submitter if they still have the bug

160
00:15:33,347 --> 00:15:38,833
Or somebody said "Oh I'm gonna do some
test next weekend" and it's 2 years later

161
00:15:39,819 --> 00:15:44,006
and you're not sure they actually did
the test they were saying they would do.

162
00:15:45,792 --> 00:15:52,042
So, info were asked and it feels like
the bug is hanging.

163
00:15:55,620 --> 00:16:03,945
In those cases, it's helpful, sometimes,
to send an e-mail to the person who said

164
00:16:03,945 --> 00:16:09,586
"I'm gonna do something" or who needs to
answer if they still have the bug

165
00:16:11,091 --> 00:16:15,683
and saying "Hey! that's a gentle ping"

166
00:16:16,617 --> 00:16:19,544
"You said you would test" or "Can you still
reproduce a bug?"

167
00:16:20,842 --> 00:16:24,866
so that you can update the status of the
bug on the bug tracker.

168
00:16:34,129 --> 00:16:41,375
It's good to wait, like, a good amount of
time before bothering people

169
00:16:41,731 --> 00:16:43,163
about this kind of thing.

170
00:16:43,852 --> 00:16:48,400
I usually wait one year, like I told you,
probably shorter might be good,

171
00:16:48,684 --> 00:16:53,973
but it's good also not to harass people,
they have a life.

172
00:16:58,237 --> 00:17:03,969
Sometimes, the bugs have been tagged
'moreinfo' or 'wontfix' for a long time

173
00:17:07,673 --> 00:17:17,416
The info is not given, or it's unlikely
that somebody else wants

174
00:17:17,416 --> 00:17:19,938
this 'non-bug' fixed.

175
00:17:23,267 --> 00:17:26,843
Different teams have different policies
but most of them will be happy

176
00:17:26,843 --> 00:17:30,826
if you close the bugs that nobody is gonna
do anything about.

177
00:17:33,223 --> 00:17:38,669
If the bug was tagged 'moreinfo' more than
a year ago and

178
00:17:38,669 --> 00:17:43,686
nobody answered to give more info, or if
a major release came out and

179
00:17:43,686 --> 00:17:48,889
probably the bug is fixed but the original
submitter doesn't answer

180
00:17:48,889 --> 00:17:54,743
then it's good to close them, in most
cases, depending on the team.

181
00:17:56,603 --> 00:17:58,609
But it's good to ping them before you
close

182
00:17:58,770 --> 00:18:03,967
give them a reasonable amount of time
to try to test it again.

183
00:18:12,614 --> 00:18:14,523
Ok, we don't have the bottom of the page.

184
00:18:16,345 --> 00:18:18,431
The command to…

185
00:18:25,547 --> 00:18:36,115
The command to close a bug is to write to
<numberofbug>-done@control.b.d.o

186
00:18:38,788 --> 00:18:41,017
Maybe I shouldn't have done that.

187
00:18:47,085 --> 00:18:54,935
And closing the bugs is kind of one of
the most satisfying things to do.

188
00:18:59,120 --> 00:19:04,729
Sometimes, I speak with my maintainer
friends and I say

189
00:19:04,729 --> 00:19:08,393
"Hey, I closed 25 bugs today" and they're
kind of jealous because

190
00:19:08,466 --> 00:19:11,798
when you have to actually work on the bugs
to close them,

191
00:19:11,798 --> 00:19:14,628
you can rarely fix 25 in one day.

192
00:19:15,572 --> 00:19:18,873
So it's kind of the perks of doing
bug triaging.

193
00:19:19,523 --> 00:19:26,558
You know, "less bugs on the bug tracker,
I'm very efficient today."

194
00:19:28,229 --> 00:19:32,416
But don't close random stuff, but when
you find useless stuff to close,

195
00:19:32,416 --> 00:19:33,962
it feels good.

196
00:19:44,084 --> 00:19:44,935
Where am I?

197
00:19:57,127 --> 00:20:00,340
We missed the last sentence earlier.

198
00:20:01,318 --> 00:20:06,477
When trying to reproduce a bug, you
should pay particularly attention to

199
00:20:06,477 --> 00:20:07,939
the games team.

200
00:20:08,297 --> 00:20:14,440
You know, like, people open bug reports
against the game team and then

201
00:20:14,440 --> 00:20:19,599
Oh no, you have to install a bunch of
games to try to reproduce bugs,

202
00:20:20,166 --> 00:20:26,058
you know, but for work, so you install
a lot of games and

203
00:20:26,058 --> 00:20:32,475
you try to see if they're buggy, so that's
also another perk of triaging bugs,

204
00:20:32,719 --> 00:20:36,784
you get to try all the latest games.

205
00:20:40,476 --> 00:20:44,866
An other thing is when people open a bug
and didn't check if there was

206
00:20:44,866 --> 00:20:46,047
already one open.

207
00:20:46,893 --> 00:20:51,824
It ends up being 2 reports for the same bug.

208
00:20:52,765 --> 00:20:59,635
It's good to merge them so that's clearer.

209
00:21:01,138 --> 00:21:04,915
The 2 bug reports must be on the same
package, with the same severity

210
00:21:04,915 --> 00:21:06,417
and the same state,

211
00:21:07,106 --> 00:21:11,126
otherwise you can't merge, so you have
to send first the commands to do that,

212
00:21:11,411 --> 00:21:17,388
like I showed before, and at the end you
tell the bug tracker to merge.

213
00:21:23,680 --> 00:21:25,792
So, that we've seen.

214
00:21:39,649 --> 00:21:41,068
You can…

215
00:21:51,844 --> 00:21:55,508
Ok, you can't really see the…

216
00:21:57,413 --> 00:22:05,064
So, I was giving an example of my standard
message I paste when I close a bug

217
00:22:06,004 --> 00:22:09,300
and we don't see the end, but I'm gonna
do it from memory, it says

218
00:22:09,300 --> 00:22:14,380
"Hi! I'm closing this bug, since it was
tagged 'moreinfo' for years

219
00:22:14,380 --> 00:22:20,397
without answer. If you still experience
the issue, please feel free to reopen it

220
00:22:20,397 --> 00:22:24,003
or ask me to do it" because some people
don't know how to reopen a bug

221
00:22:24,213 --> 00:22:25,839
that has been archived.

222
00:22:28,600 --> 00:22:33,148
So that's a very standard message,
no nothing, it's not very long.

223
00:22:34,092 --> 00:22:42,630
That is good to have a model so that
you can just paste, with niceness in it.

224
00:22:45,344 --> 00:22:47,305
If you're not sure about a bug report,

225
00:22:47,305 --> 00:22:51,125
you read through it and you're still
not sure what to do

226
00:22:51,360 --> 00:22:54,864
because, let's be clear, I don't always
understand what the issue is.

227
00:22:55,392 --> 00:22:59,005
What you need to understand to triage
is what the status of the bug report is.

228
00:23:00,508 --> 00:23:02,092
I triaged bugs for the kernel.

229
00:23:03,637 --> 00:23:07,621
I don't understand anything about
the kernel, like most human beings

230
00:23:07,621 --> 00:23:13,424
but, without understanding the bug report,
you can understand if somebody asks

231
00:23:13,424 --> 00:23:15,219
for info and nobody provided it

232
00:23:15,708 --> 00:23:22,316
or, for example, for the kernel, all the
bug reports that were for the nouveau driver

233
00:23:22,316 --> 00:23:23,668
that is not supported anymore,

234
00:23:24,074 --> 00:23:28,668
it was possible to close them because
nobody cared anymore.

235
00:23:30,092 --> 00:23:33,223
So you don't necessarily need to understand
what the bug is about

236
00:23:33,223 --> 00:23:35,339
to do something about the bug report

237
00:23:37,086 --> 00:23:41,303
but sometimes it's a bit more complicated,
you're not sure

238
00:23:41,303 --> 00:23:44,746
just close the bug report, move to the
next one

239
00:23:45,202 --> 00:23:50,439
There's probably an other one that's,
you know, low hanging

240
00:23:50,439 --> 00:23:52,266
take the easy stuff.

241
00:23:54,136 --> 00:23:59,709
Because if you do something wrong, or
if you bother the maintainer to ask

242
00:23:59,709 --> 00:24:03,371
what should be done, then you're not
really helping,

243
00:24:03,696 --> 00:24:08,326
you might be taking time from them that
would be best invested somewhere else.

244
00:24:12,805 --> 00:24:16,681
Small warning, there are some people who
really don't like you touching

245
00:24:16,926 --> 00:24:18,186
their bug reports.

246
00:24:19,930 --> 00:24:23,786
I wish you not to encounter them.

247
00:24:24,275 --> 00:24:33,094
I you do, just either ignore them or ask
the maintainers of the package you're triaging

248
00:24:33,379 --> 00:24:38,539
to step in and help or you can also
contact the anti-harassment team.

249
00:24:40,124 --> 00:24:46,508
But it's very rare and most of Debian
people are very nice people

250
00:24:47,726 --> 00:24:51,549
and they'll be happy to cooperate and
discuss with you

251
00:24:51,994 --> 00:24:57,854
and bug triaging is fun and rewarding
and easy.

252
00:25:01,061 --> 00:25:05,704
Those are the links to the things that
are useful to triage.

253
00:25:06,151 --> 00:25:07,896
The first one is…

254
00:25:18,951 --> 00:25:22,000
Ok, the first one is bad, I'll correct it.

255
00:25:24,843 --> 00:25:30,450
The second one is how to triage with
all the different commands

256
00:25:30,450 --> 00:25:32,358
that are useful.

257
00:25:32,967 --> 00:25:36,908
The third one is server control,

258
00:25:40,801 --> 00:25:44,502
a reminder of all the different instructions
you can send to server control

259
00:25:44,706 --> 00:25:48,044
which is the way to interact with
a bug report.

260
00:25:48,644 --> 00:25:55,626
The last one is about only closing and
you don't interact with control,

261
00:25:55,960 --> 00:26:01,694
you write to bugnumber-done, so that's
a different e-mail destination.

262
00:26:07,190 --> 00:26:14,059
So, the idea was to have a workshop,
so this was the explanation part and now

263
00:26:14,059 --> 00:26:18,939
let's close some bugs, or sort them,
maybe.
