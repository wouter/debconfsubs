1
00:00:10,020 --> 00:00:14,530
First let me backtrack a little bit and start with a bit of context

2
00:00:15,797 --> 00:00:22,576
In the linux kernel, device mapper is provided as a way to create virtual block devices

3
00:00:22,676 --> 00:00:29,767
those block devices usually have the names /dev/dm-0, /dev/dm-1 and so on

4
00:00:29,867 --> 00:00:37,430
and udev creates symlinks /dev/mapper/$TARGET where $TARGET is the dm target name

5
00:00:37,718 --> 00:00:46,007
They're used for a whole bunch of purposes amongst which transport encryption of block devices using dmcrypt module

6
00:00:46,481 --> 00:00:56,006
as well as integrity checking both for read-only targets for the whole device as well as per-sector integrity information

7
00:00:56,106 --> 00:01:00,654
They're also used for the Logical Volume Manager (LVM)

8
00:01:00,754 --> 00:01:10,102
and a whole bunch of other things, you can check dmsetup or this directory in the linux kernel source tree

9
00:01:10,941 --> 00:01:14,827
Here is an example of such virtual block device tags

10
00:01:14,927 --> 00:01:30,714
we have 2 disks /dev/sda and /dev/sdb on which /dev/mapper creates pv0 and pv1

11
00:01:30,814 --> 00:01:47,783
which are encrypted devices used together as a physical volume myvg with 4 logical volumes, lv1 to lv4

12
00:01:47,883 --> 00:01:55,936
lv1 is mounted on root (/), lv2 on /var, lv3 on /home, lv4 is swap

13
00:01:59,365 --> 00:02:10,147
One thing that people sometimes forget is that encryption for crypt target is not done by cryptsetup itself but by the kernel crypto API

14
00:02:10,757 --> 00:02:26,377
the source device (sda or sdb) can be either random noise, in which case we have plain dmcrypt device or either have a header with metadata

15
00:02:26,720 --> 00:02:35,750
metadata consists of type of metadata, encryption cypher, the key derivation function (KDF), the UUID...

16
00:02:35,850 --> 00:02:38,074
and that's used for key management.

17
00:02:38,174 --> 00:02:47,141
 That's what's cryptsetup does, it reads those headers and performs key management, so the most common header is LUKS...

18
00:02:47,637 --> 00:02:49,618
at least for Linux

19
00:02:49,923 --> 00:02:57,238
these days cryptsetup agreed to perform key management and uses libdevmapper to create the mapped devices

20
00:02:57,338 --> 00:03:00,972
and then delegates all the crypto to the kernel

21
00:03:05,734 --> 00:03:12,897
For a number of type volumes, that is volumes other than swap and /tmp

22
00:03:12,997 --> 00:03:18,497
it's usually a good idea to use LUKS rather than plain dmcrypt device

23
00:03:18,597 --> 00:03:25,927
because that means there is more infrastructure to prevent yourself from shooting yourself in the foot

24
00:03:31,261 --> 00:03:36,442
cryptsetup in Debian is shipped by various packages

25
00:03:36,975 --> 00:03:38,576
I just listed a few here

26
00:03:38,676 --> 00:03:47,186
so we have cryptsetup-bin and libcryptsetup for the upstream bits, respectively for the upstream binaries and the shared libraries

27
00:03:47,286 --> 00:03:51,987
and for those two we also ship udebs for the integration into debian instaler

28
00:03:52,087 --> 00:03:57,892
we also have gazillons of scripts

29
00:03:57,992 --> 00:04:00,407
that are spread across those two packages.

30
00:04:00,507 --> 00:04:12,713
these scripts are used to parse crypttab and turn the cryptab entries into cryptsetup Open commands

31
00:04:12,813 --> 00:04:15,951
with suitable arguments

32
00:04:17,018 --> 00:04:26,619
we also ship scripts to use the output of a program as passphrase

33
00:04:27,991 --> 00:04:34,887
as well as initramfs as boot scripts and hook scripts for unlocking at early boot stage

34
00:04:35,344 --> 00:04:44,030
To this day, our scripts are spread amongst those two packages, since about a month or two

35
00:04:47,307 --> 00:04:53,289
cryptsetup is a transitionnal dummy package that depends on these two

36
00:04:53,631 --> 00:05:03,385
it will remain so for another release cycle to ensure a small upgrade path

37
00:05:05,023 --> 00:05:14,662
One thing that is quite important to mention is that we are upstreams for these bits

38
00:05:14,762 --> 00:05:19,272
so our work is not only about packaging

39
00:05:19,372 --> 00:05:24,682
we also need to do the Q&A, the maintenance, and development of the scripts we are shipping

40
00:05:26,778 --> 00:05:30,207
some debian derivatives are dependable

41
00:05:35,684 --> 00:05:43,199
Since cryptsetup, especially when unlocking at early boot stage is fairly low level, when things break they do quite badly.

42
00:05:43,299 --> 00:05:50,590
that makes people quite angry at us, so we need to be quite careful with the Q&A here.

43
00:05:54,017 --> 00:06:00,042
Here is a timeline of the project with a few selected highlights

44
00:06:00,142 --> 00:06:09,223
In 2004, the linux kernel 2.6.4 was released, being the first to contain the dmcrypt module

45
00:06:11,327 --> 00:06:14,961
Shortly afterwards, dmcrypt landed into Debian

46
00:06:15,089 --> 00:06:18,595
about one year later, upstream released 1.0

47
00:06:18,695 --> 00:06:28,414
just in time for etch, we had the first partman-crypto release

48
00:06:28,514 --> 00:06:33,292
it was possible to setup encrypted volumes in the installer

49
00:06:33,392 --> 00:06:39,699
then skipping 7 years of lot of development

50
00:06:39,799 --> 00:06:43,557
In 2013, upstream released 1.6

51
00:06:44,099 --> 00:06:52,897
LUKS defaut cipher mode switches from CBC to XTS which I think most of you are using today.

52
00:06:52,997 --> 00:06:58,986
Skipping another five years, upstream released 2.0 earlier this year 

53
00:06:59,086 --> 00:07:07,721
with a new LUKS2 on-disk format which I may talk about if I have time, and a bunch of experimental features

54
00:07:07,821 --> 00:07:22,545
about 2 months ago, Jonas Meurer and I met for a packaging sprint in order to refactor the whole Debian parts

55
00:07:25,350 --> 00:07:34,372
we moved a lot of code, did some triaging and unfortunately it introduced some regressions

56
00:07:34,882 --> 00:07:46,008
In the future, hopefully before the freeze, upstream will release 2.1 with LUKS format defaulting to LUKS2

57
00:07:47,379 --> 00:07:57,421
Now I will focus on unlocking at early boot stage, because that's what most of our work is about.

58
00:07:57,521 --> 00:08:04,625
We also provide scripts for unlocking at later stages, mostly for manual unlocking and SysV init scripts

59
00:08:05,454 --> 00:08:15,910
For those of us who use SystemD, it comes with its own crypttab and parser logic

60
00:08:16,707 --> 00:08:24,103
so they're not using the cryptsetup binaries at all, but they are using libcryptsetup

61
00:08:24,203 --> 00:08:33,029
To give proper credits, all this is joint work with Jonas Meurer and other contributors

62
00:08:37,524 --> 00:08:43,645
Along the year we have added support for multiple use cases

63
00:08:43,745 --> 00:08:53,241
People wanted to have a custom block device stack and to be able to unlock it

64
00:08:53,432 --> 00:08:56,971
This doesn't come out of the box, so they come with their own workarounds

65
00:09:06,981 --> 00:09:10,966
But they are not always aware of our work

66
00:09:11,066 --> 00:09:18,170
I guess we are bad at advertising, so it's a good reason to talk about this today.

67
00:09:18,270 --> 00:09:25,502
First I have a pleed, that is you should never use our own initramfs script as a baseline for your own scripts

68
00:09:25,602 --> 00:09:32,261
because those are using our internal API and are subject to change without notice.

69
00:09:32,361 --> 00:09:42,048
If you want to use custom initramfs integration, you need to stick to the documented interface, or ask us to extend it

70
00:09:42,148 --> 00:09:49,507
So when we had this sprint we kind of reworked the whole initramfs integration as I said

71
00:09:49,607 --> 00:09:53,269
and that broke a lot of third-party scripts unfortunately

72
00:09:53,369 --> 00:09:57,828
I filed RC bugs against them, the ones I found at least

73
00:09:57,928 --> 00:10:05,893
but it's a bit unfortunate, it's not because it's a shell script that it's free access for copying

74
00:10:09,878 --> 00:10:15,489
One thing I want to talk about is remote unlocking using SSH

75
00:10:15,589 --> 00:10:25,531
Since Lenny, the dropbear SSH server has initramfs integration and today it is in dropbear-initramfs package

76
00:10:25,631 --> 00:10:39,940
You can install dropbear SSH into the initrd of your server, then ssh into that initrd and unlock your device there.

77
00:10:40,040 --> 00:10:44,371
What happens is the initramfs boot scripts are blocking on the password prompt

78
00:10:44,471 --> 00:10:55,369
but you can access to it remotely and ????? passphrase in some ask-pass ????? 

79
00:10:55,469 --> 00:11:03,275
then some magic happens and you bypass that password prompt that's on the local console

80
00:11:05,252 --> 00:11:12,042


