1
00:00:02,521 --> 00:00:04,081
Let's talk about GNU/Hurd

2
00:00:05,841 --> 00:00:11,101
For us it's a bit all about freedom 0,
that is, the ability to use software,

3
00:00:11,481 --> 00:00:14,161
basically, for any purpose.

4
00:00:14,881 --> 00:00:18,261
And for us, the important thing is that
you shouldn't have to ask

5
00:00:18,441 --> 00:00:20,981
the system administrator for things.

6
00:00:21,481 --> 00:00:23,581
You should be allowed to do
whatever you want.

7
00:00:24,161 --> 00:00:30,901
So for instance, why is fdisk, mke2fs,
etc. hidden in /sbin?

8
00:00:31,142 --> 00:00:36,841
I want to be able to build disk images,
play with them, mount them, etc.

9
00:00:37,700 --> 00:00:44,581
So just be able to work with the kind of
disk and network access I have,

10
00:00:44,581 --> 00:00:46,041
and do whatever I want with this.

11
00:00:47,161 --> 00:00:52,301
It's about freedom to innovate as well, if
I want to use an experimental filesystem,

12
00:00:52,301 --> 00:00:56,921
just play with, without being afraid of
crashing the machine.

13
00:00:57,841 --> 00:01:01,421
You should be able to just
run the file system

14
00:01:01,440 --> 00:01:06,241
and let the system administrator be happy
with this because it's safe to do this.

15
00:01:08,661 --> 00:01:14,181
And also, it's a way to provide freedom
from misbehaving programs

16
00:01:14,181 --> 00:01:18,481
like a driver which doesn't work so well,
some things like this.

17
00:01:20,581 --> 00:01:25,041
Just to give an idea, in GNU/Hurd,

18
00:01:25,041 --> 00:01:28,882
you have the kernel which does basically
almost nothing,

19
00:01:28,882 --> 00:01:33,741
just managing tasks, the memory and
inter-process communications,

20
00:01:33,741 --> 00:01:37,821
and then you have a lot of daemons
doing the actual stuff,

21
00:01:37,821 --> 00:01:43,861
so the pfinet is the TCP/IP stack,
and ext2fs does the filesystem thing.

22
00:01:44,381 --> 00:01:47,502
And then, you have the user,
just running programs.

23
00:01:48,581 --> 00:01:55,321
And these tools just, actually just talk
to the daemons through the microkernel,

24
00:01:55,321 --> 00:02:00,462
the microkernel doesn't do much, it just
passes requests along.

25
00:02:02,621 --> 00:02:05,720
For instance, if a server crashes,
then that's fine.

26
00:02:07,080 --> 00:02:10,342
For instance a driver crashes,
or just hangs,

27
00:02:10,342 --> 00:02:17,221
you just can kill and then pfinet
will re-open a new instance of the driver

28
00:02:17,221 --> 00:02:24,041
and it will just work, thanks to TCP just
continuing to ping the other computer.

29
00:02:25,261 --> 00:02:27,921
So it's just an error, it's not something
of the death.

30
00:02:28,922 --> 00:02:33,041
At some point on my desktop, I could
switch off the light,

31
00:02:33,041 --> 00:02:35,522
and then that would crash my laptop.

32
00:02:35,842 --> 00:02:42,940
Because switching off the light would
reboot my hard disk, USB hard disk,

33
00:02:42,940 --> 00:02:46,461
and then the kernel of the laptop
wouldn't like this.

34
00:02:47,421 --> 00:02:49,421
This is not something which is
supposed to happen.

35
00:02:49,680 --> 00:02:54,401
So, with a server approach, this is
completely fixed.

36
00:02:55,120 --> 00:03:00,321
It's also easier to debug, it's really
nice to be able to gdb a TCP/IP stack,

37
00:03:00,321 --> 00:03:07,201
when there is something happening in there,
just run gdb, you can gprof it, etc.

38
00:03:08,721 --> 00:03:10,780
You can also dare more crazy things.

39
00:03:11,102 --> 00:03:16,820
For instance, the Linux console doesn't
support much, because we don't want to put

40
00:03:16,820 --> 00:03:19,120
too much complex code in there.

41
00:03:19,902 --> 00:03:23,881
On GNU/Hurd the console actually supports
things like Chinese,

42
00:03:23,881 --> 00:03:25,661
double-width support, etc.

43
00:03:25,961 --> 00:03:27,861
which is not supported
by the Linux console,

44
00:03:27,861 --> 00:03:31,380
and that's right because you don't want
to put too crazy stuff.

45
00:03:32,160 --> 00:03:35,661
Here since it's just a userland program,
then you're fine,

46
00:03:35,661 --> 00:03:41,361
and so we do have Chinese support in,
actually, textmode in the Debian Installer.

47
00:03:44,542 --> 00:03:52,401
Just to show an example, so here I have
ftpfs which uses the TCP/IP stack

48
00:03:52,401 --> 00:03:56,101
to actually mount a remote directory,

49
00:03:56,101 --> 00:04:03,321
and then I can use isofs to mount an ISO
image which is inside that FTP server.

50
00:04:03,861 --> 00:04:09,682
And then I can just let cp copy a file
from the ISO image which is on the server.

51
00:04:10,441 --> 00:04:17,141
So this translates that way, so I've done
this command a long time ago,

52
00:04:17,141 --> 00:04:22,600
just to say that "ftp:" in my home
directory is whatever FTP,

53
00:04:22,600 --> 00:04:29,962
and then I can take a "~/ftp:/etc." URL
and give that to isofs

54
00:04:29,962 --> 00:04:33,401
and then mount that on my "mnt",

55
00:04:33,401 --> 00:04:38,381
and then I can just browse inside the ISO
image, without having to download

56
00:04:38,381 --> 00:04:44,481
the whole ISO image, without having to ask
root for this kind of things, etc.

57
00:04:45,480 --> 00:04:48,601
And I can also permanently store this
in ext2fs.

58
00:04:49,201 --> 00:04:53,561
So just to give an example, I have a
translator on my signature files,

59
00:04:53,561 --> 00:04:58,660
which just calls fortune, so when I
"cat .signature" [demo],

60
00:04:58,660 --> 00:05:02,600
I get one signature or another, because
each time I open the file,

61
00:05:02,600 --> 00:05:06,941
it's a new instance of fortune which is
started.

62
00:05:07,602 --> 00:05:12,142
You can see that, indeed, this is stored
in my signature file.

63
00:05:14,061 --> 00:05:15,280
So this is fun!

64
00:05:16,721 --> 00:05:22,741
Another example: as a user, I can start
my own TCP/IP stack,

65
00:05:22,741 --> 00:05:27,000
tell it to use a virtual network
interface,

66
00:05:27,000 --> 00:05:32,303
and then put the TCP/IP service on some
node in my home,

67
00:05:32,303 --> 00:05:37,764
and then I can run openvpn to actually
push and pull packets

68
00:05:37,764 --> 00:05:41,500
from that virtual interface, and build a
VPN with somewhere else.

69
00:05:42,121 --> 00:05:49,301
And then I can remap the system, what is
supposed to be the system TCP/IP stack

70
00:05:49,301 --> 00:05:51,141
into my own socket,

71
00:05:51,141 --> 00:05:58,441
and then I get a new shell for which the
system TCP/IP stack is actually

72
00:05:58,441 --> 00:06:00,420
my own TCP/IP stack.

73
00:06:01,021 --> 00:06:06,382
So I can decide which program actually
uses this TCP/IP stack,

74
00:06:06,382 --> 00:06:14,801
and just do my own VPN without having to
ask anything to the administrator.

75
00:06:17,261 --> 00:06:22,361
But also, for instance it happens quite
often that you have a binary,

76
00:06:22,361 --> 00:06:26,201
maybe not sh, but like, python or perl
or whatever,

77
00:06:26,201 --> 00:06:34,301
you have a program which wants /bin/sh
to be actually bash or whatever,

78
00:06:34,301 --> 00:06:38,841
so I want to change this, so I can remap
this, so for instance [demo]

79
00:06:38,841 --> 00:06:43,541
if I look at sh, so as usual,
oh it's green,

80
00:06:43,541 --> 00:06:45,541
but you can see here that it's dash,

81
00:06:45,541 --> 00:06:53,201
and if I remap /bin/sh into /bin/bash, for
instance, I get a new shell where actually,

82
00:06:53,201 --> 00:07:01,661
sh is not the same, so it's remapped into
/bin/bash,

83
00:07:01,661 --> 00:07:04,441
and so it's actually bash which actually
shows up here.

84
00:07:04,860 --> 00:07:10,881
So I do really choose how I work, what my
environment looks like.

85
00:07:11,921 --> 00:07:18,181
And for instance, I can remap the whole
/bin directory into my own directory,

86
00:07:18,181 --> 00:07:21,581
where I expose /bin, but also
other things,

87
00:07:21,581 --> 00:07:27,281
so that programs which have /bin/something
hardcoded into them,

88
00:07:27,281 --> 00:07:32,381
I can use them without having to ask the
administrator to install stuff inside /bin.

89
00:07:34,000 --> 00:07:41,040
So it's kind of interesting, a bit like
stow, Nix, Guix, but done in a nice way.

90
00:07:43,321 --> 00:07:47,861
How does it work? Well it's actually
relatively simple in the principle,

91
00:07:47,861 --> 00:07:52,021
it's simply that libc doesn't talk with
the kernel or whatever,

92
00:07:52,021 --> 00:07:57,321
it always uses RPCs, so to ask nicely
about opening files etc.,

93
00:07:57,321 --> 00:08:04,021
and so it's really natural in GNU/Hurd
that you can redirect things.

94
00:08:05,241 --> 00:08:10,840
So for instance, the remap translator here
is like, maybe,

95
00:08:10,840 --> 00:08:13,481
200-300 lines
[Note of transcriptor: 150 actually],

96
00:08:13,721 --> 00:08:15,320
because it's just a matter of

97
00:08:15,320 --> 00:08:20,161
"you open a file, OK, I look at the file
path, is it something I want to translate?

98
00:08:20,161 --> 00:08:23,481
Yes, I translate that, and then I open
the real file,

99
00:08:23,481 --> 00:08:26,641
and give the new handle to the program",

100
00:08:27,080 --> 00:08:29,401
and that's all, so it's extremely simple.

101
00:08:32,221 --> 00:08:36,940
So <i>everything</i> in GNU/Hurd is an RPC and
so it is interposable,

102
00:08:36,940 --> 00:08:44,401
and then translators get exposed in the
filesystem, we have seen the TCP/IP stack,

103
00:08:44,401 --> 00:08:46,961
it's just a path inside the filesystem.

104
00:08:47,721 --> 00:08:53,381
And then the user can decide whatever it
wants to do to interpose whatever.

105
00:08:53,781 --> 00:08:58,981
So, for instance fakeroot, in Linux,
is quite big,

106
00:08:58,981 --> 00:09:02,581
because it has to interpose libc symbols,

107
00:09:02,581 --> 00:09:07,401
and every time libc invents something new,
then it breaks in fakeroot

108
00:09:07,401 --> 00:09:14,801
because fakeroot has to know about this
new symbol, etc. and interpose them,

109
00:09:14,801 --> 00:09:18,221
either through ptrace or ld or whatever.

110
00:09:19,602 --> 00:09:22,641
In GNU/Hurd, fakeroot is, like,
a thousand lines long,

111
00:09:22,641 --> 00:09:26,161
because it just implements a few basic
things,

112
00:09:26,161 --> 00:09:34,861
and then everything just works, which just
interpose basic authentication hooks,

113
00:09:34,861 --> 00:09:37,141
and libc uses them all the time.

114
00:09:38,860 --> 00:09:43,981
So it's fully virtualizable, and with
a really fine grain interface,

115
00:09:43,981 --> 00:09:48,021
because you can precisely decide
which RPCs are interposed,

116
00:09:48,181 --> 00:09:50,560
or which files in the filesystem
are interposed.

117
00:09:51,821 --> 00:09:56,400
And then you can just use your home
directory, the TCP/IP stack,

118
00:09:56,400 --> 00:09:59,981
and pile stuff over it, the way you want.

119
00:10:02,222 --> 00:10:06,321
Just to give a crazy example, we have
a lot of stuff,

120
00:10:06,321 --> 00:10:12,201
I actually have an ISO image inside a
partitioned disk image on FTP over a VPN.

121
00:10:13,041 --> 00:10:14,821
And this is not <i>so</i> crazy.

122
00:10:15,821 --> 00:10:20,282
Maybe the ISO image inside the partitioned
disk, the ISO image is a bit too much,

123
00:10:20,282 --> 00:10:26,361
but one file inside the partitioned disk
image on FTP over VPN is not so crazy,

124
00:10:26,361 --> 00:10:31,061
because maybe you are on a hostile
network, so you have to use a VPN,

125
00:10:31,061 --> 00:10:34,761
and then you want to access a file
you know is inside a disk image,

126
00:10:34,761 --> 00:10:40,441
I don't know, a known disk image which is
provided on a public FTP server,

127
00:10:40,441 --> 00:10:44,001
and you don't want to download the whole
image just to get, I don't know,

128
00:10:44,001 --> 00:10:45,502
the README file or something like this.

129
00:10:45,860 --> 00:10:49,481
So it's not so crazy, and it just
works nicely.

130
00:10:52,141 --> 00:10:54,621
So a bit more Debian stuff.

131
00:10:55,601 --> 00:11:00,001
Porting packages to Hurd is quite easy
in principle,

132
00:11:00,001 --> 00:11:05,520
because it's just a POSIX system, there is
a lot more than just POSIX,

133
00:11:05,520 --> 00:11:07,521
but it provides a POSIX interface.

134
00:11:08,021 --> 00:11:11,761
So portable programs should be
really fine.

135
00:11:13,121 --> 00:11:17,761
Just for fun, some dumb issues, so for
instance some programs think that

136
00:11:17,761 --> 00:11:20,981
if it's not Linux or BSD, then they can
include windows.h...

137
00:11:22,061 --> 00:11:22,881
Why not...

138
00:11:23,661 --> 00:11:26,241
If the system has mach.h, that must be
MacOS,

139
00:11:26,561 --> 00:11:31,601
because MacOS is the only system in the
world that uses Mach, I don't know why...

140
00:11:32,841 --> 00:11:37,861
Some people try to grep cpuinfo, which
doesn't exist on GNU/Hurd yet,

141
00:11:37,861 --> 00:11:43,361
and so they basically just run "make -j"
which just explodes the system,

142
00:11:43,361 --> 00:11:48,561
I mean even on a Linux system it's just
the same, unless it's a small program,

143
00:11:48,561 --> 00:11:51,141
but with a lot of C++ files it's horrible.

144
00:11:52,302 --> 00:11:58,781
Some people include limits.h from linux/
instead of just the standard one, well...

145
00:12:00,741 --> 00:12:04,441
A problematic thing is people who
hardcoded errno values;

146
00:12:04,441 --> 00:12:09,680
the values of errno are not standardized,
so you shouldn't hardcode them, like,

147
00:12:09,680 --> 00:12:12,501
in testsuite results or things like this.

148
00:12:13,761 --> 00:12:17,561
And quite often in configure it's
hardcoded that

149
00:12:17,561 --> 00:12:21,856
only Linux knows -lpthread or -ldl, etc.

150
00:12:22,061 --> 00:12:25,101
so quite often programs are not
generic enough,

151
00:12:25,101 --> 00:12:30,061
and that's just easy to fix, but we have
more and more of these.

152
00:12:30,641 --> 00:12:33,341
So we have a porter page developing
a bit more about these.

153
00:12:34,581 --> 00:12:42,021
I wanted to talk a bit more about
PATH_MAX, it is not defined on GNU/Hurd,

154
00:12:42,021 --> 00:12:46,560
for very good reasons, and it is allowed
by POSIX not to define it,

155
00:12:46,560 --> 00:12:51,461
just to say that there is no limitation on
the PATH_MAX value,

156
00:12:51,941 --> 00:12:54,261
we don't have a limit on the size of
the paths.

157
00:12:54,922 --> 00:12:59,422
And indeed it has a fragile semantic, it
has never meant

158
00:12:59,422 --> 00:13:03,881
"a reasonable size for an array of
characters to store a path".

159
00:13:05,181 --> 00:13:09,281
On Linux it's 4000, that's a whole page,

160
00:13:09,281 --> 00:13:12,280
that's a whole TLB entry for
just one file name.

161
00:13:13,441 --> 00:13:16,801
It's extremely costly, most people don't
have so long paths,

162
00:13:16,801 --> 00:13:24,241
and so it's really a pity to use so much
memory, because it's always a whole page

163
00:13:24,241 --> 00:13:27,421
because it will always be aligned
on 4k etc.

164
00:13:27,941 --> 00:13:30,081
So, well, that's a waste for one.

165
00:13:30,701 --> 00:13:35,120
And paths can actually be longer,
there is no strict limitation,

166
00:13:35,120 --> 00:13:40,062
you can mkdir something, cd into that
mkdir again, cd, etc.,

167
00:13:40,062 --> 00:13:43,501
you can do that as much as you want,
there is no limitation on this,

168
00:13:43,501 --> 00:13:45,381
it's just that when you call

169
00:13:45,381 --> 00:13:50,841
"get current working directory",
you won't get it completely.

170
00:13:51,761 --> 00:13:56,661
And actually, some programs misbehave
in that case,

171
00:13:56,661 --> 00:14:01,421
because they won't see these files,
they will be quite actually hidden,

172
00:14:01,421 --> 00:14:02,921
or protected, or I don't know,

173
00:14:02,921 --> 00:14:07,341
you can not remove them just giving
the path, you have to cd, cd, cd, cd,

174
00:14:07,341 --> 00:14:08,820
and then you can access the file.

175
00:14:11,181 --> 00:14:15,242
And for no reason, actually, because Linux
inside doesn't have

176
00:14:15,242 --> 00:14:17,842
such limitation, actually.

177
00:14:19,241 --> 00:14:24,821
And also, it's stupid, but POSIX didn't
really said precisely whether

178
00:14:24,821 --> 00:14:30,060
the final \0 actually is included in
PATH_MAX or not,

179
00:14:30,060 --> 00:14:33,521
so people would allocate PATH_MAX+1,
or maybe not.

180
00:14:36,382 --> 00:14:40,660
So we have a lot of code which doesn't,
maybe, actually work,

181
00:14:40,660 --> 00:14:45,121
but nobody tests it, actually, because
they would never have so long paths.

182
00:14:45,741 --> 00:14:48,561
So I'm a bit afraid of all these codes
using PATH_MAX.

183
00:14:49,541 --> 00:14:50,881
You should be afraid as well.

184
00:14:52,620 --> 00:14:54,501
Just to give an overview of the state.

185
00:14:54,740 --> 00:15:00,661
We have a i386 support, we have a 64bit
support which has started,

186
00:15:00,661 --> 00:15:02,401
we have the kernel booting,

187
00:15:02,401 --> 00:15:06,641
and now it's mostly translating between
32 and 64 in our RPCs.

188
00:15:07,822 --> 00:15:16,441
We have drivers for network boards as a
userland translator, using the DDE layer.

189
00:15:18,042 --> 00:15:20,281
We have disk, we have a Xen port.

190
00:15:20,921 --> 00:15:26,161
We have a preliminary sound support
which was announced today, using Rump,

191
00:15:26,161 --> 00:15:27,521
the Rump kernel.

192
00:15:28,141 --> 00:15:29,601
We don't have USB yet.

193
00:15:31,121 --> 00:15:35,541
It is quite stable, I haven't reinstalled
my boxes for, like, a decade,

194
00:15:35,541 --> 00:15:37,762
I don't remember when I installed them,
actually.

195
00:15:38,500 --> 00:15:43,261
And then the buildd machines just keep
building packages for weeks

196
00:15:43,261 --> 00:15:44,561
without a problem.

197
00:15:44,681 --> 00:15:47,341
We have 81% of the archive.

198
00:15:48,241 --> 00:15:52,441
We have the native Debian Installer which
is really working great.

199
00:15:54,461 --> 00:16:00,561
Recent work is, like, interesting thing is,
a distributed mtab translator

200
00:16:00,561 --> 00:16:04,021
to provide /proc/mounts in a hurdish way.

201
00:16:05,040 --> 00:16:09,700
We have quite a few optimizations which
went in to improve the performance.

202
00:16:11,941 --> 00:16:14,541
We had releases quite some time ago,

203
00:16:14,541 --> 00:16:17,260
I really recommend to have a look at this
one, it's fun.

204
00:16:18,361 --> 00:16:21,441
We've some Wheezy and Jessie snapshots,

205
00:16:21,441 --> 00:16:25,260
they are not official, but for us it's
really an official thing.

206
00:16:29,361 --> 00:16:34,521
An important thing I wanted to discuss
this week is the removal from ftp-master.

207
00:16:35,341 --> 00:16:40,021
This is due since quite a few years now,
honestly,

208
00:16:40,021 --> 00:16:44,322
it's really not useful to mirror the hurd
packages over the whole world,

209
00:16:44,322 --> 00:16:49,121
because there are not even as many users
as the number of mirrors.

210
00:16:50,161 --> 00:16:55,681
So OK, that's fine for just the removal
from the main archive in terms of mirroring.

211
00:16:56,141 --> 00:16:58,862
But then we have a lot of consequences.

212
00:16:59,181 --> 00:17:03,381
For instance, buildd.debian.org is really
an important thing,

213
00:17:03,381 --> 00:17:08,281
because that is where the release team
schedules transitions,

214
00:17:08,281 --> 00:17:12,901
and loosing this, for us, would be really
tedious work,

215
00:17:12,901 --> 00:17:16,721
because I've been there, doing, actually,
the transition work,

216
00:17:16,721 --> 00:17:22,080
the same work as the release team, and
it's really painful to do this <i>again</i>.

217
00:17:23,801 --> 00:17:26,321
So we would really like to have a solution
for this.

218
00:17:26,881 --> 00:17:31,001
Maybe get that fed from debian-ports and,

219
00:17:31,001 --> 00:17:35,621
then that's fine, we can be on
debian-ports, as long as at least

220
00:17:35,621 --> 00:17:37,901
there is some synchronization between
something.

221
00:17:38,921 --> 00:17:43,881
And also, getting exposed on the buildd
package status page,

222
00:17:43,881 --> 00:17:48,821
so that people are aware that there is
some port which is failing,

223
00:17:48,821 --> 00:17:52,360
and maybe they are keen on spending some
time on it, maybe not,

224
00:17:52,360 --> 00:17:56,541
but at least get them know about it.

225
00:17:58,281 --> 00:18:03,101
And also, a corner thing, when we have
a version upgrade, like gcc or perl,

226
00:18:03,101 --> 00:18:09,280
the release team asks
"OK, we'll have to upgrade the buildds",

227
00:18:09,280 --> 00:18:12,121
and at the moment they don't even have
an account on them,

228
00:18:12,121 --> 00:18:15,481
so they can not check whether the version
is good or not.

229
00:18:17,781 --> 00:18:20,161
Maybe we should just provide an account,

230
00:18:20,161 --> 00:18:24,121
we'd thus need to know who we need to give
an account to.

231
00:18:26,001 --> 00:18:31,121
Basically, my idea would be
"OK, that's fine not being on ftp-master".

232
00:18:31,721 --> 00:18:35,921
The thing is we still want to have most of
the support of Debian,

233
00:18:35,921 --> 00:18:41,702
to make our life less a burden,
as much as possible,

234
00:18:41,702 --> 00:18:44,981
without any extra load on
the release team, etc.

235
00:18:45,281 --> 00:18:49,341
We do understand well that we don't want
to put work on people's hand.

236
00:18:50,821 --> 00:18:56,081
But we would like to still get some benefit
and probably there are solutions for this.

237
00:18:56,921 --> 00:19:04,540
And conversely, all of this, I mean, not
putting more work on us Hurd porters,

238
00:19:04,540 --> 00:19:10,301
would actually be the same solutions that
existing ports on debian-ports

239
00:19:10,301 --> 00:19:16,741
would be really happy to have, to improve
their life, to have less work to do,

240
00:19:17,041 --> 00:19:20,541
than what they currently have,
it's really a problem.

241
00:19:21,141 --> 00:19:25,981
So maybe we want to think about a real
status for Second Class Citizens,

242
00:19:25,981 --> 00:19:30,001
like Hurd, but also the sparc, hppa, etc.

243
00:19:30,380 --> 00:19:36,081
Maybe we want to have some BoF at some time,
so we can gather and discuss about this.

244
00:19:38,481 --> 00:19:45,741
Future work, the most interesting thing is
probably using the Rump drivers,

245
00:19:45,741 --> 00:19:50,640
because at the moment we use DDE but
it's not really going forward.

246
00:19:51,041 --> 00:19:55,701
We thought it would be a way to get newer
drivers, Linux drivers,

247
00:19:55,701 --> 00:20:00,061
without extra efforts, but it doesn't
actually happen at the moment,

248
00:20:00,061 --> 00:20:06,041
while Rump does go forward, we see work
being done with Xen etc.

249
00:20:06,621 --> 00:20:08,961
So this is probably a long-term solution.

250
00:20:10,921 --> 00:20:13,941
Maybe we'll have another distribution
through Guix.

251
00:20:15,141 --> 00:20:18,401
This is progressing, we are quite far from
doing this,

252
00:20:18,401 --> 00:20:23,860
so for now Debian is really the only Hurd
distribution that we have, so we'll see.

253
00:20:24,841 --> 00:20:32,001
And of course, just come and have fun with
your own pet project, just join, thanks!

254
00:20:33,441 --> 00:20:38,761
[Applause]

255
00:20:39,641 --> 00:20:42,401
[Michael Banck] Any quick questions before
we run to lunch?

256
00:20:53,321 --> 00:20:54,841
[Steve Chamberlain] Hello, I just wondered

257
00:20:55,281 --> 00:21:00,961
if you're using Hurd on that laptop
for the presentation?

258
00:21:01,261 --> 00:21:02,581
[Samuel Thibault] Yeah, yeah,
this is running Hurd, yes.

259
00:21:02,701 --> 00:21:04,662
[SC] So it's quite, like, usable everyday?

260
00:21:05,201 --> 00:21:07,921
[ST] Well, not everyday because
without USB,

261
00:21:07,921 --> 00:21:12,101
you can not mount a USB stick for instance,
so that's quite inconvenient,

262
00:21:12,101 --> 00:21:15,421
but yeah, I could probably use it everyday.

263
00:21:15,960 --> 00:21:20,040
I don't, I mean, for work,
I can not afford this, but yeah.

264
00:21:20,241 --> 00:21:23,481
Also, we don't have wireless drivers
at the moment.

265
00:21:23,761 --> 00:21:26,481
We hope that with the Rump drivers
we would get this.

266
00:21:26,801 --> 00:21:28,741
So, yes.

267
00:21:29,041 --> 00:21:30,841
Some people do use it everyday.

268
00:21:31,121 --> 00:21:32,281
Not me.

269
00:21:33,120 --> 00:21:36,101
[SC] But those would be the major thing
missing for more people

270
00:21:36,101 --> 00:21:37,361
to be able to use it.

271
00:21:37,841 --> 00:21:38,901
OK, thanks.

272
00:21:40,280 --> 00:21:48,581
[MB] Any more questions? We're run out of
questions, then thanks again. Thanks.

273
00:21:49,141 --> 00:21:54,560
[Applause]
