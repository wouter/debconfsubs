1
00:00:01,701 --> 00:00:11,044
Next up: Paul Liu and "madduck"
will present the DebConf 18 bids.

2
00:00:12,187 --> 00:00:16,189
Hello everyone. I'm Paul, from Taiwan

3
00:00:16,189 --> 00:00:20,615
and today I'm here to bid for 
Debconf18 for Taiwan.

4
00:00:20,615 --> 00:00:23,091
Let me introduce it.
Where is Taiwan?

5
00:00:23,091 --> 00:00:26,420
It's here - this small island.
Let's look closer:

6
00:00:26,420 --> 00:00:33,091
We are here, near China, Korea, Japan
and Hong Kong - they are around us.

7
00:00:36,516 --> 00:00:41,485
Taipei is a modern, convenient and safe city.

8
00:00:41,485 --> 00:00:43,765
How convenient?

9
00:00:43,765 --> 00:00:49,482
It's safe to walk outside even at
midnight - that kind of safe.

10
00:00:49,482 --> 00:00:55,925
Convenience stores are mostly open 24 hours
so you can buy food or drinks any time.

11
00:00:55,925 --> 00:01:01,122
And also many direct flights from anywhere
to Taipei, including these airports.

12
00:01:04,984 --> 00:01:09,720
So what's good in Taiwan?
Basically food!

13
00:01:09,720 --> 00:01:12,534
And food...

14
00:01:12,534 --> 00:01:14,424
... and more food.

15
00:01:14,424 --> 00:01:16,467
We present our culture by food.

16
00:01:16,467 --> 00:01:20,627
So when you eat the food, you can
taste our culture.

17
00:01:23,525 --> 00:01:29,594
This is our tallest building in Taiwan -
the I.T. industry is very good

18
00:01:29,594 --> 00:01:38,192
so you can see that we have
our friends here - Google and Canonical.

19
00:01:41,745 --> 00:01:50,083
Local transportation is very high-density,
so you can go everywhere just by MRT.

20
00:01:50,083 --> 00:02:02,062
You can use the EasyCard, a contact-less card,
to take the bus or train to anywhere.

21
00:02:02,062 --> 00:02:08,377
And in less than two hours, the high-speed
railway to the South part of Taiwan.

22
00:02:10,627 --> 00:02:18,373
The network is also very cheap,
for example US $15 for 10 days usage

23
00:02:18,373 --> 00:02:24,935
and free Wi-Fi has good coverage
in the city, but you have to stay

24
00:02:24,935 --> 00:02:32,525
in the same spot to use it - it's
not mobile.

25
00:02:39,241 --> 00:02:46,132
OK, I don't have much time, so I will
just show some pictures of Taiwan.

26
00:02:46,132 --> 00:02:52,026
All these places can be reached by the MRT
so they're really near.

27
00:02:52,026 --> 00:03:01,483
At the city zoo and cable car you can
drink some Chinese tea

28
00:03:01,483 --> 00:03:05,319
and this is also a good place.

29
00:03:05,319 --> 00:03:11,171
Along the roadside you can see there are
food stands everywhere

30
00:03:11,171 --> 00:03:15,579
so that's why food is so good.

31
00:03:15,579 --> 00:03:26,668
So, if you have questions you can contact
us in IRC or by email.

32
00:03:30,450 --> 00:03:37,768
We have a good team contributing
and preparing the bid.

33
00:03:37,768 --> 00:03:41,370
Thank you.
(applause)

34
00:03:47,967 --> 00:03:57,657
(audience)
Do you have ideas for possible venues?

35
00:03:57,657 --> 00:04:01,556
Yes, I am looking at the
universities in Taipei

36
00:04:01,556 --> 00:04:04,687
because of the price.

37
00:04:07,237 --> 00:04:11,740
(audience)
Do you have any idea about dates?

38
00:04:11,740 --> 00:04:15,979
Yes, we want to go for June
because in May it's humid and rainy

39
00:04:15,979 --> 00:04:21,248
and in July and August there
are typhoons and strong winds

40
00:04:21,248 --> 00:04:24,643
so that's not good.

41
00:04:24,643 --> 00:04:29,975
The best time is June.

42
00:04:31,207 --> 00:04:38,035
There is a wiki page if you're interested
in following more about Taipei

43
00:04:38,035 --> 00:04:42,777
so for instance the venue option
currently being investigated is here.

44
00:04:42,777 --> 00:04:46,541
We also answered the question about
the potential dates here.

45
00:04:46,541 --> 00:04:53,322
So if you have interest please go
to the wiki and develop questions there.

46
00:04:53,874 --> 00:04:59,909
(audience)
What's it like for visas?

47
00:04:59,909 --> 00:05:07,558
For many European countries and USA
there is a waiver, so you don't need visas.

48
00:05:11,398 --> 00:05:19,177
Also some Asian visitors have
30 days without needing a visa.

49
00:05:21,712 --> 00:05:26,003
(audience)
How many people are in the local team?

50
00:05:26,003 --> 00:05:32,748
Currently four committed, but I am
trying to build the team more.

51
00:05:39,049 --> 00:05:42,436
(applause)

52
00:05:45,378 --> 00:05:47,788
Now, I am standing up here

53
00:05:47,788 --> 00:05:50,673
not because you should be afraid
that I will organise another DebConf

54
00:05:50,673 --> 00:05:54,682
but...

55
00:05:57,837 --> 00:05:59,636
Oh, hmm.

56
00:05:59,636 --> 00:06:07,276
but because I am not a member of
the DebConf decision team I can

57
00:06:07,276 --> 00:06:12,637
stand in for the Prague people
who can't be here

58
00:06:12,637 --> 00:06:14,570
to present their bid.

59
00:06:14,570 --> 00:06:18,584
So even though Taipei would be
the first DebConf in Asia -

60
00:06:18,584 --> 00:06:24,457
and that's fantastic, so thank you
very much for coming forward -

61
00:06:24,457 --> 00:06:29,100
we're just not going to make it
that easy for you, there is competition!

62
00:06:29,100 --> 00:06:33,579
Prague ran last year but
Montreal was selected

63
00:06:33,579 --> 00:06:36,802
so they stood up and are doing it again.

64
00:06:36,802 --> 00:06:39,521
I'm not going to mention the
E.U. joke on here.

65
00:06:39,521 --> 00:06:46,740
So Prague: a lot of people have been there
and a lot of people have heard about it.

66
00:06:46,740 --> 00:06:49,623
It's a very beautiful city in
the heart of Europe

67
00:06:49,623 --> 00:06:53,033
and the cradle of the first university
as far as I can tell.

68
00:06:53,033 --> 00:06:57,113
It has a big international airport
with direct connections from

69
00:06:57,113 --> 00:07:00,790
pretty much anywhere else in the world.

70
00:07:00,790 --> 00:07:05,648
As I suspect a lot of DebConfers will
travel there by train or bus because

71
00:07:05,648 --> 00:07:10,898
that is already an adventure.

72
00:07:11,924 --> 00:07:17,431
There are multiple venue options
being looked at; we had one for DebConf 17

73
00:07:17,431 --> 00:07:21,151
but that needs to be re-negotiated because
the Dean of the university changed

74
00:07:21,151 --> 00:07:24,524
but that's always an opportunity
as well, isn't it?

75
00:07:24,524 --> 00:07:26,765
And then of course in terms
of leisure options

76
00:07:26,765 --> 00:07:29,907
for when we're not working hard at the venue

77
00:07:29,907 --> 00:07:35,046
there is the historic city, many parks,
the Prague zoo, excellent public transport

78
00:07:35,046 --> 00:07:38,718
- curiously I don't know why Ondrej sent
me these slides today, because

79
00:07:38,718 --> 00:07:43,318
last year he specifically mentioned beer
about fifty times during these slides

80
00:07:43,318 --> 00:07:48,151
but there is no beer in here, he has
one picture of Prague,

81
00:07:48,151 --> 00:07:50,687
and then another one, and
one of the zoo

82
00:07:50,687 --> 00:07:55,243
which is the fourth best zoo in the world
(laughter)

83
00:07:55,243 --> 00:07:58,316
That would make a great day trip option.

84
00:07:58,316 --> 00:08:01,568
So just imagine that you
see beer here

85
00:08:01,568 --> 00:08:06,017
and when I talked to Ondrej earlier
he's running up the team again.

86
00:08:06,017 --> 00:08:11,597
They're prepared and committed
to improving the bid they sent last year

87
00:08:11,597 --> 00:08:18,894
so by the bid deadline we expect to have
a very competitive suggestion

88
00:08:18,894 --> 00:08:23,276
for DebConf 18 in the Czech Republic.

89
00:08:23,276 --> 00:08:25,820
And that concludes this presentation,

90
00:08:25,820 --> 00:08:28,137
are there any questions
that I can answer or proxy?

91
00:08:29,090 --> 00:08:31,245
(audience)
Will there be beer?

92
00:08:31,245 --> 00:08:34,175
No, there isn't going to be any beer.

93
00:08:36,623 --> 00:08:38,994
(audience)
Will there be food?

94
00:08:38,994 --> 00:08:42,854
Help.
(audience laughter)

95
00:08:42,854 --> 00:08:48,003
I can't answer that question but
I'm happy to relay it to the team.

96
00:08:51,698 --> 00:08:58,025
Well, thank you very much
and Didier would like your attention.

97
00:08:58,025 --> 00:09:00,822
(applause)

98
00:09:05,254 --> 00:09:08,437
I don't have any slides, but
just a small reminder for teams

99
00:09:08,437 --> 00:09:12,191
who would like to present something
for DebConf 18

100
00:09:12,191 --> 00:09:15,519
about what the procedure is.

101
00:09:15,519 --> 00:09:19,252
As you might have been made
aware at previous DebConfs

102
00:09:19,252 --> 00:09:23,192
you're supposed to build a team

103
00:09:23,192 --> 00:09:28,081
gather all the information you can
and prepare it on the DebConf wiki.

104
00:09:28,081 --> 00:09:32,985
You can ask for a mailing list,
either on DebConf or Debian infrastructure

105
00:09:32,985 --> 00:09:35,831
if you need to co-ordinate the work.

106
00:09:35,831 --> 00:09:42,042
The deadline for bids is 1st December
of this year, and you are encouraged

107
00:09:42,042 --> 00:09:49,744
to join the existing DebConf list -
debconf-team@lists.debconf.org

108
00:09:49,744 --> 00:09:55,562
You can ask for assistance or feedback
on your bid, and make sure your bid

109
00:09:55,562 --> 00:09:59,778
is really super-good by 1st December.

110
00:09:59,778 --> 00:10:06,460
Then we will have some interactions
with your team to enhance your bid

111
00:10:06,460 --> 00:10:11,832
and then the DebConf committee will
try to get the decision done

112
00:10:11,832 --> 00:10:15,601
some time in February.

113
00:10:15,601 --> 00:10:20,714
So we will announce the schedule
when we have it, but that is roughly it.

114
00:10:22,016 --> 00:10:24,977
To recap: get everything ready for
1st December

115
00:10:24,977 --> 00:10:29,854
get involved, talk to people who have
been organising this DebConf and next.

116
00:10:29,854 --> 00:10:34,687
Get involved in Montreal and their bid
to see how they work

117
00:10:34,687 --> 00:10:38,834
and if possible be part of the local
team next year

118
00:10:38,834 --> 00:10:42,223
and that's it.
Are there any questions?

119
00:10:44,607 --> 00:10:48,390
Wow, that looks so simple when
we say it like that, but...

120
00:10:51,941 --> 00:10:56,045
(audience)
Is it nice to organise a DebConf?

121
00:10:56,045 --> 00:11:00,572
It is nice to get DebConf organised.
(laughter)

122
00:11:06,838 --> 00:11:13,389
It's very cool to host DebConf -
energy-consuming, certainly

123
00:11:13,389 --> 00:11:17,564
but very cool if you can organise
one in your country.

124
00:11:19,736 --> 00:11:23,826
So it's apparently time for penguins.
