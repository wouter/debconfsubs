#!/usr/bin/env python3

from unique_slugify import fnify
import xml.etree.ElementTree as ET


baseurl = 'http://ftp.acc.umu.se/pub/debian-meetings/2015/debconf15/'

f = open('video.list')

videos = {}

#manual initalization of some
manual_videos = { 'Organization_announcements.webm' : 248,
                'Live_demos_2.webm' : 361,
                'DebConf17.webm' : 346,
                'Lightning_talks_2.webm' : 359,
                'Lightning_talks_3.webm' : 360,
                'using_dbconfigcommon.webm' : 190,
#                'jenkinsdebianorg-session.webm' : 999,
                'Citizenfour_Q_A_Session.webm' : 324 }

for video in f:
    v = video.strip()
    if v in manual_videos:
       videos[v] = manual_videos[v]
    else:
       videos[v] = 0

tree = ET.parse('debconf15.xml')
root = tree.getroot()

for talk in root.iter('event'):
    title = talk.find('title').text
    id = talk.get('id')
    video_slug = fnify(title) + ".webm"

    try:
        if videos[video_slug] == -1:
            continue
        else:
            if(videos[video_slug] == 0):
                videos[video_slug] = id

    except KeyError:
        videos[video_slug] = -1
videos_byid = {v: k for k, v in videos.items()}

for video in videos.keys():
    id = videos[video]
    if id == 0 :
        #this print should never show up
        print("video without a talk: " + video)
    elif id == -1 :
        #uncomment to get the more likely talks without video list
        #print("talk without a video: " + video)
        pass
    else:
        #uncomment to get list of ids + videos urls
        #print(str(videos[video]) + "," + baseurl + video)
        xpath = ".//event[@id='" + str(id) + "']"
        talk = root.find(xpath)
#        print(talk.tag + talk.attrib['id'])
        vlink = ET.Element('video')
        vlink.text = baseurl + videos_byid[id]
        talk.append(vlink)


tree.write('debconf15.output.xml', encoding='utf-8')
